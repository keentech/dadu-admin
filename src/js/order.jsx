import { App } from './app';
import Util from './util';
import { Page } from './page';
import './dx-ext';

let orderType = [{name:'全部类型', id:''}, {name:'微信订单', id:'1'}, {name:'积分订单', id:'2'}];
let orderState = [{name:'全部状态', id:''}, {name:'待支付', id: 0}, {name:'待发货', id:1}, {name:'已取消', id:2}, {name:'已发货',id:5}, {name:'已完成',id:6},{name:'交易关闭',id:7}];

let filterExpr = {}, searchTextBoxInstance;
let applyFilterExpr = (ds,applyFilter=false) => {
	let arr = [];

	if (filterExpr.type) {
		arr.push(['pay_method', '=', filterExpr.type]);
	}

	if (filterExpr.state||filterExpr.state===0) {
		arr.push(['state', '=', filterExpr.state]);
	}

	if (filterExpr.start) {
		arr.push(['created_at', '>=', filterExpr.start]);
	}

	if (filterExpr.end) {
		arr.push(['created_at', '<=', filterExpr.end]);
	}
	
	if(applyFilter){ return arr};

	if (arr.length) ds.filter(arr);
	else ds.filter(null);

	
}

App.registerCrud({
	route: 'order',
	url: $.config('apiUrl') + 'restful?_model=order',
	title: '订单',
	placeholder: '订单号或产品名称',
	capable: {
		create: true
	},
	grid: {
		selection: {
			mode: 'none'
		},
		columnAutoWidth:true,
		showRowLines:true,
		wordWrapEnabled:true
	},
	search:{
		mobile:'out_trade_no'
	},
	toolbar:{
		items:[{
			location: 'before',
			widget: 'dxTextBox',
			options: {
				width: 300,
				placeholder: '搜索',
				onInitialized: function(e) {
					searchTextBoxInstance = e.component;
				}
			}
		}, {
			location: 'before',
			widget: 'dxButton',
			options: {
				text: '查询',
				onClick: function(e) {
					var ds = App.crudGridInstance().option('dataSource');

					ds.searchExpr(['out_trade_no', 'product_name','tel_number','username','create_user.name']);
					ds.searchValue(searchTextBoxInstance.option('value'));

					ds.reload();
				}
			}
		}, {
			location: 'after',
			widget: 'dxSelectBox',
			options: {
				dataSource: orderType,
				valueExpr: 'id',
				displayExpr: 'name',
				value: orderType[0].id,
				onValueChanged: function(e) {
					var ds = App.crudGridInstance().option('dataSource');

					filterExpr.type = e.value;
					applyFilterExpr(ds);

					ds.reload();
				}
			}
		}, {
			location:'after',
			widget:'dxSelectBox',
			options: {
				dataSource: orderState,
				valueExpr:'id',
				displayExpr:'name',
				value: orderState[0].id,
				onValueChanged:function(e){
					var ds = App.crudGridInstance().option('dataSource');

					filterExpr.state = e.value;
					applyFilterExpr(ds);

					ds.reload();

				}
			}
		}, {
			location:'after',
			widget:'dxDateBox',
			options: {
				displayFormat: "yyyy-MM-dd",
				dateSerializationFormat: 'yyyy-MM-dd',
				placeholder: '开始时间',
				onValueChanged:function(e){
					var ds = App.crudGridInstance().option('dataSource');

					filterExpr.start = e.value;
					applyFilterExpr(ds);

					ds.reload();
				}
			}
		},{
			location:'after',
			widget:'dxDateBox',
			options: {
				displayFormat: "yyyy-MM-dd",
				dateSerializationFormat: 'yyyy-MM-dd',
				placeholder: '结束时间',
				onValueChanged:function(e){
					var ds = App.crudGridInstance().option('dataSource');

					filterExpr.end = e.value;
					applyFilterExpr(ds);

					ds.reload();
				}
			}
		},{
			location:'after',
			widget:'dxButton',
			options:{
				text:'导出',
				onClick:function(e){
					var ds = App.crudGridInstance().option('dataSource');
					var url = new Url(API('order-export'));
					let loadOptions = {desc:true,filter:applyFilterExpr(ds,true)};

						url.query._param = JSON.stringify(loadOptions);

						window.location=url;

						 // $.crudStore(API('order-export')).load({desc:true,filter:applyFilterExpr(ds,true)}).then(res=>{
						 // 	console.log(res)
						 // })
						

					

					// let apply=applyFilterExpr(ds).concat({desc:true});
					// console.log(apply)
					// $.get(API('order-export'),apply,function(res){
					// 	console.log(res)
					// })
				}
			}
		}]
	},
	columns:[{
		dataField:'out_trade_no',
		caption:'订单号'
	},{
		dataField:'create_user.name',
		caption:'付款人'
	},{
		dataField:'product_name',
		caption:'产品名称'
	},{
		dataField:'total_fee',
		caption:'总金额'
	},{
		dataField:'quantity',
		caption:'数量'
	},{
		dataField:'pay_method',
		caption:'支付方式',
		cellTemplate:function(c,e){
			$(c).text(e.value===1?'微信支付':'积分兑换')
		}
	},{
		dataField:'username',
		caption:'收货人'
	},{
		dataField:'tel_number',
		caption:'手机号'
	},{
		dataField:'address',
		caption:'地址'
	},{
		dataField:'state',
		caption:'状态',
		cellTemplate:function(c,e){
			var text=orderState.filter(function(item){return item.id==e.value&&item.id!==''});
			if(text.length!=0)$(c).text(text[0].name)
		}
	},{
		dataField:'created_at',
		caption:'下单时间'
	},{
		dataField:'	state',
		allowSorting:false,
		caption:'发货',
		cellTemplate:function(c,e){
			let logistics={
				out_trade_no:e.data.out_trade_no
			}
			function active(){
			$.post(API('order-delivery'),
				logistics,
				function(e){
					DevExpress.ui.notify('发货成功','success',1500) ;
					App.crudGridInstance().option('dataSource').load()
						});
						return false
			};

			if(e.data.state == '1' ){
		 	$("<a>").text('发货').css({"text-decoration":"underline"}).click(function(){
		 		active()
		 	}).appendTo(c)
		 }
		}
	}]
})