export class Page {
	constructor($container) {
		this.$container = $container;
		this.$content = $container;
	}

	get content() {
		return this.$content;
	}

	crudLayout(opt) {
		let data = $.extend({title: '未设置'}, opt), html = `<div class="app-content-body ">
		  <div class="bg-light lter b-b wrapper-md hidden-print">
		    <h1 class="m-n font-thin h3">{{title}}</h1>
		  </div>
		  <div>
		    <div class="crud-container crud-reset">
		    </div>
		  </div>
		</div>`;

		this.$container.html(Mustache.render(html, data));
		this.$content = this.$container.find(".crud-container");

		return this;
	}
}