import { App } from './app';
import Util from './util';
import { Page } from './page';
import { DxExt } from './dx-ext';

DxExt.define({
  name: 'DealerInput',
  init: function($container, option) {
    var _this = this;

    option = $.extend({}, {
      single: false
    }, option);

    $container.html("<div class='tagbox'></div><div style='margin-top: 20px;' class='grid'></div>");

    this.tagBoxInstance = $container.find(".tagbox").dxTagBox({
      deferRendering: false,
      dataSource: $.crudStore(API("restful?_model=sys-dealer")),
      searchEnabled: true,
      valueExpr: 'id',
      displayExpr: 'name',
      placeholder: '请选择经销商',
      onSelectionChanged: (e) => {
        let v = _this.getValue() || [], changed = false;

        e.addedItems.forEach(x => {
          let idx = v.findIndex(y => y.id == x.id);
          if (idx < 0) {
            v.push({ id: x.id, show: 0, name: x.name });
            changed = true;
          }
        });

        let len = v.length;
        e.removedItems.forEach(x => v = v.filter(it => x.id !== it.id));
        if (! changed && len != v.length) changed = true;

        if (changed) setTimeout(() => _this.setValue(v), 0);
      }
    }).dxTagBox("instance");

    this.gridInstance = $container.find(".grid").dxDataGrid({
      columns: [{
        dataField: 'name',
        caption: '名称'
      }, {
        dataField: 'show',
        width: 120,
        caption: '是否显示',
        cellTemplate: (c, ci) => {
          let $el = $("<a>");
          $el.css("text-decoration", "underline").text(ci.value ? "显示" : "不显示").attr('title', '单击切换').appendTo(c);
          $el.click(function () {
            let v = _this.getValue() || [], idx = v.findIndex(x => x.id == ci.data.id);

            if (idx >= 0) {
              v[idx].show = (v[idx].show == 0 ? 1 : 0);
              _this.setValue(v);
            }
          });
        }
      }, {
        width: 120,
        dataField: 'id',
        caption: '删除',
        cellTemplate: (c, ci) => {
          let $el = $("<a>");
          $el.css("text-decoration", "underline").text("删除").appendTo(c);
          $el.click(function () {
            let v = _this.getValue() || [], idx = v.findIndex(x => x.id == ci.value);

            if (idx >= 0) {
              v.splice(idx, 1);
              _this.setValue(v);
            }
          });
        }
      }]
    }).dxDataGrid("instance");

    // $container.find(".file .dx-button-content").html('<i class="iconfont">&#xe623;</i>');
  },

  render: function($container) {
    let v = this.getValue() || [];

    if (this.gridInstance) {
      this.gridInstance.option('dataSource', v);
    }

    if (this.tagBoxInstance) {
      this.tagBoxInstance.option('value', v.map(item => item.id));
    }

    // if ($container.find(".grid").length > 0) {
    //   $container.find(".grid").dxDataGrid('option', 'dataSource', v);
    // }
  }
});

function validateForm(formInstance) {
    console.log('validateForm');
    
    var editors = [];

    function _check(o) {
      if(o.dataField) {
        var editor = formInstance.getEditor(o.dataField);
        if(editor && typeof(editor.isExtInstance) === 'function') {
          editors.push(editor);
        }
      } else if(o.items) {
        $.each(o.items, function(i, item) {
          _check(item);
        })
      }
    }

    _check({
      items: formInstance.option('items')
    });

    var result = true;
    $.each(editors, function(i, e) {
      if(!e.validate()) {
        result = false;
      }
    });

    return result;
}

DxExt.define({
  name: 'dxScene',

  init: function ($container, option) {
    var sceneByLevel = {
      "1": [],
      "2": [],
      "3": []
    };

    function _tr(items, level) {
      for (var i = 0; i < items.length; i++) {
        sceneByLevel[level.toString()].push(items[i]);

        if (items[i].child && items[i].child.length > 0) {
          _tr(items[i].child, level + 1);
        }
      }
    }

    _tr(this.opt.sceneTree, 1);

    this.sceneByLevel = sceneByLevel;
  },

  render: function ($container) {
    let v = this.getValue(),
      _this = this;

    v = v || [];

    $container.html('');

    let tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' +
      '<div class="pull-left"><div class="grape-input-one" style="margin-right: 10px;"></div></div>' +
      '<div class="pull-left"><div class="grape-input-two" style="margin-right: 10px;"></div></div>' +
      '<div class="pull-left"><div class="grape-input-three" style="margin-right: 10px;"></div></div>' +
      '<div class="pull-left" style="margin-right: 10px;"><div class="remove-item"></div></div>' +
      '</div></div>';

    let lookupItem = (v, items) => {
      let idx = items.findIndex(it => it.value == v);
      
      return idx >= 0 ? items[idx] : null;
    };

    let arrayOrEmpty = (item) => {
      return item ? [item] : [];
    };

    console.log('dxScene value');
    console.log(v);

    $.each(v, (i, item) => {
      let $tpl = $(tpl);

      $tpl.find(".grape-input-one").dxSelectBox({
        dataSource: this.sceneByLevel["1"],
        displayExpr: 'name',
        valueExpr: 'value',
        value: parseInt(item.scene_one_id),
        onValueChanged: function (e) {
          v[i].scene_one_id = e.value;

          let item = lookupItem(e.value, _this.sceneByLevel["1"]);
          $tpl.find(".grape-input-two").dxSelectBox('instance').option('dataSource', item ? item.child : []);

          $tpl.find('.grape-input-three').dxSelectBox('instance').option('dataSource', []);
        }
      });

      $tpl.find(".grape-input-two").dxSelectBox({
        dataSource: arrayOrEmpty(lookupItem(item.scene_two_id, _this.sceneByLevel["2"])),
        displayExpr: 'name',
        valueExpr: 'value',
        value: parseInt(item.scene_two_id),
        onValueChanged: function (e) {
          v[i].scene_two_id = e.value;

          let item = lookupItem(e.value, _this.sceneByLevel["2"]);
          $tpl.find(".grape-input-three").dxSelectBox('instance').option('dataSource', item ? item.child : []);
        }
      });

      $tpl.find(".grape-input-three").dxSelectBox({
        dataSource: arrayOrEmpty(lookupItem(item.scene_three_id, _this.sceneByLevel["3"])),
        displayExpr: 'name',
        valueExpr: 'value',
        value: parseInt(item.scene_three_id),
        onValueChanged: function (e) {
          v[i].scene_three_id = e.value;
        }
      });

      $tpl.find(".remove-item").dxButton({
        text: '删除',
        onClick: function () {
          v.splice(i, 1);
          _this.setValue(v);
        }
      });

      $tpl.appendTo($container);
    });

    $("<div/>").dxButton({
      text: '增加',
      onClick: function () {
        v.push({ scene_one_id: 0, scene_two_id: 0, scene_three_id: 0 });
        _this.setValue(v);
      }
    }).appendTo($container);

  }
});

DxExt.define({
  name: 'dxTasteList',

  init: function ($container, option) {
    // this.setValue(null);
    this.store = $.crudStore(API('restful?_model=wine-cuisine'));
  },

  render: function ($container) {
    var v = this.getValue(),
      _this = this;

    v = v || [];

    var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' +
      '<div class="pull-left" style="margin-right:30px"><div class="grape-input-taste"></div></div>' +
      '<div class="pull-left" style="margin-right:30px"><div class="grape-input-remark"></div></div>' +
      '<div class="pull-left" style="margin-right:30px"><div class="grape-remove"></div></div>' +
      '</div></div>';

    $container.html('');

    $.each(v, function (i, item) {
      var $c = $(tpl);

      $c.find(".grape-input-taste").dxSelectBox({
        dataSource: _this.store,
        width: 200,
        value: item.wine_cuisines_id,
        valueExpr: 'id',
        displayExpr: 'name',
        onValueChanged: function (e) {
          v[i].wine_cuisines_id = e.value;
        }
      });

      $c.find('.grape-input-remark').dxTextBox({
        width: 400,
        value: item.text,
        onValueChanged: function (e) {
          v[i].text = e.value;
        }
      })

      $c.find(".grape-remove").dxButton({
        text: '删除',
        onClick: function () {
          v.splice(i, 1);
          _this.setValue(v);
        }
      });


      $c.appendTo($container);
    });

    $("<div/>").dxButton({
      text: '增加',
      onClick: function () {
        v.push({
          wine_cuisines_id: 0,
          text: ''
        });
        _this.setValue(v);
      }
    }).appendTo($container);

  }
});

DxExt.define({
  name: 'dxUserLabel',

  init: function ($container, option) {

  },

  render: function ($container) {
    var v = this.getValue(),
      _this = this;

    v = v || [];

    var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' +
      '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">身份</div>' +
      '<div class="pull-left" style="margin-right: 10px;"><div class="grape-input-name"></div></div>' +
      '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">性别</div>' +
      '<div class="pull-left"  style="margin-right: 10px;"><div class="grape-input-sex"></div></div>' +
      '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">年龄段</div>' +
      '<div class="pull-left" style="margin-right: 10px;"><div class="grape-input-age"></div></div>' +
      '<div class="pull-left"><div class="grape-remove"></div></div>' +
      '</div></div>';

    $container.html('');

    let age = ['18-25', '25-30', '30-35', '35-45', '45-55', '55以上'];
    let auth = ['领导', '客户', '长辈', '闺蜜', '基友', '朋友', '情侣'];

    $.each(v, function (i, item) {
      var $c = $(tpl);

      $c.find(".grape-input-name").dxSelectBox({
        dataSource: auth,
        width: 200,
        value: item.auth,
        onValueChanged: function (e) {
          v[i].auth = e.value;
        }
      });

      $c.find('.grape-input-sex').dxSelectBox({
        width: 200,
        dataSource: [{ text: '男', value: 'man' }, { text: '女', value: 'woman' }],
        valueExpr: 'value',
        displayExpr: 'text',
        value: item.sex,
        onValueChanged: function (e) {
          v[i].sex = e.value;
        }
      })

      $c.find('.grape-input-age').dxSelectBox({
        width: 200,
        dataSource: age,
        value: item.age,
        onValueChanged: function (e) {
          console.log(e);
          v[i].age = e.value;
        }
      })

      $c.find(".grape-remove").dxButton({
        text: '删除',
        onClick: function () {
          v.splice(i, 1);
          _this.setValue(v);
        }
      });

      $c.appendTo($container);
    });

    $("<div/>").dxButton({
      text: '增加',
      onClick: function () {
        v.push({auth: '', sex: '', age: ''});
        _this.setValue(v);
      }
    }).appendTo($container);


  }
});

DxExt.define({
  name: 'KeywordList',

  init: function ($container, option) {
    // this.setValue(null);
    this.store = $.crudStore(API('restful?_model=wine-taste-keywords'));
  },

  render: function ($container) {
    var v = this.getValue(),
      _this = this;

    v = v || [];

    var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' +
      '<div class="pull-left" style="margin-right:30px"><div class="grape-input-taste"></div></div>' +
      '<div class="pull-left" style="margin-right:30px"><div class="grape-input-remark"></div></div>' +
      '<div class="pull-left" style="margin-right:30px"><div class="grape-remove"></div></div>' +
      '</div></div>';

    $container.html('');

    $.each(v, function (i, item) {
      var $c = $(tpl);

      $c.find(".grape-input-taste").dxSelectBox({
        dataSource: _this.store,
        width: 200,
        value: item.keyword_id,
        valueExpr: 'id',
        displayExpr: 'name',
        onValueChanged: function (e) {
          v[i].keyword_id = e.value;
        }
      });

      $c.find('.grape-input-remark').dxTextBox({
        width: 400,
        value: item.text,
        onValueChanged: function (e) {
          v[i].text = e.value;
        }
      })

      $c.find(".grape-remove").dxButton({
        text: '删除',
        onClick: function () {
          v.splice(i, 1);
          _this.setValue(v);
        }
      });


      $c.appendTo($container);
    });

    $("<div/>").dxButton({
      text: '增加',
      onClick: function () {
        v.push({
          keyword_id: 0,
          text: ''
        });
        _this.setValue(v);
      }
    }).appendTo($container);

  }
});

let initPage = (context, $container) => {
	let page = new Page($container);

	let id = context.params.id, wineStore = $.crudStore(API("restful?_model=product"));
  	let formData = '', data = {};

	page.crudLayout({title: id ? '编辑' : '新建'});

	page.content.html(`
    <form id="form-container">
      <div class="form_title">
        <div class="my_hd" style="padding: 0; border-bottom: 0;">
          <div id="dxtabs"></div>
        </div>
        <div class="crud-container crud-reset">
	        <div class="row">
	          <div class="col-sm-12">
	            
	            <div id="wine-form">
	            </div>
	          </div>
	        </div>
	      	
	      	<div class="row">
		        <div class="col-sm-12 text-right" style="margin-top: 15px;">
		          <div id='saveData'></div>
		          <div id="cancel"></div>
		        </div>
	      	</div>
        </div>
	  	</div>
    </form>
	`);

      $(".main-title").text(id ? "编辑葡萄酒信息" : "新建葡萄酒信息");

      let xhrs = [], prefetchData = {}, formInstances = {};

      xhrs.push($.get(API("restful?_model=product")).then(function (resp) {
        prefetchData['restful?_model=product'] = resp.data;
      }));

      xhrs.push($.get(API('restful?_model=product-attr-group')).then(function (resp) {
        prefetchData['restful?_model=product-attr-group'] = resp;
      }));


      $.when.apply($, xhrs).then(function () {
        let formItems = {};

        formItems['show'] = [{
          dataField:'keywords',
          label:{text:'产品名称'},
          editorType: 'KeywordList',
          editorOptions: {

          }
        },{
          dataField: 'consume',
          label: { text: '消费指南' },
          editorType: 'dxTextArea'
        }, {
          itemType: 'group',
          colCount: '3',
          items: [{
              dataField: 'wineglass',
              label: { text: '杯形' }
            },
            {
              dataField: 'sober_time',
              label: { text:  '醒酒时间' }
            }, {
              dataField: 'temperature',
              label: { text:  '饮用温度' }
            }
          ]
        }, {
            itemType: 'group',
            items: [{
                dataField: 'chinese_food',
                label: { text:  '中餐搭配', }

              },
              {
                dataField: 'western_food',
                label: { text:  '西餐搭配' }
              }, {
                dataField: 'snack',
                label: { text:  '小食搭配' }
              },
              {
                dataField: 'undelicacy',
                label: { text:  '暗黑搭配' }
              }
            ]
        }, {
            dataField:'talk_keywords',
            label:{text:'谈资关键词'},
            helpText: '20字内'
        },{

            dataField: 'talk_about',
            label: { text:  '谈资建议' },
            editorType: 'dxEditor'
        }];

       


          formItems['params'] = [{
            itemType: 'group',
            colCount: 2,
            items: [{
              dataField: 'chateau_id',
              label: { text: '品牌' },
              validationRules: [{type: "required"}],
              editorType: 'dxSelectBox',
              editorOptions: {
                dataSource: $.crudStore(API('restful?_model=wine-chateau')),
                showClearButton: true,
                placeholder: "品牌",
                searchEnabled: true,
                valueExpr: 'id',
                noDataText: '没有请求到品牌数据',
                itemTemplate: function(data, index, $el) {
                  if (data) {
                    $("<span>" + data.chname + " (" + data.name + ")</span>").appendTo($el);
                  }
                },
                displayExpr: function(data) {
                  return data ? data.chname + " (" + data.name + ")" : "";
                }
              }

            }]
          }, {
            itemType: 'group',
            items: [
              {
                dataField: 'chname',
                validationRules: [{
                  type: "required",
                  message: "请输入商品名称"
                }],
                label: { text: '商品名称' }
              }, {
                dataField: 'name',
                validationRules: [{
                  type: "required",
                  message: "商品毛重"
                }],
                label: { text: '商品毛重' }
              }, {
                dataField: 'letDrink_recom',
                validationRules: [{
                  type: "required",
                  message: "请输入乐饮推荐语"
                }],
                label: { text: '商品毛重' }
              }
            ]
          }, {
              itemType: 'group',
              colCount:3,
              items:[{
                dataField:'promotion_price',
                validationRules: [{type: "required"}],
                label: { text:  '促销价' },
                editorType: 'dxNumberBox'
              },{
                dataField: 'price',
                validationRules: [{type: "required"}],
                label: { text:  '原价' },
                editorType: 'dxNumberBox'
              },{
                dataField:'sku',
                label: { text:  '库存' },
                editorType: 'dxNumberBox'
              }]
            }, {
              itemType: 'group',
              colCount: 1,
              items:[{
                dataField: 'sell_method',
                validationRules: [{type: "required"}],
                label: { text:  '销售方式' },
                editorType: 'dxRadioGroup',
                editorOptions: {
                  layout: "horizontal",
                  valueExpr: 'value',
                  dataSource: [{
                    text: '线上销售',
                    value: 'online'
                  }, {
                    text: '线下销售',
                    value: 'offline'
                  },{
                    text:'产品展示',
                    value:'show'
                  }]
                },
                helpText: '线上销售可以从线上直接购买，前端的价格信息单独展示；线下销售，线上不能购买。价格展示在经销商信息上。'
              }]
            }, {
              itemType: 'group',
              items: [{
                dataField: 'banner',
                validationRules: [{
                  type: "required"
                }],
                label: { text: '封面图' },
                editorType: 'ImageUploader',
                editorOptions: {
                  single: true,
                  imageWidth: 750,
                  imageHeight: 422
                }
              }, {
                  dataField: 'covers',
                  validationRules: [{
                    type: "required"
                  }],
                  label: { text: '轮播图' },
                  editorType: 'ImageUploader',
                  editorOptions: {
                    imageWidth: 750,
                    imageHeight: 1118
                  }
                }
              ]
            }, 
             {
              itemType: 'group',
              colCount: '2',
              items: [{
                dataField: 'category',
                label: { text:  '类别' },
                validationRules: [{
                  type: "required",
                  message: "请输入类别"
                }],
                editorType: 'dxSelectBox',
                editorOptions: {
                  dataSource: prefetchData['product-category'],
                  acceptCustomValue: true,
                  onCustomItemCreating: function(e) {
                    return e.text;
                  },
                  placeholder: '选择或者输入类别'
                }
              }, {
                dataField: 'level',
                label: { text: '产品级别' }
              }]
            }, {
                dataField: 'vintage',
                label: { text: '生产日期' },
                validationRules: [{
                  type: "required",
                  message: "请输入生产日期"
                }],
                editorType: 'YearSelect'
           }, {
             itemType: 'group',
              items: [{
                dataField: 'importers',
                label: { text: '进口商' },
                editorType: 'dxTagBox',
                editorOptions: {
                  deferRendering: false,
                  dataSource: $.crudStore(API("restful?_model=sys-importer")),
                  searchEnabled: true,
                  valueExpr: 'id',
                  displayExpr: 'name'
                }
              }, {
                dataField: 'dealers',
                label: { text: '经销商' },
                editorType: 'DealerInput',
                editorOptions: {
                }
              }]
           }
          ]

         let tabs = [{
          text: '属性信息',
          id: "show"
        },{
          text: "参数信息",
          id: "params",
          disabled: ! id
        }];

        $.each([ "params","show"], function(index, item) {
          formInstances[item] = $("<div>").appendTo($("#wine-form")).dxForm({
            items: formItems[item],
            showColonAfterLabel: true,
            showValidationSummary: false,
            validationGroup: "crud",
            alignItemLabels: true,
            alignItemLabelsInAllGroups: true
          }).dxForm("instance");
        });

        function showTab(item) {
          for (let k in formInstances) {
            formInstances[k].option('visible', k === item);
          }
        }

        $("#dxtabs").dxTabs({
          dataSource: tabs,
          selectedIndex: 0,
          onItemClick: function(e) {
            showTab(e.itemData.id);
          }
        });

        showTab("show");

        if(id) {
          wineStore.byKey(id).then(function (resp) {
            console.log(resp);

            $.each(["country_id", "chateau_id"], function(i, item) {
              resp[item] = parseInt(resp[item]);
            });

            $.each(["relation_products", "relation_importers"], function(i, item) {
              if (resp[item]) resp[item] = $.map(resp[item], function(it) { return it.id; });
            });

            if (resp['relation_dealers']) {
              resp['dealers'] = $.map(resp['relation_dealers'], (it) => { return {id: it.id, show: it.pivot.show, name: it.name}; });
            }

            let mapped = {
              "blend_deployment": "deployment_graperies",
              "scenes": "scene_ids",
              "cuisines": "wine_cuisines",
              "relation_importers": "importers",
              "judge.expert_judges": "judge.judge_arr",
              "score.expert_scores": "score.self_arr",
              "score.same_scores": "score.same_arr",
              "drink.tasteKeywords": "drink.keywords",
            };

            for (let k in mapped) {
              if (_.has(resp, k)) {
                _.set(resp, mapped[k], _.get(resp, k));
                _.unset(resp, k);
              }
            }

            console.log(resp);

            let dataBasic = $.extend({}, resp);
            $.each(["drink", "score", "judge", "technique"], function(i, item) { 
              dataBasic[item] = undefined;
            });

            formInstances['basic'].option('formData', dataBasic);

            formInstances['drink'].option('formData', resp.drink);
            formInstances['judge'].option('formData', resp.judge);
            formInstances['technique'].option('formData', resp.technique);

            formInstances['score'].option('formData', resp.score);
            
          });

        } else {
          let productData = new DevExpress.data.CustomStore({
            store: $.crudStore(wineStore)
          });

          //form(productData)
        }
      });

      function saveCurrentTab() {
        let selectedItem = $("#dxtabs").dxTabs("option", "selectedItem");

        if (selectedItem) {
          $("#saveData").dxButton('option', { disabled: true, text: '正在保存' });

          let url = selectedItem.id === 'basic' ? API("restful?_model=wine-product") : API("restful?_model=wine-product-" + selectedItem.id);

          let formData = $.extend({}, formInstances[selectedItem.id].option('formData'));

          if (selectedItem.id !== 'basic') {
            formData.product_id = id;
          } else {
            let mapped = {
              relation_products: 'product_relation_ids',
              //scenes: 'scene_ids'
            };

            for (let k in mapped) {
              if (formData[k]) {
                formData[mapped[k]] = formData[k].join(",");
              } else {
                formData[mapped[k]] = '';
              }

              formData[k] = undefined;
            }

            if (formData['covers']) {
              formData['cover_ids'] = $.map(formData['covers'], function(item) { return item.id; }).join(",");
            }
          }

          console.log(formData);

          let request = null;
          if (formData.id) {
            request = $.crudStore(url).update(formData.id, formData);
          } else {
            request = $.crudStore(url).insert(formData);
          }

          request.then(function(resp, data) {

            DevExpress.ui.notify({
              message: "已更新",
            }, "success", 1000);

            if (! id) {
              App.getEventContext().redirect('#/newproduct/' + data.data.id);
            }

            console.log(resp);
          }).fail(function (e) {

            let message = "更新失败";
            if ($.crudStoreResp && $.crudStoreResp.message) {
              message += ": " + $.crudStoreResp.message;
            } else {
              message += ": 服务器错误";
            }

            DevExpress.ui.notify({
              message: message,
            }, "error", 1000);

          }).always(function () {
            $("#saveData").dxButton('option', { disabled: false, text: '保存' });
          })
        }
      }

      $("#saveData").dxButton({
        text: id ? '保存' : '新建',
        type: 'default',
        useSubmitBehavior: false
      }).click(function () {
        let selectedItem = $("#dxtabs").dxTabs("option", "selectedItem");

        if (selectedItem) {
          let result = formInstances[selectedItem.id].validate();

          if (! result.isValid) {
            let $el = result.validators[0].element();

            let offset = $el.offset();

            $('html, body').animate({
                scrollTop: offset.top - $("header.app-header").height() - 20
            });

          // } else if (validateForm(formInstances[selectedItem.id])) {
          //   saveCurrentTab();
          } else {
            saveCurrentTab();
          }
        }

        return false;
      })

    $("#cancel").dxButton({
        text: "返回",
        type: "normal",
        onClick: function() {
          context.redirect("#/members");
        }
    });
}

App.registerRoute({
	name: 'newproduct/:id', 
	onLoad: (context, $container) => {
		initPage(context, $container);
	}
});

App.registerRoute({
	name: 'newproduct', 
	onLoad: (context, $container) => {
		initPage(context, $container);
	}
});