class Util {
	formatXbm(xbm) {
		if (xbm == '2') return '女';
		else if (xbm == '1') return '男';
		else return xbm;
	}

	crudStore(url, options) {

		var SERVICE_URL = url;
		options = $.extend({
			onDataArrived: function(data) {
				return data;
			}
		}, options);

		return new DevExpress.data.CustomStore($.extend({}, options, {

			load: function(loadOptions) {

				var u = new Url(SERVICE_URL);

				u.query._param = JSON.stringify(loadOptions);

				return $.getJSON(u).then(function(resp) {
					console.log("data done");
					return options.onDataArrived(resp);
				});
			},

			byKey: function(key) {
				var u = new Url(SERVICE_URL);
				u.path += "/" + encodeURIComponent(key);

				return $.getJSON(u).then(function (resp) {
					return resp;
				});
			},

			insert: function(values) {
				console.log(values);
				return $.post(SERVICE_URL, values).always(function(resp) {
					$.crudStoreResp = resp.responseJSON;
					return resp;
				});
			},

			update: function(key, values) {
				var u = new Url(SERVICE_URL);
				u.path += "/" + encodeURIComponent(key);

				return $.ajax({
					url: u,
					method: "PUT",
					data: values
				}).always(function(resp) {
					$.crudStoreResp = resp.responseJSON;
					return resp;
				});
			},

			remove: function(key) {
				var u = new Url(SERVICE_URL);
				u.path += "/" + encodeURIComponent(key);

				return $.ajax({
					url: u,
					method: "DELETE"
				});
			}

		}));
	}
}

export default new Util();