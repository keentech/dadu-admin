import { App } from './app';
import Util from './util';
import { Page } from './page';
import './dx-ext';

// App.registerCrud({
// 	url: $.config('apiUrl') + 'restful?_model=wine-product-qrcode',
// 	route: 'qrcode',
// 	title: '二维码发放',
// 	capable: {
// 		create: true
// 	},
// 	grid: {
// 		selection: {
// 			mode: 'none'
// 		}
// 	},
// 	buttons: [],
// 	columns: [{
// 		dataField: 'created_at',
// 		caption: '发放时间',
// 		width: 150
// 	}, {
// 		dataField: 'amount',
// 		caption: '发放数量',
// 		width: 100
// 	}, {
// 		caption: '酒庄中文',
// 	}, {
// 		caption: '酒庄英文',
// 	}, {
// 		dataField: 'chateauCode',
// 		caption: '酒庄码',
// 		width: 60
// 	}, {
// 		dataField: 'countryCode',
// 		caption: '国家码',
// 		width: 60
// 	}, {
// 		dataField: 'subdivisionCode',
// 		caption: '地区码',
// 		width: 60
// 	}, {
// 		dataField: 'regionCode',
// 		caption: '产区码',
// 		width: 60
// 	}, {
// 		caption: '产品中文',
// 	}, {
// 		caption: '产品英文',
// 	}, {
// 		dataField: 'productCode',
// 		caption: '产品码',
// 		width: 60
// 	}, {
// 		dataField: 'vintage',
// 		caption: '年份',
// 		width: 60
// 	}, {
// 		caption: '操作',
// 		minWidth: 120,
// 		buttons: [{
// 			title: '导出',
// 			onClick: function(data, instance) {
// 				location.href = data.download_url;
// 			}
// 		}, {
// 			title: '查看',
// 			url: '#/qrcode-record/{{id}}'
// 		}]
// 	}],
// 	form: {
// 		items: [{
// 			dataField: 'chateau_id',
// 			validationRules: [{type: "required"}],
// 			label: {
// 				text: '酒庄'
// 			},
// 			editorType: 'dxSelectBox',
// 			editorOptions: {
// 				dataSource: Util.crudStore($.config('apiUrl') + 'restful?_model=wine-chateau'),
// 				showClearButton: true,
// 				placeholder: "酒庄",
// 				searchEnabled: true,
// 				valueExpr: 'id',
// 				noDataText: '没有请求到酒庄数据',
// 				itemTemplate: function(data, index, $el) {
// 					if(data) {
// 						$("<span>" + data.chname + " (" + data.name + ")</span>").appendTo($el);
// 					}
// 				},
// 				displayExpr: function(data) {
// 					return data ? data.chname + " (" + data.name + ")" : "";
// 				},
// 				onValueChanged: function(e) {
// 					setTimeout(function() {
// 						var formInstance = App.getFormInstance();

// 						if(formInstance) {
// 							var inst = formInstance.getEditor('product_id');
// 							inst.option('disabled', !e.value);

// 							if(e.value) {
// 								inst.option('dataSource').filter('chateau_id', '=', e.value);
// 								inst.option('dataSource').reload();
// 							}
// 						}
// 					}, 0);
// 				}
// 			}
// 		}, {
// 			dataField: 'product_id',
// 			validationRules: [{type: "required"}],
// 			label: {
// 				text: '产品'
// 			},
// 			editorType: 'dxSelectBox',
// 			editorOptions: {
// 				dataSource: new DevExpress.data.DataSource({
// 					store: Util.crudStore($.config('apiUrl') + "restful?_model=wine-product")
// 				}),
// 				searchEnabled: true,
// 				valueExpr: 'id',
// 				noDataText: '没有请求到产品数据',
// 				disabled: true,
// 				itemTemplate: function(data, index, $el) {
// 					if(data) {
// 						$("<span>" + data.chname + " (" + data.name + ")</span>").appendTo($el);
// 					}
// 				},
// 				displayExpr: function(data) {
// 					return data ? data.chname + " (" + data.name + ")" : "";
// 				}
// 			}
// 		}, {
// 			dataField: 'vintage',
// 			validationRules: [{type: "required"}],
// 			label: {
// 				text: '年份'
// 			},
// 			editorType: 'YearSelect',
// 			editorOptions: {}
// 		}, {
// 			dataField: 'amount',
// 			validationRules: [{type: "required"}],
// 			label: {
// 				text: '数量'
// 			},
// 			editorType: 'dxNumberBox',
// 			editorOptions: {}
// 		}]
// 	}
// });

// App.registerCrud({
// 	url: $.config('apiUrl') + 'restful?_model=wine-qrcode-record',
// 	route: 'qrcode-record/:qrcode_id',
// 	title: '二维码记录',
// 	capable: {
// 		create: false
// 	},
// 	buttons: [],
// 	columns: [{
// 		dataField: 'created_at',
// 		caption: '创建时间'
// 	}, {
// 		dataField: 'code',
// 		caption: '编码'
// 	}, {
// 		dataField: 'sortno',
// 		caption: '序码'
// 	}, {
// 		dataField: 'state',
// 		caption: '状态',
// 		cellTemplate: function(c, ci) {
// 			c.html('未扫码');
// 		}
// 	}, {
// 		caption: '操作',
// 		buttons: [{
// 			title: '查看',
// 			onClick: function(data, instance) {
// 				$("<div/>").appendTo($("#popups")).dxPopup({
// 					title: '查看二维码',
// 					visible: true,
// 					width: 360,
// 					height: 360,
// 					onContentReady: function(e) {
// 						var $content = e.component.content();

// 						new QRCode($(".qrcode", $content)[0], $.config('qrCodeUrl') + data.code);
// 					},
// 					contentTemplate: function() {
// 						var $content = $("<div style='text-align: center;'><div class='qrcode' style='display: inline-block;'></div></div>");
// 						return $content;
// 					}
// 				});
// 			}
// 		}]
// 	}]
// });