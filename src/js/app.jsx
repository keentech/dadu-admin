
class AppClass {
	constructor() {
		this._eventContext = null;
		this._loadingPanel = null;
		this._pages = [];
		this._routes = [];
		this._user = null;

		console.log("Making App");
	}

	get pages() {
		return this._pages;
	}

	get routes() {
		return this._routes;
	}

	get user() {
		return this._user;
	}

	login(user) {
		this._user = $.extend({}, user);
	}

	crudChartInstance(){
		return $(".data-chart").dxChart("instance")
	}


	crudGridInstance() {
		return $(".crud-grid").dxDataGrid("instance");
	}

	getFormInstance() {
		return $("#form").dxForm("instance");
	}

	getEventContext() {
		return this._eventContext;
	}

	setEventContext(v) {
		this._eventContext = v;
	}

	showLoading(message) {
		message = message || '正在加载...';

		this._loadingPanel = $("#loadpanel").dxLoadPanel({
        	shadingColor: "rgba(0,0,0,0.4)",
        	position: { of: "body" },
        	visible: true,
        	showIndicator: true,
        	showPane: true,
        	shading: true,
        	closeOnOutsideClick: false,
        	message: message
    	}).dxLoadPanel("instance");
	}

	hideLoading() {
		if (this._loadingPanel !== null) {
			this._loadingPanel.hide();
			this._loadingPanel = null;
		}
	}

	registerCrud(opt) {
		this.pages.push(opt);
	}

	registerRoute(opt) {
		this.routes.push(opt);
	}
}

export let App = new AppClass();