'use strict';

var _app = require('./app');

var _util = require('./util');

var _util2 = _interopRequireDefault(_util);

var _page = require('./page');

require('./dx-ext');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// App.registerCrud({
// 	route: 'daily-sign',
// 	url: $.config('apiUrl') + 'restful?_model=cms-daily-sign',
// 	placeholder: '每日签',
// 	title: '每日签',
// 	capable: {
// 		create: true
// 	},
// 	search: {
// 		mobile: 'title',
// 	},
// 	columns: [{
// 			dataField: 'id',
// 			caption: 'id'
// 		}, {
// 			dataField: 'title',
// 			caption: '名称'
// 		}, {
// 			dataField: 'online_at',
// 			caption: '上线时间'
// 		}, {
// 			dataField: 'column_name',
// 			caption: '类别'
// 		}, {
// 			dataField: 'online',
// 			caption: '上下线',
// 			cellTemplate: function(con, data) {
// 				$(con).dxUpdown('article', data)
// 			}
// 		},
// 		{
// 			caption: '操作',
// 			buttons: [{
// 				title: '编辑',
// 				url: '#/daily-sign/edit/{{id}}',
// 				del: 'cms-daily-sign'
// 			}]
// 		}
// 	],
// 	form: {
// 		readOnly: false,
// 		showColonAfterLabel: true,
// 		showValidationSummary: false,
// 		colCount: 1,
// 		afterDataUpdate: function(formInstance, data) {
// 			//console.log(formInstance.geiEditor("#editor"));

// 		},
// 		beforeSubmit: function(form, id) {
// 			var formData = form.option('formData');
// 			var cover = [];
// 			$.each(formData.covers, function(i, cov) {
// 				cover.push(cov.id)
// 			});
// 			if(cover.length == 0) {
// 				formData.covers_ids = 0;
// 			} else {
// 				formData.cover_ids = cover.join()
// 			};
// 			formData.column_id = "1";
// 			formData.covers = "";
// 			formData.state = "publish";
// 			form.option('formData', formData);
// 			return true;
// 		},
// 		items: [{
// 			itemType: "group",

// 			items: [{
// 				dataField: "title",
// 				label: {
// 					text: '文章名称'
// 				},
// 				validationRules: [{
// 					type: "required",
// 					message: "请输入名称"
// 				}]
// 			},
// 			{
// 				dataField: "content",
// 				label: {
// 					text: '文章内容'
// 				},
// 				editorType: 'dxTextArea',
// 				validationRules: [{
// 					type: "required",
// 					message: "请输入内容"
// 				}]
// 			},
// 			{
// 				dataField: "covers",
// 				name: "name",
// 				validationRules: [{
// 					type: 'required'
// 				}],
// 				editorType: "ImageUploader",
// 				label: {
// 					text: '每日签图片'
// 				},
// 				editorOptions: {
// 					imageWidth: 690,
// 					imageHeight: 518
// 				}
// 			},
// 			{
// 				dataField: "online_at",
// 				label: {
// 					text: '上线日期'
// 				},
// 				editorType: "dxDateBox",
// 				editorOptions: {
// 					displayFormat: 'yyyy-MM-dd',
// 					dateSerializationFormat: 'yyyy-MM-dd HH:mm:ss'

// 				},
// 				validationRules: [{
// 					type: "required",
// 					message: "请输入使用日期"
// 				}]

// 			}
// 			]
// 		}]
// 	}
// });

_app.App.registerCrud({
	route: 'today',
	url: $.config('apiUrl') + 'restful?_model=product-attr-group',
	placeholder: '产品',
	title: '产品属性',
	capable: {
		create: true
	},
	search: {
		mobile: 'title'
	},
	columns: [{
		dataField: 'id',
		caption: 'id'
	}, {
		dataField: 'name',
		caption: "名称"
	}, {
		dataField: 'online_at',
		caption: '日期'
	}, {
		dataField: 'id',
		caption: '操作',
		cellTemplate: function cellTemplate(con, opt) {
			$('<a>').text('编辑').click(function () {
				_app.App.getEventContext().redirect('#/today/edit/' + opt.value);
			}).appendTo(con);

			$("<span>|</span>").css('margin', '0 10px').appendTo(con);

			$('<a>').text('删除').click(function () {
				$.crudStore(API("restful?_model=wine_today_potable")).remove(opt.value).then(function (res) {
					new DevExpress.ui.notify("删除成功", "success", 1000);
					_app.App.crudGridInstance().option('dataSource').reload();
				});
			}).appendTo(con);
		}
	}],
	form: {
		readOnly: false,
		showColonAfterLabel: true,
		showValidationSummary: false,
		colCount: 1,
		beforeSubmit: function beforeSubmit(form, id) {
			var formData = form.option('formData');

			form.option('formData', formData);
			return true;
		},
		items: [{
			itemType: "group",
			items: [{
				label: { text: '属性名称' },
				dataField: 'name',
				validationRules: [{
					type: "required",
					message: "请输入内容"
				}]
			}, {
				label: { text: '添加属性' },
				dataField: 'labels',
				editorType: 'ExpertYearScoreList'
			}]

		}]
	}
});
//# sourceMappingURL=daily.js.map
