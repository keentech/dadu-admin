'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Util = function () {
	function Util() {
		_classCallCheck(this, Util);
	}

	_createClass(Util, [{
		key: 'formatXbm',
		value: function formatXbm(xbm) {
			if (xbm == '2') return '女';else if (xbm == '1') return '男';else return xbm;
		}
	}, {
		key: 'crudStore',
		value: function crudStore(url, options) {

			var SERVICE_URL = url;
			options = $.extend({
				onDataArrived: function onDataArrived(data) {
					return data;
				}
			}, options);

			return new DevExpress.data.CustomStore($.extend({}, options, {

				load: function load(loadOptions) {

					var u = new Url(SERVICE_URL);

					u.query._param = JSON.stringify(loadOptions);

					return $.getJSON(u).then(function (resp) {
						console.log("data done");
						return options.onDataArrived(resp);
					});
				},

				byKey: function byKey(key) {
					var u = new Url(SERVICE_URL);
					u.path += "/" + encodeURIComponent(key);

					return $.getJSON(u).then(function (resp) {
						return resp;
					});
				},

				insert: function insert(values) {
					console.log(values);
					return $.post(SERVICE_URL, values).always(function (resp) {
						$.crudStoreResp = resp.responseJSON;
						return resp;
					});
				},

				update: function update(key, values) {
					var u = new Url(SERVICE_URL);
					u.path += "/" + encodeURIComponent(key);

					return $.ajax({
						url: u,
						method: "PUT",
						data: values
					}).always(function (resp) {
						$.crudStoreResp = resp.responseJSON;
						return resp;
					});
				},

				remove: function remove(key) {
					var u = new Url(SERVICE_URL);
					u.path += "/" + encodeURIComponent(key);

					return $.ajax({
						url: u,
						method: "DELETE"
					});
				}

			}));
		}
	}]);

	return Util;
}();

exports.default = new Util();
//# sourceMappingURL=util.js.map
