"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AppClass = function () {
	function AppClass() {
		_classCallCheck(this, AppClass);

		this._eventContext = null;
		this._loadingPanel = null;
		this._pages = [];
		this._routes = [];
		this._user = null;

		console.log("Making App");
	}

	_createClass(AppClass, [{
		key: "login",
		value: function login(user) {
			this._user = $.extend({}, user);
		}
	}, {
		key: "crudChartInstance",
		value: function crudChartInstance() {
			return $(".data-chart").dxChart("instance");
		}
	}, {
		key: "crudGridInstance",
		value: function crudGridInstance() {
			return $(".crud-grid").dxDataGrid("instance");
		}
	}, {
		key: "getFormInstance",
		value: function getFormInstance() {
			return $("#form").dxForm("instance");
		}
	}, {
		key: "getEventContext",
		value: function getEventContext() {
			return this._eventContext;
		}
	}, {
		key: "setEventContext",
		value: function setEventContext(v) {
			this._eventContext = v;
		}
	}, {
		key: "showLoading",
		value: function showLoading(message) {
			message = message || '正在加载...';

			this._loadingPanel = $("#loadpanel").dxLoadPanel({
				shadingColor: "rgba(0,0,0,0.4)",
				position: { of: "body" },
				visible: true,
				showIndicator: true,
				showPane: true,
				shading: true,
				closeOnOutsideClick: false,
				message: message
			}).dxLoadPanel("instance");
		}
	}, {
		key: "hideLoading",
		value: function hideLoading() {
			if (this._loadingPanel !== null) {
				this._loadingPanel.hide();
				this._loadingPanel = null;
			}
		}
	}, {
		key: "registerCrud",
		value: function registerCrud(opt) {
			this.pages.push(opt);
		}
	}, {
		key: "registerRoute",
		value: function registerRoute(opt) {
			this.routes.push(opt);
		}
	}, {
		key: "pages",
		get: function get() {
			return this._pages;
		}
	}, {
		key: "routes",
		get: function get() {
			return this._routes;
		}
	}, {
		key: "user",
		get: function get() {
			return this._user;
		}
	}]);

	return AppClass;
}();

var App = exports.App = new AppClass();
//# sourceMappingURL=app.js.map
