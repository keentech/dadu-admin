'use strict';

var _app = require('./app');

var _util = require('./util');

var _util2 = _interopRequireDefault(_util);

var _page = require('./page');

var _dxExt = require('./dx-ext');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var product_show = [{}];

var formItems = {
  show: product_show,
  params: product_params
};

var initPage = function initPage(context, $container) {
  var page = new _page.Page($container);

  var id = context.params.id,
      wineStore = $.crudStore(API("restful?_model=product"));
  var formData = '',
      data = {};

  page.crudLayout({ title: id ? '编辑' : '新建' });

  page.content.html('\n    <form id="form-container">\n      <div class="form_title">\n        <div class="my_hd" style="padding: 0; border-bottom: 0;">\n          <div id="dxtabs"></div>\n        </div>\n        <div class="crud-container crud-reset">\n\t        <div class="row">\n\t          <div class="col-sm-12">\n\t            \n\t            <div id="wine-form">\n\t            </div>\n\t          </div>\n\t        </div>\n\t      \t\n\t      \t<div class="row">\n\t\t        <div class="col-sm-12 text-right" style="margin-top: 15px;">\n\t\t          <div id=\'saveData\'></div>\n\t\t          <div id="cancel"></div>\n\t\t        </div>\n\t      \t</div>\n        </div>\n\t  \t</div>\n    </form>\n\t');

  var formInstances = {};

  $(".main-title").text(id ? "编辑葡萄酒信息" : "新建葡萄酒信息");

  var tabs = [{
    text: '展示信息',
    id: "show"
  }, {
    text: "参数信息",
    id: "params",
    disabled: !id
  }];

  $.each(["show", "params"], function (index, item) {
    formInstances[item] = $("<div>").appendTo($("#wine-form")).dxForm({
      items: formItems[item],
      showColonAfterLabel: true,
      showValidationSummary: false,
      validationGroup: "crud",
      alignItemLabels: true,
      alignItemLabelsInAllGroups: true
    }).dxForm("instance");
  });

  function showTab(item) {
    for (var k in formInstances) {
      formInstances[k].option('visible', k === item);
    }
  }

  $("#dxtabs").dxTabs({
    dataSource: tabs,
    selectedIndex: 0,
    onItemClick: function onItemClick(e) {
      showTab(e.itemData.id);
    }
  });

  $("#saveData").dxButton({
    text: id ? '保存' : '新建',
    type: 'default',
    useSubmitBehavior: false
  }).click(function () {
    var selectedItem = $("#dxtabs").dxTabs("option", "selectedItem");

    if (selectedItem) {
      var result = formInstances[selectedItem.id].validate();

      if (!result.isValid) {
        var $el = result.validators[0].element();

        var offset = $el.offset();

        $('html, body').animate({
          scrollTop: offset.top - $("header.app-header").height() - 20
        });

        // } else if (validateForm(formInstances[selectedItem.id])) {
        //   saveCurrentTab();
      } else {
        saveCurrentTab();
      }
    }

    return false;
  });

  $("#cancel").dxButton({
    text: "返回",
    type: "normal",
    onClick: function onClick() {
      context.redirect("#/members");
    }
  });
};

_app.App.registerRoute({
  name: 'newproduct/:id',
  onLoad: function onLoad(context, $container) {
    initPage(context, $container);
  }
});

_app.App.registerRoute({
  name: 'newproduct',
  onLoad: function onLoad(context, $container) {
    initPage(context, $container);
  }
});
//# sourceMappingURL=products.js.map
