'use strict';

var _app = require('./app');

var _util = require('./util');

var _util2 = _interopRequireDefault(_util);

var _page = require('./page');

require('./dx-ext');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_app.App.registerRoute({
	name: "price",
	onLoad: function onLoad(context, $container) {
		var page = new _page.Page($container);
		var ID = context.params.id;
		var _reload = '';
		page.crudLayout({ title: "价格列表" });
		page.content.html('<div class="panel panel-default">\n\t\t    <div class="panel-heading font-bold">\n\t\t\t      \u5546\u54C1\u4EF7\u683C\u8BE6\u60C5\n\t\t\t    </div>\n\t\t\t    <div class="panel-body">\n\t\t\t    \t<div class="toolbar1"></div>\n\t\t\t    \t<div class="data-grid"></div>\n\t\t\t    </div>\n\t\t  \t</div>');

		var new_list = function newData() {
			var sku_id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

			var newtitle = '新建';
			if (sku_id) {
				newtitle = '修改';
			}
			$("#popups").html('<div>');

			$("<div/>").appendTo($("#popups")).dxPopup({
				width: 800,
				height: 600,
				visible: true,
				title: newtitle,
				contentTemplate: function contentTemplate(contentElement) {
					return '<div class="formData"><div class="new_tool_data"></div><div class="toolbar2" style="margin:30px auto;display:block"></div></div>';
				},
				onContentReady: function onContentReady(e) {
					var $content = e.component.content();
					var sku_attr = [];
					var origin_price = [{
						label: { text: '市场价' },
						dataField: "origin_price",
						validationRules: [{
							type: "required",
							message: '请输入市场价'
						}]
						// 					,{
						// 						label:{text:'促销价'},
						// 						dataField:"price",
						// 					
						// 						validationRules: [{
						//                     type: "required",
						//                     message:'请输入促销价'
						//                   		}]
						// 					},{
						// 						label:{text:'二档优惠价'},
						// 						dataField:"price_3_4",
						// 					
						// 						validationRules: [{
						//                     type: "required",
						//                     message:'请输入二档优惠价'
						//                   		}]
						// 					},{
						// 						label:{text:'三档优惠价'},
						// 						dataField:"price_5",
						// 					
						// 						validationRules: [{
						//                     type: "required",
						//                     message:'请输入三档优惠价'
						//                   		}]
						// 					}
					}, {
						label: { text: '库存' },
						dataField: "sku",

						validationRules: [{
							type: "required",
							message: '请输入库存'
						}]
					}];

					$.get($.config('apiUrl') + ('product/' + ID + '/labels')).then(function (res) {
						var select_list = res.data.labels;
						var items = {
							colCount: 2,
							itemType: 'group',
							items: []
						};
						function attene() {
							var resp = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

							$.each(select_list, function (i, item) {
								var _list;

								var attr = {
									attr_label_id: item.id,
									attr_value_id: ''
								};
								sku_attr.push(attr);
								var editor = {
									dataSource: item.values,
									displayExpr: 'value',
									valueExpr: "id",
									onValueChanged: function onValueChanged(e) {
										console.log(e);
										sku_attr[i].attr_value_id = e.value;
									}
								};

								if (resp) {
									resp.forEach(function (is) {
										if (is.attr_label_id == item.id) {
											editor['value'] = is.attr_value_id;
											sku_attr[i].attr_value_id = is.attr_value_id;
										}
									});
								}

								var list = (_list = {
									label: { text: item.name },
									editorType: 'dxSelectBox',
									validationRules: [{
										type: "required",
										message: '请选择' + item.name
									}]
								}, _defineProperty(_list, 'editorType', 'dxSelectBox'), _defineProperty(_list, 'editorOptions', editor), _list);
								items['items'].push(list);
							});
						}

						if (sku_id) {
							$.get($.config('apiUrl') + ('restful/' + sku_id + '?_model=product-sku')).then(function (resp) {
								var sku_data = resp.data;
								attene(sku_data.sku_attr);
								items['items'] = items['items'].concat(origin_price);

								$content.find('.new_tool_data').dxForm({
									formData: sku_data,
									showColonAfterLabel: true,
									showValidationSummary: false,
									validationGroup: "crud",
									alignItemLabels: true,
									alignItemLabelsInAllGroups: true,
									items: [items]
								});
							});
						} else {
							attene();
							items['items'] = items['items'].concat(origin_price);

							$content.find('.new_tool_data').dxForm({
								// formData:sku,
								showColonAfterLabel: true,
								showValidationSummary: false,
								validationGroup: "crud",
								alignItemLabels: true,
								alignItemLabelsInAllGroups: true,
								items: [items]
							});
						}
					});

					$content.find('.toolbar2').dxButton({
						type: 'success',
						text: newtitle,
						onClick: function onClick() {
							var data = $('.formData').find('.new_tool_data').dxForm('instance').option('formData');
							console.log(data);
							data['sku_attr'] = sku_attr;
							data['product_id'] = ID;
							if (sku_id) {
								$.ajax({
									url: $.config('apiUrl') + ('restful/' + sku_id + '?_model=product-sku'),
									data: data,
									type: 'PUT',
									success: function success(dat) {
										if (dat.code == '200' && dat.status == 'success') {
											DevExpress.ui.notify(newtitle + '成功', 'success', 1500);
											$("#popups").find('div').remove();
											_reload();
										} else {
											var message = dat.message ? dat.message : '保存失败';
											DevExpress.ui.notify(message, 'warning', 1500);
										}
									},
									error: function error(dt, te, er) {
										console.error(dt, te, er);
										DevExpress.ui.notify(newtitle + '失败', 'warning', 1500);
									}
								});
							} else {

								$.post($.config('apiUrl') + 'restful?_model=product-sku', data, function (dat, textstatus, datatype) {
									console.log(data);
									if (dat.code == '200' && dat.status == 'success') {
										DevExpress.ui.notify(newtitle + '成功', 'success', 1500);
										$("#popups").find('div').remove();
										_reload();
									}
								});
							}
						}
					});
				}
			});
		};

		var param = '{"filter":["product_id","=",' + ID + ']}';
		_reload = function reload() {
			$.get($.config('apiUrl') + 'restful?_model=product-sku&_param=' + param).then(function (res) {
				var data = res.data;

				var set_price = function set_price(id, key) {
					var value = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

					var num = prompt('请输入', value ? value : '0');
					if (num) {
						$.post($.config('apiUrl') + "state-change", {
							'changeType': 'productSku',
							'changeKey': key,
							'changeValue': num,
							'changeId': id
						}, function (res) {
							if (res.status) {
								DevExpress.ui.notify({
									message: function message() {
										_reload();
										return '修改成功';
									}
								}, "success", 1000);
							} else {
								DevExpress.ui.notify({
									message: function message() {
										return '操作失败';
									}
								}, "warning", 1000);
							}
						});
					}
				};

				var price = [{
					dataField: 'skuCode',
					caption: "编号"
				}, {
					dataField: 'desc',
					caption: '样式'
				}, {
					dataField: 'origin_price',
					caption: "市场价",
					cellTemplate: function cellTemplate($c, d) {
						var sid = d.key.id;
						$("<div>").text(d.value).css({ 'text-decoration': 'underline', 'cursor': 'pointer' }).click(function () {
							set_price(sid, 'origin_price', d.value);
						}).appendTo($c);
					}

				}, {
					dataField: 'price',
					caption: '优惠价'
					// 				cellTemplate:function($c,d){
					// 					let sid = d.key.id
					// 					$("<div>").text(d.value).css({'text-decoration':'underline','cursor':'pointer'}).click(function(){
					// 						set_price(sid,'price',d.value)
					// 					}).appendTo($c)
					// 				}

					// 			,{
					// 				dataField:'price_3_4',
					// 				caption:'二档优惠价',
					// 				cellTemplate:function($c,d){
					// 					let sid = d.key.id
					// 					$("<div>").text(d.value).css({'text-decoration':'underline','cursor':'pointer'}).click(function(){
					// 						set_price(sid,'price_3_4',d.value)
					// 					}).appendTo($c)
					// 				}
					// 				
					// 			},{
					// 				dataField:'price_5',
					// 				caption:'三档优惠价',
					// 				cellTemplate:function($c,d){
					// 					let sid = d.key.id
					// 					$("<div>").text(d.value).css({'text-decoration':'underline','cursor':'pointer'}).click(function(){
					// 						set_price(sid,'price_5',d.value)
					// 					}).appendTo($c)
					// 				}
					// 				
					// 			},
				}, {
					dataField: "consume_price",
					caption: '积分价'
				}, {
					dataField: 'sku',
					caption: "库存",
					cellTemplate: function cellTemplate($c, d) {
						var sid = d.key.id;
						$("<div>").text(d.value).css({ 'text-decoration': 'underline', 'cursor': 'pointer' }).click(function () {
							set_price(sid, 'sku', d.value);
						}).appendTo($c);
					}
				}, {
					dataField: "id",
					caption: '操作',
					cellTemplate: function cellTemplate($c, d) {
						console.log(d);
						$('<div>').text('删除').css({ 'text-decoration': 'underline', 'cursor': 'pointer' }).click(function () {
							$.ajax({
								url: $.config('apiUrl') + ('restful/' + d.value + '?_model=product-sku'),
								type: 'delete',
								success: function success(data) {
									console.log(data);
									if (data.status == 'success') {
										DevExpress.ui.notify('删除成功', 'success', 1500);
										$("#popups").find('div').remove();
										_reload();
									} else {
										var message = dat.message ? dat.message : '保存失败';
										DevExpress.ui.notify(message, 'warning', 1500);
									}
								},
								error: function error(er, dx, ht) {
									DevExpress.ui.notify('未知错误', 'warning', 1500);
									console.error(er, dx, ht);
								}
							});
						}).appendTo($c);
					}
					// cellTemplate:function($c,d){
					// 	console.log(d)
					// 	$('<div>').text('修改').click(function(){
					// 		new_list(d.value)
					// 	}).appendTo($c)
					// }
				}];

				page.content.find('.data-grid').dxDataGrid({
					dataSource: data,
					columns: price
				});

				page.content.find('.toolbar1').dxToolbar({
					items: [{
						location: 'before',
						widget: 'dxButton',
						options: {
							text: '增加',
							onClick: function onClick(e) {
								new_list();
							}
						}
					}]
				});
			});
		};

		_reload();
	}
});

// App.registerRoute({
// 	name:"data/dealer",
// 	onLoad:function(context,$container){
// 		let page=new Page($container);

// 		let origin=context.params.id;
// 		let origin_name = context.params.name;
// 		let columns="";
// 		if(origin){
// 			page.crudLayout({title:"经销商管理"});
// 			page.content.html(`<div class="panel panel-default">
// 		    <div class="panel-heading font-bold">
// 			      进口商${origin_name}
// 			    </div>
// 			    <div class="panel-body">
// 			    	<div class="toolbar1"></div>
// 			    	<div class="data-grid"></div>
// 			    </div>
// 		  	</div>`);	
// 			let filter = [['importer_id','=',origin]]

// 			let store = $.crudStore(API('restful?_model=sys-importer-dealer'), {
// 					onLoading: function(loadOptions) {
// 						loadOptions.filter = filter;
// 					}
// 				});
// 			columns = {
// 				dataSource:store,
// 				columns:[{
// 					caption:'id',
// 					dataField:'id'
// 				},{
// 					caption:'经销商名称',
// 					dataField:'dealer.name'
// 				},{
// 					caption:'备注',
// 					dataField:'remark'
// 				},{
// 					caption:'地址',
// 					dataField:'dealer.area_name'
// 				},{
// 					caption:'操作',
// 					dataField:'dealer.id',
// 					cellTemplate:function(container,options){
// 						$("<a>").text('编辑').css({"text-decoration":"underline"}).click(function(){
// 							App.getEventContext().redirect(`#/data/new_dealer?openid=${options.data.dealer.id}&id=${origin}&name=${origin_name}`)
// 						}).appendTo(container)
// 					}
// 				}]
// 			}

// 			page.content.find('.toolbar1').dxToolbar({
// 			items:[{
// 				widget:'dxButton',
// 			location:"before",
// 			options:{
// 				text:"增加经销商",
// 				onClick:function(e){
// 						App.getEventContext().redirect(`#/data/new_dealer?id=${origin}&name=${origin_name}&dataset=${origin}`); 
// 					}
// 				}
// 			}]
// 		});

// 		}else{ 

// 		page.crudLayout({title:'进口商管理'});
// 		page.content.html(`<div class="panel panel-default">
// 		    <div class="panel-heading font-bold">
// 			      进口商管理
// 			    </div>
// 			    <div class="panel-body">
// 			    	<div class="toolbar1"></div>
// 			    	<div class="data-grid"></div>
// 			    </div>
// 		  	</div>`);

// 		columns = {
// 			dataSource:$.crudStore(API("restful?_model=sys-importer")),
// 			columns:[{
// 				caption:'id',
// 				dataField:'id'
// 			},{
// 				caption:'进口商名称',
// 				dataField:'name'
// 			},{
// 				caption:'备注',
// 				dataField:'remark'
// 			},{
// 				caption:'经销商',
// 				dataField:'dealers_name'
// 			},{
// 				caption:'操作',
// 				dataField:"id",
// 				cellTemplate:function(container,options){
// 					let $html = $('<div><a>编辑</a> |<a>添加经销商</a></div>')
// 					$html.find("a").css({"text-decoration":'underline'})

// 					$html.find('a:first').click(function(){
// 						App.getEventContext().redirect(`#/data/new_dealer?importer=${options.value}`);
// 					})

// 					$html.find('a:last').click(function(){
// 						App.getEventContext().redirect(`#/data/dealer?id=${options.value}&name=${options.data.name}`);
// 					})

// 					$html.appendTo(container)

// 				}
// 			}]
// 		}

// 			page.content.find('.toolbar1').dxToolbar({
// 			items:[{
// 				widget:'dxButton',
// 			location:"before",
// 			options:{
// 				text:"增加",
// 				onClick:function(e){
// 						App.getEventContext().redirect('#/data/new_dealer'); 
// 					}
// 				}
// 			}]
// 		});

// 			}


// 	 page.content.find('.data-grid').dxDataGrid(columns)

// 	}
// })


// App.registerRoute({
// 	name:'data/new_dealer',
// 	onLoad:function(context,$container){
// 		let page=new Page($container);
// 		let origin=context.params.dataset;
// 		let origin_id=context.params.openid;
// 		let origin_name = context.params.name;
// 		let ID = context.params.id;
// 		let importer = context.params.importer;

// 		console.log(importer);

// 		if(origin || origin_id || ID){
// 			page.crudLayout({title:'经销商'});

// 			page.content.html(`<div class="panel panel-default">
// 			    <div class="panel-heading font-bold">
// 			      经销商管理
// 			    </div>
// 			    <div class="panel-body">
// 			    	<div class="data-grid"></div>
// 			    </div>
// 		  	</div>`);

// 			page.content.find(".data-grid").css({"padding":"20px"});

// 			let store = '';


// 			let items = [{
// 				colCount:2,
// 				itemType:'group',
// 				items:[{
// 					label:{text:'经销商名称'},
// 					dataField:'name'
// 				},{
// 					label:{text:' '},
// 					editorType:'dxSelectBox',
// 					editorOptions:{
// 						dataSource:$.crudStore(API("restful?_model=sys-dealer")),
// 						displayExpr:'name',
// 						valueExpr:'id',
// 						showClearButton:true,
// 						searchEnabled:true,
// 						onValueChanged:function(e){

// 							let instance = page.content.find(".data-grid").dxForm('instance')
// 							$.get(API(`restful/${e.value}?_model=sys-dealer`)).then(res =>{
// 								instance.option('formData',res.data);
// 								let area_code=res.data.area_code;
// 								if(area_code){ 
// 								let country = area_code.slice(0,2)+'0000',city=area_code.slice(0,4)+'00' 
// 								$.get(API("areas")).then(resp =>{
// 										let data =  resp;

// 									page.content.find(".select_country").dxSelectBox("instance").option("dataSource",resp);
// 									page.content.find(".select_country").dxSelectBox("instance").option("value",country);
// 									data.forEach( item =>{
// 										if(item.value == country){

// 											page.content.find(".select_city").dxSelectBox("instance").option("dataSource",item.child);
// 											page.content.find(".select_city").dxSelectBox("instance").option("value",city);
// 											return item.child.forEach(it=>{
// 												if(it .value == city)
// 												page.content.find(".select_region").dxSelectBox("instance").option("dataSource",it.child);
// 												page.content.find(".select_region").dxSelectBox("instance").option("value",area_code);

// 													})
// 												}
// 											})

// 								})
// 								};
// 							})

// 						}
// 					}

// 				}]
// 			},{
// 				label:{text:'地址'},
// 				itemType:'group',
// 				colCount:3,
// 				items:[{
// 					label:{text:'省级'},
// 					template:function(data){
// 					return $("<div class='select_country'>").dxSelectBox({
// 								placeholder:'请选择省级',
// 								dataSource:$.crudStore(API("areas")),
// 								displayExpr:"name",
// 								valueExpr:'value',
// 								searchEnabled:true,
// 								onSelectionChanged:function(e){

// 									if(!e.selectedItem){return }
// 							page.content.find(".select_city").dxSelectBox("instance").option('dataSource',e.selectedItem.child);
// 							page.content.find(".select_region").dxSelectBox("instance").option("dataSource",[])
// 								}
// 							})
// 					}
// 				},{
// 					label:{text:'市级'},
// 					template:function(data){
// 					return	$("<div class='select_city'>").dxSelectBox({placeholder:'请选择市级',displayExpr:"name",
// 								valueExpr:'value',
// 								searchEnabled:true,
// 								noDataText:'请先选择前一级',
// 								onSelectionChanged:function(e){
// 									if(!e.selectedItem){return }
// 									page.content.find(".select_region").dxSelectBox("instance").option("dataSource",e.selectedItem.child)
// 								}
// 							})
// 					}
// 				},{
// 					label:{text:'地区'},
// 					template:function(data){
// 					return	$("<div class='select_region'>").dxSelectBox({placeholder:'请选择区域',displayExpr:"name",
// 								valueExpr:'value',
// 								searchEnabled:true,
// 								noDataText:'请先选择前一级',
// 								onSelectionChanged:function(e){

// 									if(!e.selectedItem){return }
// 									page.content.find(".data-grid").dxForm("instance").option('formData.area_code',e.selectedItem.value);
// 								}
// 							})
// 					}
// 				}]
// 			},{
// 				label:{text:'具体门牌号'},
// 				dataField:'detail'
// 			},{
// 				label:{text:'联系电话'},
// 				dataField:'tel'
// 			},
// 			{
// 				editorType:'dxButton',
// 				editorOptions:{
// 					text:function(){if(origin_id){return '修改'}else{return '创建'}},
// 					type:'success',
// 					useSubmitBehavior:false,
// 					onClick:function(e){
// 						let data=page.content.find(".data-grid").dxForm("instance").option('formData');
// 						data.importer_id=origin;
// 						if(origin_id){
// 							$.crudStore(API(`restful?_model=sys-dealer`)).update(origin_id,data).then((index,res)=>{

// 							DevExpress.ui.notify({message:"修改成功"},"success",1500) ;

// 							setTimeout(function(){window.history.go(-1)},1500)

// 						}).fail(res=>{
// 							DevExpress.ui.notify({message:"请检查"},"warning",2000) 
// 						})

// 						return
// 						}
// 						$.ajax({url:API('restful?_model=sys-dealer'),method:'post',data:data}).always((res)=>{
// 							if(res.status == 'success'){ 
// 							DevExpress.ui.notify({message:"创建成功"},"success",1500) ;

// 							setTimeout(function(){window.history.go(-1);},1500)

// 						}else{  
// 								DevExpress.ui.notify({message:function(){
// 									let text = '错误：'+res.responseJSON.message;
// 									return text
// 								}},"warning",1500) 
// 							}
// 						})
// 					}
// 				}
// 			}
// 			];

// 			if(origin_id){
// 				store = $.crudStore(API(`restful/${origin_id}?_model=sys-dealer`));
// 				$.get(API(`restful/${origin_id}?_model=sys-dealer`)).then(res =>{
// 					page.content.find(".data-grid").dxForm({
// 					formData:res.data,
// 					showColonAfterLabel: true,
// 					colCount:1,
// 					items:items
// 					})


// 				let area_code=res.data.area_code;

// 				let country = area_code.slice(0,2)+'0000',city=area_code.slice(0,4)+'00' ;
// 								$.get(API("areas")).then(resp =>{
// 									let data =  resp;
// 									page.content.find(".select_country").dxSelectBox("instance").option("dataSource",resp);
// 									page.content.find(".select_country").dxSelectBox("instance").option("value",country);
// 									data.forEach( item =>{
// 										if(item.value == country){

// 											page.content.find(".select_city").dxSelectBox("instance").option("dataSource",item.child);
// 											page.content.find(".select_city").dxSelectBox("instance").option("value",city);
// 											return item.child.forEach(it=>{
// 												if(it .value == city)
// 												page.content.find(".select_region").dxSelectBox("instance").option("dataSource",it.child);
// 												page.content.find(".select_region").dxSelectBox("instance").option("value",area_code);

// 													})
// 												}
// 											})
// 									})
// 								})


// 			}else{
// 				page.content.find(".data-grid").dxForm({
// 				dataSource:new DevExpress.data.CustomStore({
//             	store:$.crudStore(API('restful?_model=sys-dealer'))
//         			  }),
// 				showColonAfterLabel: true,
// 				colCount:1,
// 				items:items
// 					})
// 			}


// 			return 
// 		}


// 		let items = [{
// 				label:{text:'进口商名称'},
// 				dataField:'name'
// 			},{
// 				label:{text:'地址'},
// 				itemType:'group',
// 				colCount:3,
// 				items:[{
// 					label:{text:'省级'},
// 					template:function(data){
// 					return $("<div class='select_country'>").dxSelectBox({
// 								placeholder:'请选择省级',
// 								dataSource:$.crudStore(API("areas")),
// 								displayExpr:"name",
// 								valueExpr:'value',
// 								searchEnabled:true,
// 								onSelectionChanged:function(e){

// 									if(!e.selectedItem){return }
// 							page.content.find(".select_city").dxSelectBox("instance").option('dataSource',e.selectedItem.child);
// 							page.content.find(".select_region").dxSelectBox("instance").option("dataSource",[])
// 								}
// 							})
// 					}
// 				},{
// 					label:{text:'市级'},
// 					template:function(data){
// 					return	$("<div class='select_city'>").dxSelectBox({placeholder:'请选择市级',displayExpr:"name",
// 								valueExpr:'value',
// 								searchEnabled:true,
// 								noDataText:'请先选择前一级',
// 								onSelectionChanged:function(e){
// 									if(!e.selectedItem){return }
// 									page.content.find(".select_region").dxSelectBox("instance").option("dataSource",e.selectedItem.child)
// 								}
// 							})
// 					}
// 				},{
// 					label:{text:'地区'},
// 					template:function(data){
// 					return	$("<div class='select_region'>").dxSelectBox({placeholder:'请选择区域',displayExpr:"name",
// 								valueExpr:'value',
// 								searchEnabled:true,
// 								noDataText:'请先选择前一级',
// 								onValueChanged:function(e){
// 									page.content.find(".data-grid").dxForm("instance").option('formData.area_code',e.value);
// 								}
// 							})
// 					}
// 				}]
// 			},{
// 				label:{text:'具体门牌号'},
// 				dataField:'detail'
// 			},{
// 				label:{text:'联系人'},
// 				dataField:'linkman'
// 			},{
// 				label:{text:"电话"},
// 				dataField:'tel'
// 			},{
// 				label:{text:"邮箱"},
// 				dataField:'email'
// 			},{
// 				editorType:'dxButton',
// 				editorOptions:{
// 					text:function(){
// 						if(importer){
// 							return '修改'
// 						}else{
// 							return '创建'
// 						}},
// 					type:'success',
// 					useSubmitBehavior:false,
// 					onClick:function(e){

// 						let data=page.content.find(".data-grid").dxForm("instance").option('formData');
// 						let URL = 'restful?_model=sys-importer';
// 						let method = 'post';
// 						let text = '创建';
// 						if(importer){ 
// 							URL = `restful/${importer}?_model=sys-importer`;
// 							method = 'PUT';
// 							text = '修改'
// 						}

// 						$.ajax({url:API(URL),method:method,data:data}).always((res)=>{
// 							if(res.status == 'success'){ 
// 							DevExpress.ui.notify({message:text+"成功"},"success",1500) ;


// 							setTimeout(function(){window.history.go(-1);},1500)

// 						}else{
// 								DevExpress.ui.notify({message:function(){
// 									let text = '错误：'+res.responseJSON.message;
// 									return text
// 								}},"warning",1500)
// 							}
// 						}).fail(res=>{
// 							DevExpress.ui.notify({message:"请检查"},"warning",2000) 
// 						})
// 					}
// 				}
// 			}]


// 		if(importer){
// 			page.crudLayout({title:'编辑进口商'});

// 			page.content.html(`<div class="panel panel-default">
// 			    <div class="panel-heading font-bold">
// 			      进口商管理
// 			    </div>
// 			    <div class="panel-body">
// 			    	<div class="data-grid"></div>
// 			    </div>
// 		  	</div>`);

// 		page.content.find(".data-grid").css({"padding":"20px"});


// 			$.get(API(`restful/${importer}?_model=sys-importer`)).then(res => {

// 			page.content.find(".data-grid").dxForm({
// 			formData:res.data,
// 			showColonAfterLabel: true,
// 			colCount:1,
// 			items:items
// 		})
// 			let area_code=res.data.area_code;

// 				let country = area_code.slice(0,2)+'0000',city=area_code.slice(0,4)+'00' ;
// 								$.get(API("areas")).then(resp =>{
// 									let data =  resp;
// 									page.content.find(".select_country").dxSelectBox("instance").option("dataSource",resp);
// 									page.content.find(".select_country").dxSelectBox("instance").option("value",country);
// 									data.forEach( item =>{
// 										if(item.value == country){
// 											page.content.find(".select_city").dxSelectBox("instance").option("dataSource",item.child);
// 											page.content.find(".select_city").dxSelectBox("instance").option("value",city);
// 											return item.child.forEach(it=>{
// 												if(it .value == city)
// 												page.content.find(".select_region").dxSelectBox("instance").option("dataSource",it.child);
// 												page.content.find(".select_region").dxSelectBox("instance").option("value",area_code);

// 													})
// 												}
// 											})
// 									})

// 			})

// 		}else{
// 			page.crudLayout({title:'新建进口商'});

// 			page.content.html(`<div class="panel panel-default">
// 			    <div class="panel-heading font-bold">
// 			      进口商管理
// 			    </div>
// 			    <div class="panel-body">
// 			    	<div class="data-grid"></div>
// 			    </div>
// 		  	</div>`);

// 		page.content.find(".data-grid").css({"padding":"20px"});

// 			page.content.find(".data-grid").dxForm({
// 			dataSource:new DevExpress.data.CustomStore({
//             store: $.crudStore(API('restful?_model=sys-importer'))
//           }),
// 			showColonAfterLabel: true,
// 			colCount:1,
// 			items:items
// 		})
// 		}


// 	}
// })
//# sourceMappingURL=dealer.js.map
