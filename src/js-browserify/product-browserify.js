(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AppClass = function () {
	function AppClass() {
		_classCallCheck(this, AppClass);

		this._eventContext = null;
		this._loadingPanel = null;
		this._pages = [];
		this._routes = [];
		this._user = null;

		console.log("Making App");
	}

	_createClass(AppClass, [{
		key: "login",
		value: function login(user) {
			this._user = $.extend({}, user);
		}
	}, {
		key: "crudChartInstance",
		value: function crudChartInstance() {
			return $(".data-chart").dxChart("instance");
		}
	}, {
		key: "crudGridInstance",
		value: function crudGridInstance() {
			return $(".crud-grid").dxDataGrid("instance");
		}
	}, {
		key: "getFormInstance",
		value: function getFormInstance() {
			return $("#form").dxForm("instance");
		}
	}, {
		key: "getEventContext",
		value: function getEventContext() {
			return this._eventContext;
		}
	}, {
		key: "setEventContext",
		value: function setEventContext(v) {
			this._eventContext = v;
		}
	}, {
		key: "showLoading",
		value: function showLoading(message) {
			message = message || '正在加载...';

			this._loadingPanel = $("#loadpanel").dxLoadPanel({
				shadingColor: "rgba(0,0,0,0.4)",
				position: { of: "body" },
				visible: true,
				showIndicator: true,
				showPane: true,
				shading: true,
				closeOnOutsideClick: false,
				message: message
			}).dxLoadPanel("instance");
		}
	}, {
		key: "hideLoading",
		value: function hideLoading() {
			if (this._loadingPanel !== null) {
				this._loadingPanel.hide();
				this._loadingPanel = null;
			}
		}
	}, {
		key: "registerCrud",
		value: function registerCrud(opt) {
			this.pages.push(opt);
		}
	}, {
		key: "registerRoute",
		value: function registerRoute(opt) {
			this.routes.push(opt);
		}
	}, {
		key: "pages",
		get: function get() {
			return this._pages;
		}
	}, {
		key: "routes",
		get: function get() {
			return this._routes;
		}
	}, {
		key: "user",
		get: function get() {
			return this._user;
		}
	}]);

	return AppClass;
}();

var App = exports.App = new AppClass();


},{}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

jQuery.fn.extend({

	crudForm: function crudForm(options) {
		if ((typeof options === "undefined" ? "undefined" : _typeof(options)) === 'object') {}
	},

	dxInit: function dxInit() {
		$("[dx-toggle]", $(this)).each(function () {
			if ($(this).hasClass("dx-widget")) {
				return true;
			}

			var type = $(this).attr("dx-toggle"),
			    options = $(this).attr("dx-options") || "{}";

			try {
				options = JSON.parse(options);
			} catch (ex) {

				options = {};
			}

			switch (type) {
				case "button":
					$(this).dxButton(options);
					break;

				case "select":
					$(this).dxSelectBox(options);
					break;

				case "text":
					$(this).dxTextBox(options);
					break;

				case "textarea":
					$(this).dxTextArea(options);
					break;

				case "number":
					$(this).dxNumberBox(options);
					break;

				case "check":
					$(this).dxCheckBox($.extend({
						text: $(this).text()
					}, options));
					break;

				default:
					console.log("Error: unsupported type " + type);
					break;
			}
		});

		return $(this);
	},
	dxDelete: function dxDelete(url, option) {
		$("<a>").text("删除").click(function () {
			var id = option.key.id;
			DevExpress.ui.dialog.confirm("确定删除吗?", "请确认").done(function (sele) {

				if (sele) {

					$.ajax({
						type: "delete",
						url: $.config('apiUrl') + "restful/" + id + "?_model=" + url
					}).then(function (a) {

						if (a.status = "success") {

							DevExpress.ui.notify({
								message: function message() {

									var gridComponent = $("#content .crud-grid").dxDataGrid('instance');

									gridComponent.option('dataSource').load();

									return "成功删除";
								}
							}, "success", 1000);
						} else {

							DevExpress.ui.notify({

								message: function message() {

									return "操作失败";
								}
							}, "warning", 1000);
						}
					});
				}
			});
		}).appendTo(this);
		return this;
	},
	dxUpdown: function dxUpdown(url, option) {
		var text = option.value == 'yes' ? '下线' : '上线',
		    off = option.value == 'yes' ? 'no' : 'yes';

		$('<a>').text(text).css({
			'text-decoration': 'underline'
		}).click(function () {
			$.post($.config('apiUrl') + "state-change", {
				'changeType': url,
				'changeKey': 'online',
				'changeValue': off,
				'changeId': option.key.id
			}, function (d) {
				if (d.status) {
					DevExpress.ui.notify({
						message: function message() {
							var gridComponent = $("#content .crud-grid").dxDataGrid('instance');
							gridComponent.option('dataSource').load();
							return off == 'yes' ? '上线成功' : '下线成功';
						}
					}, "success", 1000);
				} else {
					DevExpress.ui.notify({
						message: function message() {
							return '操作失败';
						}
					}, "warning", 1000);
				}
			});
		}).appendTo(this);
		return this;
	},
	dxWeight: function dxWeight(url, option) {

		$('<a>').text(option.value == null ? '无' : option.value).css({
			'text-decoration': 'underline'
		}).click(function () {
			var number = prompt('请输入权重', option.value);
			if (number == null) {
				return;
			};
			if (number >= 0 && number <= 100) {
				$.post($.config('apiUrl') + "state-change", {
					'changeType': url,
					'changeKey': 'weight',
					'changeValue': number,
					'changeId': option.key.id
				}, function (d) {
					if (d.status) {
						DevExpress.ui.notify({
							message: function message() {
								var gridComponent = $("#content .crud-grid").dxDataGrid('instance');
								gridComponent.option('dataSource').load();
								return '修改成功';
							}
						}, "success", 1000);
					} else {
						DevExpress.ui.notify({
							message: function message() {
								return '操作失败';
							}
						}, "warning", 1000);
					}
				});
			} else {
				DevExpress.ui.notify({
					message: '请输入正确的值'
				}, 'warning', 1000);
			}
		}).appendTo(this);
		return this;
	},
	dxsetsku: function dxsetsku(url, option) {
		$("<a>").text(option.value ? option.value : '0').css({
			'text-decoration': 'underline'
		}).click(function () {
			var num = prompt('请输入库存', option.value ? option.value : '0');
			if (num >= 0) {
				$.post($.config('apiUrl') + "state-change", {
					'changeType': url,
					'changeKey': 'sku',
					'changeValue': num,
					'changeId': option.key.id
				}, function (res) {
					if (res.status) {
						DevExpress.ui.notify({
							message: function message() {
								var gridComponent = $("#content .crud-grid").dxDataGrid('instance');
								gridComponent.option('dataSource').load();
								return '修改成功';
							}
						}, "success", 1000);
					} else {
						DevExpress.ui.notify({
							message: function message() {
								return '操作失败';
							}
						}, "warning", 1000);
					}
				});
			} else {
				DevExpress.ui.notify({
					message: '请输入正确的值'
				}, 'warning', 1000);
			}
		}).appendTo(this);
		return this;
	}
});

var DxExtInstance = klass(function ($container, config, opt) {
	this.$container = $container;
	this.opt = $.extend({}, opt);
	this.config = $.extend({}, config);
	this.user = {};

	this.config.init.call(this, this.$container, opt);

	this.setValue(opt.value || undefined);
}).statics({
	validate: function validate(formInstance) {
		var editors = [];

		function _check(o) {
			if (o.dataField) {
				var editor = formInstance.getEditor(o.dataField);
				if (editor && typeof editor.isExtInstance === 'function') {
					editors.push(editor);
				}
			} else if (o.items) {
				$.each(o.items, function (i, item) {
					_check(item);
				});
			}
		}

		_check({
			items: formInstance.option('items')
		});

		var result = true;
		$.each(editors, function (i, e) {
			if (!e.validate()) result = false;
		});

		return result;
	},

	define: function define(config) {
		var obj = {};
		obj[config.name] = function () {
			var instance = $(this).data('dx-ext-instance');

			if (arguments.length > 0 && _typeof(arguments[0]) === 'object') {

				if (!instance) {
					instance = new DxExtInstance($(this), config, arguments[0]);
					$(this).data('dx-ext-instance', instance);
				}

				return $(this);
			} else if (arguments.length > 0 && typeof arguments[0] === 'string') {
				var args = [];
				for (var i = 1; i < arguments.length; i++) {
					args.push(arguments[i]);
				}

				console.log("call " + arguments[0]);
				console.log(args);

				return instance[arguments[0]].apply(instance, args);
			} else {
				return undefined;
			}
		};

		jQuery.fn.extend(obj);
	}
}).methods({
	isExtInstance: function isExtInstance() {
		return true;
	},

	validate: function validate() {
		if (this.config.validate) {
			return this.config.validate.call(this);
		} else {
			return true;
		}
	},

	getValue: function getValue() {
		if (this.config.onGetValue) return this.config.onGetValue.call(this);
		return this.value;
	},

	getOption: function getOption() {
		return this.opt;
	},

	notifyValueChanged: function notifyValueChanged() {
		this.$container.trigger("dxExtValueChanged");
	},

	setValue: function setValue(v) {
		this.value = v;
		this.notifyValueChanged();
		this.render();
	},

	render: function render() {
		this.config.render && this.config.render.call(this, this.$container);
	},

	setAria: function setAria() {
		return undefined;
	},

	option: function option(k, v) {

		if (k === 'value' && v !== undefined) {
			return this.setValue(v);
		} else if (k === 'value') {
			if (this.config.beforeGetValue) {
				this.config.beforeGetValue.call(this);
			}
			return this.getValue();
		}

		if (v === undefined) {
			console.log("query option " + k);

			return this.opt[k];
		} else {
			var orig = this.opt[k];
			this.opt[k] = v;

			if (this.config.onOptionChanged) {
				this.config.onOptionChanged.call(this, {
					option: k,
					value: v,
					oldValue: orig
				});
			}
		}
	},

	element: function element() {
		return this.$container;
	},

	instance: function instance() {
		return this;
	},

	on: function on(evt, fn) {
		var _this = this;

		console.log("on " + evt);

		if (evt === 'valueChanged') {
			this.$container.on("dxExtValueChanged", function () {
				fn.call(_this, {
					component: _this,
					value: _this.value
				});
			});
		}
	},

	reset: function reset() {
		console.log("call reset");
	}
});

DxExtInstance.define({
	name: 'propertyList',
	init: function init($container, option) {},

	render: function render($container) {
		var _this2 = this;

		var _this = this;
		var DATA = this.getValue() || [];

		var te1 = "<div class='row' style='margin-bottom:10px'><div class='col-sm-12' >" + "<div class='pull-left' style='margin-right:30px'><div class='property'></div></div>" + "</div></div>" + "<div class='row ' style='margin-bottom:10px'><div class='col-sm-12 propertydata' >" + "</div></div>";

		var te4 = "<div class='pull-left' ><div class='property_add'></div></div>";

		var post_list = function post_list(ID) {
			console.log(_this2.option);

			$.get($.config('apiUrl') + ("restful/" + ID + "?_model=product-attr-group")).then(function (res) {
				var labels = res.data.labels;
				var formItem = [];

				DATA.forEach(function (it) {
					var form = {
						colCount: 5,
						itemType: 'group',
						items: []
					};

					$.each(labels, function (i, items) {

						var item = {
							label: { text: items.name },
							dataField: "values",
							validationRules: [{
								type: "required",
								message: '请选择' + items.name
							}],
							editorType: 'dxSelectBox',
							editorOptions: {
								dataSource: items.values,
								valueExpr: 'id',
								displayExpr: 'value',
								onValueChanged: function onValueChanged(e) {
									console.log(e);
								}
							}
						};

						form['items'].push(item);
					});

					var origin_price = [{
						label: { text: '原价' },
						dataField: "origin_price",
						value: it.origin_price,
						validationRules: [{
							type: "required",
							message: '请输入原价'
						}]

					}, {
						label: { text: '促销价' },
						dataField: "price",
						value: it.price,
						validationRules: [{
							type: "required",
							message: '请输入促销价'
						}]
					}, {
						label: { text: '库存' },
						dataField: "sku",
						value: it.sku,
						validationRules: [{
							type: "required",
							message: '请输入库存'
						}]
					}, {
						editorType: 'dxButton',
						editorOptions: {
							text: '删除',
							onClick: function onClick() {}
						}
					}];

					form['items'] = form['items'].concat(origin_price);

					formItem.push(form);
				});

				console.log(formItem);
				$container.find('.propertydata').dxForm({
					showColonAfterLabel: true,
					showValidationSummary: false,
					validationGroup: "crud",
					alignItemLabels: true,
					alignItemLabelsInAllGroups: true,
					items: formItem
				});

				$container.find('.property_add').dxButton({
					text: "增加",
					onClick: function onClick() {
						var post_data = {
							origin_price: '',
							price: '',
							sku: '',
							labels: []
						};
						var label = {
							value: ''
						};
						labels.forEach(function (its) {
							post_data['labels'].push(label['attr_label_id'] = its.id);
						});
						_this.setValue(DATA.push(post_data));
						post_list(ID);
					}
				});
			});
		};

		$container.css({ padding: '10px 0' });

		var te3 = "<div class='row' style='margin-bottom:10px'><div class='property_list'></div></div>" + te1 + te4;

		$container.html(te3);

		$container.find('.property_list').dxSelectBox({
			dataSource: $.crudStore(API('restful?_model=product-attr-group')),
			displayExpr: 'name',
			valueExpr: 'id',
			onValueChanged: function onValueChanged(e) {
				var ID = e.value;

				post_list(ID);
			}

		});
	}
});

DxExtInstance.define({
	name: 'imagesdetail',
	init: function init($container, option) {
		var _this = this;

		option = $.extend({}, {
			single: false
		}, option);
		console.log(option);
		$container.css({ padding: '10px 0' });
		$container.html("<div class='images'></div><div class='file image-uploader'></div>");

		var labelText = '';
		if (option.labelText) {
			labelText = option.labelText;
		} else if (option.imageWidth && option.imageHeight) {
			labelText = '建议宽高比是' + option.imageWidth + 'px x ' + option.imageHeight + 'px。';
		}

		$container.find(".file").dxFileUploader({
			selectButtonText: "上传",
			labelText: labelText,
			name: 'file',
			accept: "image/*",
			uploadMode: "instantly",
			uploadUrl: $.config('apiUrl') + "file-upload",
			onUploaded: function onUploaded(e) {
				var pat = JSON.parse(e.request.response).data;

				var value = _this.getValue();

				if (!value) {
					value = option.single ? "" : [];
				};
				console.log(value);
				if (option.single) {
					value = pat.imgSrv;
				} else {
					value.push(pat.imgSrv);
				}
				console.log(value);

				_this.setValue(value);
			}
		});

		// $container.find(".file .dx-button-content").html('<i class="iconfont">&#xe623;</i>');
	},

	render: function render($container) {
		// function imageUrl(url) {
		// 	if(!url.match(/imageView2/)) {
		// 		url += "?imageView2/1/w/100/h/57";
		// 	}

		// 	return url;
		// }

		var _this = this,
		    value = this.getValue();

		var $images = $container.find(".images");
		$images.html("");

		if (value) {
			var option = this.getOption();

			if (option.single) value = [value];
			console.log(option);
			$.each(value, function (i, dat) {
				$("<div>").append($("<img>").attr("src", dat)).append($("<span>").text("删除").click(function () {

					if (!option.single) {
						value.splice(i, 1);
						_this.setValue(value);
					} else {
						value = "";
						_this.setValue(value);
					}

					$(this).parent().remove();
				})).appendTo($images);
			});
		}
	}
});

DxExtInstance.define({
	name: 'ImageUploader',
	init: function init($container, option) {
		var _this = this;

		option = $.extend({}, {
			single: false
		}, option);

		$container.css({ padding: '10px 0' });
		$container.html("<div class='images'></div><div class='file image-uploader'></div>");

		var labelText = '';
		if (option.labelText) {
			labelText = option.labelText;
		} else if (option.imageWidth && option.imageHeight) {
			labelText = '建议宽高比是' + option.imageWidth + 'px x ' + option.imageHeight + 'px。';
		}

		$container.find(".file").dxFileUploader({
			selectButtonText: "上传",
			labelText: labelText,
			name: 'file',
			accept: "image/*",
			uploadMode: "instantly",
			uploadUrl: $.config('apiUrl') + "file-upload",
			onUploaded: function onUploaded(e) {
				var pat = JSON.parse(e.request.response).data;

				var value = _this.getValue();

				if (value === undefined) {
					value = option.single ? "" : [];
				};

				if (option.single) {
					value = pat.imgSrv;
				} else {
					value.push(pat);
				}

				_this.setValue(value);
			}
		});

		// $container.find(".file .dx-button-content").html('<i class="iconfont">&#xe623;</i>');
	},

	render: function render($container) {
		function imageUrl(url) {
			if (!url.match(/imageView2/)) {
				url += "?imageView2/1/w/100/h/57";
			}

			return url;
		}

		var _this = this,
		    value = this.getValue();

		var $images = $container.find(".images");
		$images.html("");

		if (value) {
			var option = this.getOption();

			if (option.single) value = [value];

			$.each(value, function (i, dat) {
				$("<div>").append($("<img>").attr("src", imageUrl(option.single ? dat : dat.imgSrv))).append($("<span>").text("删除").click(function () {

					if (!option.single) {
						value.splice(i, 1);
						_this.setValue(value);
					} else {
						value = "";
						_this.setValue(value);
					}

					$(this).parent().remove();
				})).appendTo($images);
			});
		}
	}
});

DxExtInstance.define({
	name: 'VideoUploader',
	init: function init($container, option) {
		var _this = this;

		option = $.extend({}, {
			single: false
		}, option);

		$container.html("<div class='video-url'></div><div class='file'></div>");

		$container.find(".file").dxFileUploader({
			selectButtonText: "上传",
			labelText: '',
			name: 'file',
			accept: "video/mp4,video/x-m4v,video/*",
			uploadMode: "instantly",
			uploadUrl: $.config('apiUrl') + "file-upload",
			onUploaded: function onUploaded(e) {
				var pat = JSON.parse(e.request.response).data;

				_this.setValue(pat.imgSrv);
			}
		});

		$container.find(".video-url").dxTextBox({
			onValueChanged: function onValueChanged(e) {
				_this.setValue(e.value);
			},
			placeholder: '视频地址'
		});
	},
	render: function render($container) {
		var v = $container.find(".video-url").dxTextBox("option", "value");
		if (v != this.getValue()) {
			$container.find(".video-url").dxTextBox("option", "value", this.getValue());
		}
	}
});

DxExtInstance.define({
	name: 'GrapeList',
	init: function init($container, option) {},
	render: function render($container) {
		var v = this.getValue(),
		    _this = this;

		v = v || [];

		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">葡萄</div>' + '<div class="pull-left"><div class="grape-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin: 0 10px;">种植面积占比</div>' + '<div class="pull-left"><div class="grape-input-area"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="grape-remove"></div>' + '</div></div>';

		$container.html('');
		$.each(v, function (i, item) {
			var $c = $(tpl);

			$c.find(".grape-input-name").dxTextBox({
				width: 200,
				value: item.name,
				onValueChanged: function onValueChanged(e) {
					v[i].name = e.value;
				}
			});

			$c.find(".grape-input-area").dxNumberBox({
				width: 80,
				value: item.proportion,
				showSpinButtons: true,
				// format: "#0%",
				onValueChanged: function onValueChanged(e) {
					v[i].proportion = e.value;
				}
			});

			$c.find(".grape-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c.appendTo($container);
		});

		$("<div/>").dxButton({
			text: '增加',
			onClick: function onClick() {
				v.push({
					name: '',
					area: 0
				});
				_this.setValue(v);
			}
		}).appendTo($container);
	}
});

DxExtInstance.define({
	name: 'GrapeDeployList',

	validate: function validate() {
		try {
			var result = DevExpress.validationEngine.validateGroup(this.validationGroup);
			if (result.isValid) {
				var value = this.getValue(),
				    err = false;
				value = value || [];

				this.opt.isValid = false;
				this.opt.validationError = "";

				if (this.opt.mode === 'single') {} else if (this.opt.mode === 'blend') {
					var total = 0;
					$.each(value, function (i, v) {
						total += parseFloat(v.scale);
					});

					if (value.length < 2) {
						err = '至少配置两种葡萄';
					} else if (total > 100) {
						err = '比例设置超过100';
					}
				} else {}

				if (!err) {
					this.opt.isValid = true;
				} else {
					this.opt.validationError = err;
				}
			}

			this.setValue(value);
		} catch (e) {
			this.opt.isValid = false;
		}

		return this.opt.isValid;
	},

	init: function init($container, option) {
		this.opt.mode = this.opt.mode || 'single';
		this.validationGroup = 'GrapeDeployList-' + Math.floor(Math.random() * 1000);

		console.log("init GrapeDeployList");

		// this.setValue(null);
	},

	render: function render($container) {
		var v = this.getValue(),
		    _this = this;

		v = v || [];

		var validationGroup = this.validationGroup;
		var vtpl = '<div class="dx-ex-validation-error"></div>';
		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">葡萄</div>' + '<div class="pull-left"><div class="grape-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin: 0 10px;">采收时间</div>' + '<div class="pull-left"><div class="grape-input-time"></div></div>' + (this.opt.mode === 'blend' ? '<div class="pull-left" style="line-height: 36px; margin: 0 10px;">比例</div>' + '<div class="pull-left"><div class="grape-input-area"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="grape-remove"></div></div>' : '') + '</div></div>';

		$container.html('');

		if (!this.opt.isValid && this.opt.validationError) {
			$('<div class="dx-ex-validation-error">' + this.opt.validationError + '</div>').appendTo($container);
		}

		$.each(v, function (i, item) {
			var $c = $(tpl);

			$c.find(".grape-input-name").dxTextBox({
				width: 200,
				value: item.name,
				onValueChanged: function onValueChanged(e) {
					v[i].name = e.value;
				}
			}).dxValidator({
				validationGroup: validationGroup,
				validationRules: [{
					type: 'required'
				}]
			});

			if (_this.opt.mode === 'blend') {
				$c.find(".grape-input-area").dxNumberBox({
					width: 80,
					value: item.scale,
					showSpinButtons: true,
					// format: "#0%",
					onValueChanged: function onValueChanged(e) {
						v[i].scale = e.value;
					}
				}).dxValidator({
					validationGroup: validationGroup,
					validationRules: [{
						type: 'required'
					}]
				});
			}

			$c.find('.grape-input-time').dxDateBox({
				value: item.harvestDate,
				type: "date",
				displayFormat: 'yyyy-MM-dd',
				dateSerializationFormat: "yyyy-MM-dd",
				onValueChanged: function onValueChanged(e) {
					v[i].harvestDate = e.value;
				}
			});

			$c.find(".grape-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c.appendTo($container);
		});

		if (this.opt.mode === 'blend') {
			$("<div/>").dxButton({
				text: '增加',
				onClick: function onClick() {
					v.push({
						name: '',
						scale: 100,
						harvestDate: null
					});
					_this.setValue(v);
				}
			}).appendTo($container);
		}

		if (v.length == 0) {
			$container.html('未选择调配类型');
		}
	},

	onOptionChanged: function onOptionChanged(e) {
		if (e.option === 'mode') {
			var value = this.getValue();

			this.opt.isValid = true;
			this.opt.validationError = "";

			if (value) {
				if (e.value === 'single') {
					if (value.length >= 1) {
						value = value.slice(0, 1);
					} else {
						value.push({
							name: '',
							scale: 100
						});
					}
				} else if (e.value === 'blend') {
					value = value.slice(0, 20);

					while (value.length < 2) {
						value.push({
							name: '',
							scale: 0
						});
					}
				}

				this.setValue(value);
			} else {
				if (e.value === 'single') {
					value = [{
						name: '',
						scale: 100
					}];
				} else if (e.value === 'blend') {
					value = [];

					while (value.length < 2) {
						value.push({
							name: '',
							scale: 0
						});
					}
				}

				this.value = value;
				this.render();
			}
		} else if (e.option === 'isValid') {} else if (e.option === 'validationError') {}
	}
});

DxExtInstance.define({
	name: 'dxEditor',
	init: function init($container, option) {

		var _this = this;
		option = $.extend({}, {
			single: false
		}, option);

		$container.html('<div contenteditable="true" class="ckeditor"></div>');

		// editorH1 editorH3

		// CKEDITOR.basePath = '../libs/ckeditor/';
		this.editor = CKEDITOR.replace($container.find('.ckeditor')[0], {
			extraPlugins: 'uploadimage,simpleimage,wine',
			height: 300,
			allowedContent: true,

			// Upload images to a CKFinder connector (note that the response type is set to JSON).
			uploadUrl: $.config('apiUrl') + "file-upload",

			// Load the default contents.css file plus customizations for this sample.
			contentsCss: [CKEDITOR.basePath + 'contents.css']
		});

		this.editor.setData("", {
			internal: true,
			noSnapshot: true
		});

		this.editor.on("change", function (evt) {
			_this.setValue(_this.editor.getData());
		});

		this.editor.on("notificationHide", function (evt) {
			_this.setValue(_this.editor.getData());
		});

		this.editor.on("wine.insert", function (evt) {
			var editor = _this.editor;

			$("<div>").appendTo($("body")).dxPopup({
				title: '请选择葡萄酒',
				visible: true,
				width: 400,
				height: 200,
				onContentReady: function onContentReady(e) {
					var $content = e.component.content();

					var instance = $(".select-wine").dxSelectBox({
						dataSource: $.crudStore(API("restful?_model=wine-product")),
						searchEnabled: true,
						valueExpr: 'id',
						itemTemplate: function itemTemplate(data, index, $el) {
							if (data) {
								$("<span>" + data.chname + " (" + data.name + ")</span>").appendTo($el);
							}
						},
						displayExpr: function displayExpr(data) {
							return data ? data.chname + " (" + data.name + ")" : "";
						}
					}).dxSelectBox('instance');

					$(".toolbar", $content).dxToolbar({
						items: [{
							location: 'after',
							widget: 'dxButton',
							options: {
								type: 'success',
								text: '确定',
								onClick: function onClick() {
									var v = instance.option('value');
									if (v) {
										editor.insertHtml("<p>{{drink:" + v + "}}</p>");
									}
									e.component.hide();
								}
							}
						}, {
							location: 'after',
							widget: 'dxButton',
							options: {
								type: 'normal',
								text: '取消',
								onClick: function onClick() {
									e.component.hide();
								}
							}
						}]
					});
				},
				contentTemplate: function contentTemplate(contentElement) {
					$('<div><div class="form-container" style="padding-bottom: 20px;"><div class="select-wine"></div></div><div class="toolbar"></div></div>').appendTo(contentElement);
				}
			});
		});

		this.editor.on("simpleimage.insert", function (evt) {
			var editor = _this.editor;

			$("<input type='file' accept='image/*'/>").click().change(function () {
				var formData = new FormData();

				if (this.files.length > 0) {
					formData.append('file', this.files[0]);

					$.ajax({
						url: $.config('apiUrl') + "file-upload",
						type: "POST",
						data: formData,
						processData: false,
						contentType: false
					}).then(function (resp) {
						if (resp && resp.status === "success") {
							editor.insertHtml("<img src='" + resp.data.imgSrv + "?imageView2/0/h/120'/>");
						} else {
							DevExpress.ui.dialog.alert('文件上传失败', '操作失败');
						}
					});
				}
			});
		});
	},

	render: function render($container) {
		if (this.editor) {
			var html = this.editor.getData();
			if (!html && this.getValue()) {
				var _this = this;
				// 直接调用setData偶尔会失败，原因未知
				var editable = _this.editor.editable();

				if (editable) {
					editable.setData(_this.getValue());
				} else {
					_this.editor.setData(_this.getValue(), {
						internal: true,
						noSnapshot: true,
						callback: function callback() {}
					});
				}
			}
		}
	},

	beforeGetValue: function beforeGetValue() {
		if (this.editor) {
			this.value = this.editor.getData();
			this.notifyValueChanged();
		}
	}
});

DxExtInstance.define({
	name: 'Tfselect',
	init: function init($container, option) {
		var _this = this,
		    itemData = [];
		option = $.extend({}, {
			single: false
		}, option);

		if (option.value) {
			$.each(option.value, function (i, va) {
				$('<div>').append($('<div class="tfselect">').dxSelectBox({
					dataSource: va,
					disabled: true,
					displayExpr: "expert_judges",
					valueExpr: "id",
					deferRendering: false
				})).append($('<div class="tftextarea">').dxTextArea({
					value: va.content
				})).append($('<div class="tfdelete">').dxButton({
					text: '删除',
					type: 'normal'
				})).appendTo($container);
			});
		}

		$container.append($('<div class="tfbutton">').dxButton({
			text: '增加专家鉴',
			type: 'normal',
			onClick: function onClick() {
				$container.find('.tfbutton').before($('<div>').append($('<div>').dxSelectBox({
					dataSource: new DevExpress.data.DataSource({
						store: $.crudStore(API("restful?_model=expert-user"))
					}),
					displayExpr: 'name',
					valueExpr: 'id'

				})).append($('<div>').dxTextArea({})).append($('<div>').dxButton({
					text: '删除',
					type: 'normal'

				})));
			}
		}));
	},
	render: function render($container) {}
});

DxExtInstance.define({
	name: 'Tfexpret',
	init: function init($container, option) {
		var _this = this;
		option = $.extend({}, {
			single: false
		}, option);

		$container.append($("<div>").dxSelectBox({
			dataSource: option.value,
			value: option.value[0].id,
			displayExpr: 'expert_user_name',
			valueExpr: 'id',
			showClearButton: true,
			placeholder: "分类",
			searchEnabled: true,
			noDataText: '没有请求到分类数据',
			deferRendering: false,
			onValueChanged: function onValueChanged(e) {
				_this.setValue(e.value);
			}
		}));
	},
	render: function render($container) {}
});

DxExtInstance.define({
	name: 'Tfscore',
	init: function init($container, option) {
		var _this = this;
		option = $.extend({}, {
			single: false
		}, option);
		var docu = $("<div> <span>专家评分</span> <div class='tfselect' ></div> <span>评分</span> <div class='tftext'></div>   </div>");

		docu.find("span").css({
			"margin": "10px",
			"width": "10%"
		});

		var boxtext = docu.find(".tftext").dxTextBox({}).data("dxTextBox");

		docu.find(".tfselect").dxSelectBox({
			dataSource: option.value,
			displayExpr: "expert_user_name",
			valueExpr: 'score',
			showClearButton: true,
			noDataText: '没有专家评分',
			placeholder: '专家分类',
			onValueChanged: function onValueChanged(e) {
				boxtext.option("value", e.value);
			}
		});

		docu.appendTo($container);
	},
	render: function render($container) {}
});

DxExtInstance.define({
	name: 'Tfsame_score',
	init: function init($container, option) {
		var _this = this;
		option = $.extend({}, {
			single: false
		}, option);
		var docu = $("<div> <span>生产年份</span> <div class='tfselect' ></div> <span>平均分</span> <div class='tftext'></div>   </div>");

		docu.find("span").css({
			"margin": "10px",
			"width": "10%"
		});

		var boxtext = docu.find(".tftext");

		var boxselect = $("<div>").dxSelectBox({
			dataSource: [],
			displayExpr: 'expert_user_name',
			valueExpr: 'expert_user_id',
			showClearButton: true,
			noDataText: '没有数据',
			onSelectionChanged: function onSelectionChanged(e) {}
		});

		docu.find(".tfselect").dxSelectBox({
			dataSource: option.value,
			displayExpr: "vintage",
			valueExpr: 'averageScore',
			showClearButton: true,
			noDataText: '没有同系列酒',
			placeholder: '同系列酒分类',
			value: 'expert_scores',
			onSelectionChanged: function onSelectionChanged(e) {
				console.log(e);
				boxtext.text(e.selectedItem.averageScore);
				boxselect.option('dataSource', e.selectedItem.expert_scores);
			}
		});

		docu.appendTo($container);
		boxselect.appendTo($container);
	},
	render: function render($container) {}
});

DxExtInstance.define({
	name: 'ExpertList',
	init: function init($container, option) {},
	render: function render($container) {
		var v = this.getValue(),
		    _this = this;

		v = v || [];

		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="width: 120px; line-height: 36px;">专家/达人</div>' + '<div class="pull-left" style="width: 50%;"><div class="expert-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="expert-remove"></div></div>' + '</div></div>' + '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="width: 120px; line-height: 36px;">点评内容</div>' + '<div class="pull-left" style="width: 50%;"><div class="expert-input-area"></div></div>' + '</div></div>' + '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="width: 120px; line-height: 36px;">一句话点评</div>' + '<div class="pull-left" style="width: 50%;"><div class="expert-input-word"></div></div>' + '</div></div>;';

		$container.html('');
		$.each(v, function (i, item) {
			var $c = $(tpl);

			$c.find(".expert-input-name").dxSelectBox({
				dataSource: $.crudStore(API('restful?_model=expert-user')),
				showClearButton: true,
				placeholder: "专家",
				searchEnabled: true,
				valueExpr: 'id',
				noDataText: '没有请求到专家数据',
				itemTemplate: function itemTemplate(data, index, $el) {
					if (data) {
						$("<span>" + data.name + " (" + data.account + ")</span>").appendTo($el);
					}
				},
				displayExpr: function displayExpr(data) {
					return data ? data.name + " (" + data.account + ")" : "";
				},
				width: '100%',
				value: item.expert_user_id,
				onValueChanged: function onValueChanged(e) {
					v[i].expert_user_id = e.value;
				}
			});

			$c.find(".expert-input-area").dxTextArea({
				width: '100%',
				value: item.content,
				onValueChanged: function onValueChanged(e) {
					v[i].content = e.value;
				}
			});

			$c.find(".expert-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c.find('.expert-input-word').dxTextBox({
				width: '100%',
				value: item.short_review,
				onValueChanged: function onValueChanged(e) {
					v[i].short_review = e.value;
				}
			});

			$c.appendTo($container);
		});

		$("<div/>").dxButton({
			text: '增加',
			onClick: function onClick() {
				v.push({
					expert_user_id: 0,
					content: '',
					short_review: ''
				});
				_this.setValue(v);
			}
		}).appendTo($container);
	}
});

DxExtInstance.define({
	name: 'ExpertScoreList',
	init: function init($container, option) {
		$container.html('<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<h3 class="pull-left score-year-title">本款酒</h3>' + '<div class="score-title pull-left" style="line-height: 36px; margin: 0 10px;">平均分 <span class="avg-score">0</span></div>' + '</div></div>' + '<div class="score-container"></div>');
	},
	render: function render($container) {
		var v = this.getValue(),
		    _this = this,
		    $content = $container.find(".score-container");

		v = v || [];

		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">专家</div>' + '<div class="pull-left"><div class="expert-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin: 0 10px;">评分</div>' + '<div class="pull-left"><div class="expert-input-area"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="expert-remove"></div>' + '</div></div>';

		var updateScore = function updateScore() {
			var v = _this.getValue() || [],
			    s = 0;

			$.each(v, function (i, vitem) {
				var n = parseInt(vitem.score) || 0;
				s += n;
			});

			$(".avg-score", $container).text(v.length > 0 ? Math.ceil(s / v.length) : 0);
		};

		$content.html('');
		$.each(v, function (i, item) {
			var $c = $(tpl);

			$c.find(".expert-input-name").dxSelectBox({
				dataSource: $.crudStore(API('restful?_model=expert-user')),
				showClearButton: true,
				placeholder: "专家",
				searchEnabled: true,
				valueExpr: 'id',
				noDataText: '没有请求到专家数据',
				itemTemplate: function itemTemplate(data, index, $el) {
					if (data) {
						$("<span>" + data.name + " (" + data.account + ")</span>").appendTo($el);
					}
				},
				displayExpr: function displayExpr(data) {
					return data ? data.name + " (" + data.account + ")" : "";
				},
				value: item.expert_user_id,
				onValueChanged: function onValueChanged(e) {
					v[i].expert_user_id = e.value;
				}
			});

			$c.find(".expert-input-area").dxNumberBox({
				width: 80,
				value: item.score,
				showSpinButtons: true,
				// format: "#0%",
				onValueChanged: function onValueChanged(e) {
					v[i].score = e.value;
					updateScore();
				}
			});

			$c.find(".expert-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c.appendTo($content);
		});

		$("<div/>").dxButton({
			text: '添加专家评分',
			onClick: function onClick() {
				v.push({
					expert_user_id: 0,
					score: 0
				});
				_this.setValue(v);
			}
		}).appendTo($content);

		updateScore();
	}
});

DxExtInstance.define({
	name: 'ExpertYearScoreList',
	init: function init($container, option) {},
	render: function render($container) {
		var v = this.getValue(),
		    _this = this;
		console.log(v);
		v = v || [];

		var tplH = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<h3 class="pull-left score-year-title">属性</h3>' + '<div class="pull-right"><div class="score-year-remove"></div></div>' + '</div></div>';

		var tpl0 = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">属性名称</div>' + '<div class="pull-left"><div class="score-year"></div></div>' + '</div></div>' + '<div class="score-container"></div>' + '<div style="margin-top: 20px;"><div class="expert-add"></div></div>';

		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">属性</div>' + '<div class="pull-left"><div class="expert-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="expert-remove"></div>' + '</div></div>';

		var updateScore = function updateScore() {
			var v = _this.getValue() || [],
			    s = 0;

			$.each(v, function (i, vitem) {
				var n = parseInt(vitem.score) || 0;
				s += n;
			});

			$(".avg-score", $container).text(v.length > 0 ? Math.ceil(s / v.length) : 0);
		};

		$container.html('');
		$.each(v, function (i, item) {
			var $c0 = $("<div style='margin-bottom: 20px;'>" + tplH + tpl0 + "</div>");

			$c0.find(".score-year-title").text('属性 #' + i);

			$c0.find(".score-year-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c0.find(".score-year").dxTextBox({
				value: v[i].name,
				onValueChanged: function onValueChanged(e) {
					v[i].name = e.value;
				}
			});

			var $cc = $c0.find(".score-container");

			if (v[i].values) {

				v[i].values = v[i].values;
				// v[i].values = undefined;
			} else {
				v[i].values = v[i].values || [];
			}

			v[i].type = 'radio';

			$c0.find(".expert-add").dxButton({
				text: '添加属性',
				onClick: function onClick() {
					v[i].values.push({
						value: ''

					});
					_this.setValue(v);
				}
			});

			$.each(item.values, function (k, item) {
				var _$c$find$dxTextBox;

				var $c = $(tpl);

				$c.find(".expert-input-name").dxTextBox((_$c$find$dxTextBox = {
					value: '',
					itemTemplate: function itemTemplate(data, index, $el) {
						if (data) {
							$("<span>" + data.name + " (" + data.account + ")</span>").appendTo($el);
						}
					},
					displayExpr: function displayExpr(data) {
						return data ? data.name + " (" + data.account + ")" : "";
					}
				}, _defineProperty(_$c$find$dxTextBox, "value", item.value), _defineProperty(_$c$find$dxTextBox, "onValueChanged", function onValueChanged(e) {
					v[i].values[k].value = e.value;
				}), _$c$find$dxTextBox));

				// $c.find(".expert-input-area").dxNumberBox({
				// 	width: 80,
				// 	value: item.score,
				// 	showSpinButtons: true,
				// 	// format: "#0%",
				// 	onValueChanged: function(e) {
				// 		v[i].items[k].score = e.value;
				// 		updateScore();
				// 	}
				// });

				$c.find(".expert-remove").dxButton({
					text: '删除',
					onClick: function onClick() {
						v[i].values.splice(k, 1);
						_this.setValue(v);
					}
				});

				$c.appendTo($cc);
			});

			$c0.appendTo($container);
		});

		$("<div/>").dxButton({
			text: '添加类',
			onClick: function onClick() {
				v.push({
					name: "",
					values: []
				});
				_this.setValue(v);
			}
		}).appendTo($container);

		updateScore();
	}
});

DxExtInstance.define({
	name: 'YearSelect',
	init: function init($container, option) {
		var years = [],
		    _this = this;
		for (var y = 1950; y <= 2050; y++) {
			years.push(y);
		}$("<div class='year-select'/>").appendTo($container).dxSelectBox({
			dataSource: years,
			acceptCustomValue: true,
			onValueChanged: function onValueChanged(e) {
				_this.setValue(e.value);
			},
			displayExpr: function displayExpr(v) {
				return v ? v + '年' : '';
			}
		});
	},
	render: function render($container) {
		var v = this.getValue(),
		    v0 = $(".year-select", $container).dxSelectBox("option", "value");

		if (v != v0) {
			$(".year-select", $container).dxSelectBox("option", "value", v);
		}
	}
});

var DxExtClass = function () {
	function DxExtClass() {
		_classCallCheck(this, DxExtClass);
	}

	_createClass(DxExtClass, [{
		key: "define",
		value: function define(opt) {
			DxExtInstance.define(opt);
		}
	}, {
		key: "validate",
		value: function validate(formInstance) {
			DxExtInstance.validate(formInstance);
		}
	}]);

	return DxExtClass;
}();

var DxExt = exports.DxExt = new DxExtClass();


},{}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Page = exports.Page = function () {
	function Page($container) {
		_classCallCheck(this, Page);

		this.$container = $container;
		this.$content = $container;
	}

	_createClass(Page, [{
		key: "crudLayout",
		value: function crudLayout(opt) {
			var data = $.extend({ title: '未设置' }, opt),
			    html = "<div class=\"app-content-body \">\n\t\t  <div class=\"bg-light lter b-b wrapper-md hidden-print\">\n\t\t    <h1 class=\"m-n font-thin h3\">{{title}}</h1>\n\t\t  </div>\n\t\t  <div>\n\t\t    <div class=\"crud-container crud-reset\">\n\t\t    </div>\n\t\t  </div>\n\t\t</div>";

			this.$container.html(Mustache.render(html, data));
			this.$content = this.$container.find(".crud-container");

			return this;
		}
	}, {
		key: "content",
		get: function get() {
			return this.$content;
		}
	}]);

	return Page;
}();


},{}],4:[function(require,module,exports){
'use strict';

var _app = require('./app');

var _util = require('./util');

var _util2 = _interopRequireDefault(_util);

var _page = require('./page');

var _dxExt = require('./dx-ext');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_dxExt.DxExt.define({
  name: 'DealerInput',
  init: function init($container, option) {
    var _this = this;

    option = $.extend({}, {
      single: false
    }, option);

    $container.html("<div class='tagbox'></div><div style='margin-top: 20px;' class='grid'></div>");

    this.tagBoxInstance = $container.find(".tagbox").dxTagBox({
      deferRendering: false,
      dataSource: $.crudStore(API("restful?_model=sys-dealer")),
      searchEnabled: true,
      valueExpr: 'id',
      displayExpr: 'name',
      placeholder: '请选择经销商',
      onSelectionChanged: function onSelectionChanged(e) {
        var v = _this.getValue() || [],
            changed = false;

        e.addedItems.forEach(function (x) {
          var idx = v.findIndex(function (y) {
            return y.id == x.id;
          });
          if (idx < 0) {
            v.push({ id: x.id, show: 0, name: x.name });
            changed = true;
          }
        });

        var len = v.length;
        e.removedItems.forEach(function (x) {
          return v = v.filter(function (it) {
            return x.id !== it.id;
          });
        });
        if (!changed && len != v.length) changed = true;

        if (changed) setTimeout(function () {
          return _this.setValue(v);
        }, 0);
      }
    }).dxTagBox("instance");

    this.gridInstance = $container.find(".grid").dxDataGrid({
      columns: [{
        dataField: 'name',
        caption: '名称'
      }, {
        dataField: 'show',
        width: 120,
        caption: '是否显示',
        cellTemplate: function cellTemplate(c, ci) {
          var $el = $("<a>");
          $el.css("text-decoration", "underline").text(ci.value ? "显示" : "不显示").attr('title', '单击切换').appendTo(c);
          $el.click(function () {
            var v = _this.getValue() || [],
                idx = v.findIndex(function (x) {
              return x.id == ci.data.id;
            });

            if (idx >= 0) {
              v[idx].show = v[idx].show == 0 ? 1 : 0;
              _this.setValue(v);
            }
          });
        }
      }, {
        width: 120,
        dataField: 'id',
        caption: '删除',
        cellTemplate: function cellTemplate(c, ci) {
          var $el = $("<a>");
          $el.css("text-decoration", "underline").text("删除").appendTo(c);
          $el.click(function () {
            var v = _this.getValue() || [],
                idx = v.findIndex(function (x) {
              return x.id == ci.value;
            });

            if (idx >= 0) {
              v.splice(idx, 1);
              _this.setValue(v);
            }
          });
        }
      }]
    }).dxDataGrid("instance");

    // $container.find(".file .dx-button-content").html('<i class="iconfont">&#xe623;</i>');
  },

  render: function render($container) {
    var v = this.getValue() || [];

    if (this.gridInstance) {
      this.gridInstance.option('dataSource', v);
    }

    if (this.tagBoxInstance) {
      this.tagBoxInstance.option('value', v.map(function (item) {
        return item.id;
      }));
    }

    // if ($container.find(".grid").length > 0) {
    //   $container.find(".grid").dxDataGrid('option', 'dataSource', v);
    // }
  }
});

function validateForm(formInstance) {
  console.log('validateForm');

  var editors = [];

  function _check(o) {
    if (o.dataField) {
      var editor = formInstance.getEditor(o.dataField);
      if (editor && typeof editor.isExtInstance === 'function') {
        editors.push(editor);
      }
    } else if (o.items) {
      $.each(o.items, function (i, item) {
        _check(item);
      });
    }
  }

  _check({
    items: formInstance.option('items')
  });

  var result = true;
  $.each(editors, function (i, e) {
    if (!e.validate()) {
      result = false;
    }
  });

  return result;
}

_dxExt.DxExt.define({
  name: 'dxScene',

  init: function init($container, option) {
    var sceneByLevel = {
      "1": [],
      "2": [],
      "3": []
    };

    function _tr(items, level) {
      for (var i = 0; i < items.length; i++) {
        sceneByLevel[level.toString()].push(items[i]);

        if (items[i].child && items[i].child.length > 0) {
          _tr(items[i].child, level + 1);
        }
      }
    }

    _tr(this.opt.sceneTree, 1);

    this.sceneByLevel = sceneByLevel;
  },

  render: function render($container) {
    var _this2 = this;

    var v = this.getValue(),
        _this = this;

    v = v || [];

    $container.html('');

    var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left"><div class="grape-input-one" style="margin-right: 10px;"></div></div>' + '<div class="pull-left"><div class="grape-input-two" style="margin-right: 10px;"></div></div>' + '<div class="pull-left"><div class="grape-input-three" style="margin-right: 10px;"></div></div>' + '<div class="pull-left" style="margin-right: 10px;"><div class="remove-item"></div></div>' + '</div></div>';

    var lookupItem = function lookupItem(v, items) {
      var idx = items.findIndex(function (it) {
        return it.value == v;
      });

      return idx >= 0 ? items[idx] : null;
    };

    var arrayOrEmpty = function arrayOrEmpty(item) {
      return item ? [item] : [];
    };

    console.log('dxScene value');
    console.log(v);

    $.each(v, function (i, item) {
      var $tpl = $(tpl);

      $tpl.find(".grape-input-one").dxSelectBox({
        dataSource: _this2.sceneByLevel["1"],
        displayExpr: 'name',
        valueExpr: 'value',
        value: parseInt(item.scene_one_id),
        onValueChanged: function onValueChanged(e) {
          v[i].scene_one_id = e.value;

          var item = lookupItem(e.value, _this.sceneByLevel["1"]);
          $tpl.find(".grape-input-two").dxSelectBox('instance').option('dataSource', item ? item.child : []);

          $tpl.find('.grape-input-three').dxSelectBox('instance').option('dataSource', []);
        }
      });

      $tpl.find(".grape-input-two").dxSelectBox({
        dataSource: arrayOrEmpty(lookupItem(item.scene_two_id, _this.sceneByLevel["2"])),
        displayExpr: 'name',
        valueExpr: 'value',
        value: parseInt(item.scene_two_id),
        onValueChanged: function onValueChanged(e) {
          v[i].scene_two_id = e.value;

          var item = lookupItem(e.value, _this.sceneByLevel["2"]);
          $tpl.find(".grape-input-three").dxSelectBox('instance').option('dataSource', item ? item.child : []);
        }
      });

      $tpl.find(".grape-input-three").dxSelectBox({
        dataSource: arrayOrEmpty(lookupItem(item.scene_three_id, _this.sceneByLevel["3"])),
        displayExpr: 'name',
        valueExpr: 'value',
        value: parseInt(item.scene_three_id),
        onValueChanged: function onValueChanged(e) {
          v[i].scene_three_id = e.value;
        }
      });

      $tpl.find(".remove-item").dxButton({
        text: '删除',
        onClick: function onClick() {
          v.splice(i, 1);
          _this.setValue(v);
        }
      });

      $tpl.appendTo($container);
    });

    $("<div/>").dxButton({
      text: '增加',
      onClick: function onClick() {
        v.push({ scene_one_id: 0, scene_two_id: 0, scene_three_id: 0 });
        _this.setValue(v);
      }
    }).appendTo($container);
  }
});

_dxExt.DxExt.define({
  name: 'dxTasteList',

  init: function init($container, option) {
    // this.setValue(null);
    this.store = $.crudStore(API('restful?_model=wine-cuisine'));
  },

  render: function render($container) {
    var v = this.getValue(),
        _this = this;

    v = v || [];

    var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="margin-right:30px"><div class="grape-input-taste"></div></div>' + '<div class="pull-left" style="margin-right:30px"><div class="grape-input-remark"></div></div>' + '<div class="pull-left" style="margin-right:30px"><div class="grape-remove"></div></div>' + '</div></div>';

    $container.html('');

    $.each(v, function (i, item) {
      var $c = $(tpl);

      $c.find(".grape-input-taste").dxSelectBox({
        dataSource: _this.store,
        width: 200,
        value: item.wine_cuisines_id,
        valueExpr: 'id',
        displayExpr: 'name',
        onValueChanged: function onValueChanged(e) {
          v[i].wine_cuisines_id = e.value;
        }
      });

      $c.find('.grape-input-remark').dxTextBox({
        width: 400,
        value: item.text,
        onValueChanged: function onValueChanged(e) {
          v[i].text = e.value;
        }
      });

      $c.find(".grape-remove").dxButton({
        text: '删除',
        onClick: function onClick() {
          v.splice(i, 1);
          _this.setValue(v);
        }
      });

      $c.appendTo($container);
    });

    $("<div/>").dxButton({
      text: '增加',
      onClick: function onClick() {
        v.push({
          wine_cuisines_id: 0,
          text: ''
        });
        _this.setValue(v);
      }
    }).appendTo($container);
  }
});

_dxExt.DxExt.define({
  name: 'dxUserLabel',

  init: function init($container, option) {},

  render: function render($container) {
    var v = this.getValue(),
        _this = this;

    v = v || [];

    var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">身份</div>' + '<div class="pull-left" style="margin-right: 10px;"><div class="grape-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">性别</div>' + '<div class="pull-left"  style="margin-right: 10px;"><div class="grape-input-sex"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">年龄段</div>' + '<div class="pull-left" style="margin-right: 10px;"><div class="grape-input-age"></div></div>' + '<div class="pull-left"><div class="grape-remove"></div></div>' + '</div></div>';

    $container.html('');

    var age = ['18-25', '25-30', '30-35', '35-45', '45-55', '55以上'];
    var auth = ['领导', '客户', '长辈', '闺蜜', '基友', '朋友', '情侣'];

    $.each(v, function (i, item) {
      var $c = $(tpl);

      $c.find(".grape-input-name").dxSelectBox({
        dataSource: auth,
        width: 200,
        value: item.auth,
        onValueChanged: function onValueChanged(e) {
          v[i].auth = e.value;
        }
      });

      $c.find('.grape-input-sex').dxSelectBox({
        width: 200,
        dataSource: [{ text: '男', value: 'man' }, { text: '女', value: 'woman' }],
        valueExpr: 'value',
        displayExpr: 'text',
        value: item.sex,
        onValueChanged: function onValueChanged(e) {
          v[i].sex = e.value;
        }
      });

      $c.find('.grape-input-age').dxSelectBox({
        width: 200,
        dataSource: age,
        value: item.age,
        onValueChanged: function onValueChanged(e) {
          console.log(e);
          v[i].age = e.value;
        }
      });

      $c.find(".grape-remove").dxButton({
        text: '删除',
        onClick: function onClick() {
          v.splice(i, 1);
          _this.setValue(v);
        }
      });

      $c.appendTo($container);
    });

    $("<div/>").dxButton({
      text: '增加',
      onClick: function onClick() {
        v.push({ auth: '', sex: '', age: '' });
        _this.setValue(v);
      }
    }).appendTo($container);
  }
});

_dxExt.DxExt.define({
  name: 'KeywordList',

  init: function init($container, option) {
    // this.setValue(null);
    this.store = $.crudStore(API('restful?_model=wine-taste-keywords'));
  },

  render: function render($container) {
    var v = this.getValue(),
        _this = this;

    v = v || [];

    var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="margin-right:30px"><div class="grape-input-taste"></div></div>' + '<div class="pull-left" style="margin-right:30px"><div class="grape-input-remark"></div></div>' + '<div class="pull-left" style="margin-right:30px"><div class="grape-remove"></div></div>' + '</div></div>';

    $container.html('');

    $.each(v, function (i, item) {
      var $c = $(tpl);

      $c.find(".grape-input-taste").dxSelectBox({
        dataSource: _this.store,
        width: 200,
        value: item.keyword_id,
        valueExpr: 'id',
        displayExpr: 'name',
        onValueChanged: function onValueChanged(e) {
          v[i].keyword_id = e.value;
        }
      });

      $c.find('.grape-input-remark').dxTextBox({
        width: 400,
        value: item.text,
        onValueChanged: function onValueChanged(e) {
          v[i].text = e.value;
        }
      });

      $c.find(".grape-remove").dxButton({
        text: '删除',
        onClick: function onClick() {
          v.splice(i, 1);
          _this.setValue(v);
        }
      });

      $c.appendTo($container);
    });

    $("<div/>").dxButton({
      text: '增加',
      onClick: function onClick() {
        v.push({
          keyword_id: 0,
          text: ''
        });
        _this.setValue(v);
      }
    }).appendTo($container);
  }
});

var initPage = function initPage(context, $container) {
  var page = new _page.Page($container);

  var id = context.params.id,
      wineStore = $.crudStore(API("restful?_model=product"));
  var formData = '',
      data = {};

  page.crudLayout({ title: id ? '编辑' : '新建' });

  page.content.html('\n    <form id="form-container">\n      <div class="form_title">\n        <div class="my_hd" style="padding: 0; border-bottom: 0;">\n          <div id="dxtabs"></div>\n        </div>\n        <div class="crud-container crud-reset">\n\t        <div class="row">\n\t          <div class="col-sm-12">\n\t            \n\t            <div id="wine-form">\n\t            </div>\n\t          </div>\n\t        </div>\n\t      \t\n\t      \t<div class="row">\n\t\t        <div class="col-sm-12 text-right" style="margin-top: 15px;">\n\t\t          <div id=\'saveData\'></div>\n\t\t          <div id="cancel"></div>\n\t\t        </div>\n\t      \t</div>\n        </div>\n\t  \t</div>\n    </form>\n\t');

  $(".main-title").text(id ? "编辑葡萄酒信息" : "新建葡萄酒信息");

  var xhrs = [],
      prefetchData = {},
      formInstances = {};

  xhrs.push($.get(API("restful?_model=product")).then(function (resp) {
    prefetchData['restful?_model=product'] = resp.data;
  }));

  xhrs.push($.get(API('restful?_model=product-attr-group')).then(function (resp) {
    prefetchData['restful?_model=product-attr-group'] = resp;
  }));

  $.when.apply($, xhrs).then(function () {
    var formItems = {};

    formItems['show'] = [{
      dataField: 'keywords',
      label: { text: '产品名称' },
      editorType: 'KeywordList',
      editorOptions: {}
    }, {
      dataField: 'consume',
      label: { text: '消费指南' },
      editorType: 'dxTextArea'
    }, {
      itemType: 'group',
      colCount: '3',
      items: [{
        dataField: 'wineglass',
        label: { text: '杯形' }
      }, {
        dataField: 'sober_time',
        label: { text: '醒酒时间' }
      }, {
        dataField: 'temperature',
        label: { text: '饮用温度' }
      }]
    }, {
      itemType: 'group',
      items: [{
        dataField: 'chinese_food',
        label: { text: '中餐搭配' }

      }, {
        dataField: 'western_food',
        label: { text: '西餐搭配' }
      }, {
        dataField: 'snack',
        label: { text: '小食搭配' }
      }, {
        dataField: 'undelicacy',
        label: { text: '暗黑搭配' }
      }]
    }, {
      dataField: 'talk_keywords',
      label: { text: '谈资关键词' },
      helpText: '20字内'
    }, {

      dataField: 'talk_about',
      label: { text: '谈资建议' },
      editorType: 'dxEditor'
    }];

    formItems['params'] = [{
      itemType: 'group',
      colCount: 2,
      items: [{
        dataField: 'chateau_id',
        label: { text: '品牌' },
        validationRules: [{ type: "required" }],
        editorType: 'dxSelectBox',
        editorOptions: {
          dataSource: $.crudStore(API('restful?_model=wine-chateau')),
          showClearButton: true,
          placeholder: "品牌",
          searchEnabled: true,
          valueExpr: 'id',
          noDataText: '没有请求到品牌数据',
          itemTemplate: function itemTemplate(data, index, $el) {
            if (data) {
              $("<span>" + data.chname + " (" + data.name + ")</span>").appendTo($el);
            }
          },
          displayExpr: function displayExpr(data) {
            return data ? data.chname + " (" + data.name + ")" : "";
          }
        }

      }]
    }, {
      itemType: 'group',
      items: [{
        dataField: 'chname',
        validationRules: [{
          type: "required",
          message: "请输入商品名称"
        }],
        label: { text: '商品名称' }
      }, {
        dataField: 'name',
        validationRules: [{
          type: "required",
          message: "商品毛重"
        }],
        label: { text: '商品毛重' }
      }, {
        dataField: 'letDrink_recom',
        validationRules: [{
          type: "required",
          message: "请输入乐饮推荐语"
        }],
        label: { text: '商品毛重' }
      }]
    }, {
      itemType: 'group',
      colCount: 3,
      items: [{
        dataField: 'promotion_price',
        validationRules: [{ type: "required" }],
        label: { text: '促销价' },
        editorType: 'dxNumberBox'
      }, {
        dataField: 'price',
        validationRules: [{ type: "required" }],
        label: { text: '原价' },
        editorType: 'dxNumberBox'
      }, {
        dataField: 'sku',
        label: { text: '库存' },
        editorType: 'dxNumberBox'
      }]
    }, {
      itemType: 'group',
      colCount: 1,
      items: [{
        dataField: 'sell_method',
        validationRules: [{ type: "required" }],
        label: { text: '销售方式' },
        editorType: 'dxRadioGroup',
        editorOptions: {
          layout: "horizontal",
          valueExpr: 'value',
          dataSource: [{
            text: '线上销售',
            value: 'online'
          }, {
            text: '线下销售',
            value: 'offline'
          }, {
            text: '产品展示',
            value: 'show'
          }]
        },
        helpText: '线上销售可以从线上直接购买，前端的价格信息单独展示；线下销售，线上不能购买。价格展示在经销商信息上。'
      }]
    }, {
      itemType: 'group',
      items: [{
        dataField: 'banner',
        validationRules: [{
          type: "required"
        }],
        label: { text: '封面图' },
        editorType: 'ImageUploader',
        editorOptions: {
          single: true,
          imageWidth: 750,
          imageHeight: 422
        }
      }, {
        dataField: 'covers',
        validationRules: [{
          type: "required"
        }],
        label: { text: '轮播图' },
        editorType: 'ImageUploader',
        editorOptions: {
          imageWidth: 750,
          imageHeight: 1118
        }
      }]
    }, {
      itemType: 'group',
      colCount: '2',
      items: [{
        dataField: 'category',
        label: { text: '类别' },
        validationRules: [{
          type: "required",
          message: "请输入类别"
        }],
        editorType: 'dxSelectBox',
        editorOptions: {
          dataSource: prefetchData['product-category'],
          acceptCustomValue: true,
          onCustomItemCreating: function onCustomItemCreating(e) {
            return e.text;
          },
          placeholder: '选择或者输入类别'
        }
      }, {
        dataField: 'level',
        label: { text: '产品级别' }
      }]
    }, {
      dataField: 'vintage',
      label: { text: '生产日期' },
      validationRules: [{
        type: "required",
        message: "请输入生产日期"
      }],
      editorType: 'YearSelect'
    }, {
      itemType: 'group',
      items: [{
        dataField: 'importers',
        label: { text: '进口商' },
        editorType: 'dxTagBox',
        editorOptions: {
          deferRendering: false,
          dataSource: $.crudStore(API("restful?_model=sys-importer")),
          searchEnabled: true,
          valueExpr: 'id',
          displayExpr: 'name'
        }
      }, {
        dataField: 'dealers',
        label: { text: '经销商' },
        editorType: 'DealerInput',
        editorOptions: {}
      }]
    }];

    var tabs = [{
      text: '属性信息',
      id: "show"
    }, {
      text: "参数信息",
      id: "params",
      disabled: !id
    }];

    $.each(["params", "show"], function (index, item) {
      formInstances[item] = $("<div>").appendTo($("#wine-form")).dxForm({
        items: formItems[item],
        showColonAfterLabel: true,
        showValidationSummary: false,
        validationGroup: "crud",
        alignItemLabels: true,
        alignItemLabelsInAllGroups: true
      }).dxForm("instance");
    });

    function showTab(item) {
      for (var k in formInstances) {
        formInstances[k].option('visible', k === item);
      }
    }

    $("#dxtabs").dxTabs({
      dataSource: tabs,
      selectedIndex: 0,
      onItemClick: function onItemClick(e) {
        showTab(e.itemData.id);
      }
    });

    showTab("show");

    if (id) {
      wineStore.byKey(id).then(function (resp) {
        console.log(resp);

        $.each(["country_id", "chateau_id"], function (i, item) {
          resp[item] = parseInt(resp[item]);
        });

        $.each(["relation_products", "relation_importers"], function (i, item) {
          if (resp[item]) resp[item] = $.map(resp[item], function (it) {
            return it.id;
          });
        });

        if (resp['relation_dealers']) {
          resp['dealers'] = $.map(resp['relation_dealers'], function (it) {
            return { id: it.id, show: it.pivot.show, name: it.name };
          });
        }

        var mapped = {
          "blend_deployment": "deployment_graperies",
          "scenes": "scene_ids",
          "cuisines": "wine_cuisines",
          "relation_importers": "importers",
          "judge.expert_judges": "judge.judge_arr",
          "score.expert_scores": "score.self_arr",
          "score.same_scores": "score.same_arr",
          "drink.tasteKeywords": "drink.keywords"
        };

        for (var k in mapped) {
          if (_.has(resp, k)) {
            _.set(resp, mapped[k], _.get(resp, k));
            _.unset(resp, k);
          }
        }

        console.log(resp);

        var dataBasic = $.extend({}, resp);
        $.each(["drink", "score", "judge", "technique"], function (i, item) {
          dataBasic[item] = undefined;
        });

        formInstances['basic'].option('formData', dataBasic);

        formInstances['drink'].option('formData', resp.drink);
        formInstances['judge'].option('formData', resp.judge);
        formInstances['technique'].option('formData', resp.technique);

        formInstances['score'].option('formData', resp.score);
      });
    } else {
      var productData = new DevExpress.data.CustomStore({
        store: $.crudStore(wineStore)
      });

      //form(productData)
    }
  });

  function saveCurrentTab() {
    var selectedItem = $("#dxtabs").dxTabs("option", "selectedItem");

    if (selectedItem) {
      $("#saveData").dxButton('option', { disabled: true, text: '正在保存' });

      var url = selectedItem.id === 'basic' ? API("restful?_model=wine-product") : API("restful?_model=wine-product-" + selectedItem.id);

      var _formData = $.extend({}, formInstances[selectedItem.id].option('formData'));

      if (selectedItem.id !== 'basic') {
        _formData.product_id = id;
      } else {
        var mapped = {
          relation_products: 'product_relation_ids'
          //scenes: 'scene_ids'
        };

        for (var k in mapped) {
          if (_formData[k]) {
            _formData[mapped[k]] = _formData[k].join(",");
          } else {
            _formData[mapped[k]] = '';
          }

          _formData[k] = undefined;
        }

        if (_formData['covers']) {
          _formData['cover_ids'] = $.map(_formData['covers'], function (item) {
            return item.id;
          }).join(",");
        }
      }

      console.log(_formData);

      var request = null;
      if (_formData.id) {
        request = $.crudStore(url).update(_formData.id, _formData);
      } else {
        request = $.crudStore(url).insert(_formData);
      }

      request.then(function (resp, data) {

        DevExpress.ui.notify({
          message: "已更新"
        }, "success", 1000);

        if (!id) {
          _app.App.getEventContext().redirect('#/newproduct/' + data.data.id);
        }

        console.log(resp);
      }).fail(function (e) {

        var message = "更新失败";
        if ($.crudStoreResp && $.crudStoreResp.message) {
          message += ": " + $.crudStoreResp.message;
        } else {
          message += ": 服务器错误";
        }

        DevExpress.ui.notify({
          message: message
        }, "error", 1000);
      }).always(function () {
        $("#saveData").dxButton('option', { disabled: false, text: '保存' });
      });
    }
  }

  $("#saveData").dxButton({
    text: id ? '保存' : '新建',
    type: 'default',
    useSubmitBehavior: false
  }).click(function () {
    var selectedItem = $("#dxtabs").dxTabs("option", "selectedItem");

    if (selectedItem) {
      var result = formInstances[selectedItem.id].validate();

      if (!result.isValid) {
        var $el = result.validators[0].element();

        var offset = $el.offset();

        $('html, body').animate({
          scrollTop: offset.top - $("header.app-header").height() - 20
        });

        // } else if (validateForm(formInstances[selectedItem.id])) {
        //   saveCurrentTab();
      } else {
        saveCurrentTab();
      }
    }

    return false;
  });

  $("#cancel").dxButton({
    text: "返回",
    type: "normal",
    onClick: function onClick() {
      context.redirect("#/members");
    }
  });
};

_app.App.registerRoute({
  name: 'newproduct/:id',
  onLoad: function onLoad(context, $container) {
    initPage(context, $container);
  }
});

_app.App.registerRoute({
  name: 'newproduct',
  onLoad: function onLoad(context, $container) {
    initPage(context, $container);
  }
});


},{"./app":1,"./dx-ext":2,"./page":3,"./util":5}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Util = function () {
	function Util() {
		_classCallCheck(this, Util);
	}

	_createClass(Util, [{
		key: 'formatXbm',
		value: function formatXbm(xbm) {
			if (xbm == '2') return '女';else if (xbm == '1') return '男';else return xbm;
		}
	}, {
		key: 'crudStore',
		value: function crudStore(url, options) {

			var SERVICE_URL = url;
			options = $.extend({
				onDataArrived: function onDataArrived(data) {
					return data;
				}
			}, options);

			return new DevExpress.data.CustomStore($.extend({}, options, {

				load: function load(loadOptions) {

					var u = new Url(SERVICE_URL);

					u.query._param = JSON.stringify(loadOptions);

					return $.getJSON(u).then(function (resp) {
						console.log("data done");
						return options.onDataArrived(resp);
					});
				},

				byKey: function byKey(key) {
					var u = new Url(SERVICE_URL);
					u.path += "/" + encodeURIComponent(key);

					return $.getJSON(u).then(function (resp) {
						return resp;
					});
				},

				insert: function insert(values) {
					console.log(values);
					return $.post(SERVICE_URL, values).always(function (resp) {
						$.crudStoreResp = resp.responseJSON;
						return resp;
					});
				},

				update: function update(key, values) {
					var u = new Url(SERVICE_URL);
					u.path += "/" + encodeURIComponent(key);

					return $.ajax({
						url: u,
						method: "PUT",
						data: values
					}).always(function (resp) {
						$.crudStoreResp = resp.responseJSON;
						return resp;
					});
				},

				remove: function remove(key) {
					var u = new Url(SERVICE_URL);
					u.path += "/" + encodeURIComponent(key);

					return $.ajax({
						url: u,
						method: "DELETE"
					});
				}

			}));
		}
	}]);

	return Util;
}();

exports.default = new Util();


},{}]},{},[4]);
