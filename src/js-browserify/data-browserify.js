(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AppClass = function () {
	function AppClass() {
		_classCallCheck(this, AppClass);

		this._eventContext = null;
		this._loadingPanel = null;
		this._pages = [];
		this._routes = [];
		this._user = null;

		console.log("Making App");
	}

	_createClass(AppClass, [{
		key: "login",
		value: function login(user) {
			this._user = $.extend({}, user);
		}
	}, {
		key: "crudChartInstance",
		value: function crudChartInstance() {
			return $(".data-chart").dxChart("instance");
		}
	}, {
		key: "crudGridInstance",
		value: function crudGridInstance() {
			return $(".crud-grid").dxDataGrid("instance");
		}
	}, {
		key: "getFormInstance",
		value: function getFormInstance() {
			return $("#form").dxForm("instance");
		}
	}, {
		key: "getEventContext",
		value: function getEventContext() {
			return this._eventContext;
		}
	}, {
		key: "setEventContext",
		value: function setEventContext(v) {
			this._eventContext = v;
		}
	}, {
		key: "showLoading",
		value: function showLoading(message) {
			message = message || '正在加载...';

			this._loadingPanel = $("#loadpanel").dxLoadPanel({
				shadingColor: "rgba(0,0,0,0.4)",
				position: { of: "body" },
				visible: true,
				showIndicator: true,
				showPane: true,
				shading: true,
				closeOnOutsideClick: false,
				message: message
			}).dxLoadPanel("instance");
		}
	}, {
		key: "hideLoading",
		value: function hideLoading() {
			if (this._loadingPanel !== null) {
				this._loadingPanel.hide();
				this._loadingPanel = null;
			}
		}
	}, {
		key: "registerCrud",
		value: function registerCrud(opt) {
			this.pages.push(opt);
		}
	}, {
		key: "registerRoute",
		value: function registerRoute(opt) {
			this.routes.push(opt);
		}
	}, {
		key: "pages",
		get: function get() {
			return this._pages;
		}
	}, {
		key: "routes",
		get: function get() {
			return this._routes;
		}
	}, {
		key: "user",
		get: function get() {
			return this._user;
		}
	}]);

	return AppClass;
}();

var App = exports.App = new AppClass();


},{}],2:[function(require,module,exports){
'use strict';

var _app = require('./app');

var _util = require('./util');

var _util2 = _interopRequireDefault(_util);

var _page = require('./page');

require('./dx-ext');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var crudGridInstance = $(".data-grid").dxDataGrid("instance");
_app.App.registerRoute({
	name: 'data/order',
	onLoad: function onLoad(context, $container) {

		var origin = context.params.data;

		var orderdata = "",
		    title = void 0,
		    text = void 0,
		    series = void 0,
		    basic = '',
		    columns = void 0;

		if (origin == 'chart-order') {

			orderdata = new DevExpress.data.DataSource({
				store: $.crudStore(API('chart-order'))
			});

			title = '订单数据';

			text = '订单总金额';

			basic = '订单总量';

			series = [{ valueField: "total", name: "订单总量", type: 'bar', axis: 'basic' }, { valueField: "total_fee", name: "订单总金额", axis: 'sale' }];

			columns = [{
				dataField: 'date',
				caption: '日期'
			}, {
				dataField: 'total',
				caption: '全部订单量'
			}, {
				dataField: 'total_fee',
				caption: '订单总金额'
			}, {
				dataField: 'general_quantity',
				caption: '微信订单量'
			}, {
				dataField: 'gift_quantity',
				caption: '积分订单量'
			}, {
				dataField: 'general_fee',
				caption: '微信订单总金额'
			}, {
				dataField: 'gift_fee',
				caption: '积分订单总金额'
			}];
		} else if (origin == "chart-visit") {

			orderdata = new DevExpress.data.DataSource({
				store: $.crudStore(API('chart-visit'))
			});

			title = '浏览数据';

			text = '产品浏览量';

			basic = '二维码来源浏览量';

			series = [{ valueField: "qr_pv", name: "二维码来源浏览量", axis: 'sale' }, { valueField: "pv", name: "普通浏览量", type: 'bar', axis: 'basic' }];

			columns = [{
				dataField: 'date',
				caption: '日期'
			}, {
				dataField: 'total',
				caption: '产品详情页总浏览量'
			}, {
				dataField: 'qr_pv',
				caption: '二维码来源浏览量'
			}, {
				dataField: 'qr_uv',
				caption: '二维码来源浏览人数'
			}, {
				dataField: 'pv',
				caption: '普通来源浏览量'
			}, {
				dataField: 'uv',
				caption: '普通来源浏览人数'
			}];
		} else {

			orderdata = new DevExpress.data.DataSource({
				store: $.crudStore(API('chart-new'))
			});

			title = '新增数据';

			text = '新增用户';

			series = [{ valueField: "count", name: "新增用户", axis: 'sale' }];

			columns = [{
				dataField: 'date',
				caption: '日期'
			}, {
				dataField: 'count',
				caption: '新增用户'
			}, {
				dataField: 'qrcode',
				caption: '二维码来源新增'
			}, {
				dataField: 'other',
				caption: '其他来源新增'
			}];
		}

		var page = new _page.Page($container);

		page.crudLayout({ title: title });

		page.content.html('\n\t\t\t<div class="panel panel-default">\n\t\t\t    <div class="panel-heading font-bold">\n\t\t\t      \u67E5\u770B\u6570\u636E\n\t\t\t    </div>\n\t\t\t    <div class="panel-body">\n\t\t\t    \t<div class="toolbar1"></div>\n\t\t\t    \t<div class="data-chart"></div>\n\t\t\t    \t<div class="toolbar2"></div>\n\t\t\t    \t<div class="data-grid"></div>\n\t\t\t    </div>\n\t\t  \t</div>\n\t\t');

		var filterExpr = { start: '', end: '', type: '' },
		    applyFilterExpr = function applyFilterExpr() {
			var da = _app.App.crudChartInstance().option('dataSource'),
			    ds = $('.data-grid').dxDataGrid('instance').option('dataSource');
			orderdata = new DevExpress.data.DataSource({
				store: $.crudStore(API(origin + '?start=' + filterExpr.start + '&end=' + filterExpr.end + '&type=' + filterExpr.type))
			});
			INSERT();
		};

		var items = [{
			location: 'after',
			widget: 'dxDateBox',
			options: {
				displayFormat: "yyyy-MM-dd",
				dateSerializationFormat: 'yyyy-MM-dd',
				placeholder: '开始时间',
				onValueChanged: function onValueChanged(e) {
					filterExpr.start = e.value;
					applyFilterExpr();
				}
			}
		}, {
			location: 'after',
			widget: 'dxDateBox',
			options: {
				displayFormat: "yyyy-MM-dd",
				dateSerializationFormat: 'yyyy-MM-dd',
				placeholder: '结束时间',
				onValueChanged: function onValueChanged(e) {
					filterExpr.end = e.value;
					applyFilterExpr();
				}
			}
		}];

		if (origin == 'chart-new') {
			items.push({
				location: 'before',
				widget: 'dxSelectBox',
				options: {
					placeholder: '请选择类型',
					dataSource: [{ name: '浏览', type: 'view' }, { name: '订单', type: 'order' }],
					displayExpr: 'name',
					valueExpr: 'type',
					value: 'view',
					onValueChanged: function onValueChanged(e) {
						console.log(e);
						filterExpr.type = e.value;
						applyFilterExpr();
					}
				}
			});
		}

		page.content.find('.toolbar1').dxToolbar({
			items: items
		});

		function INSERT() {
			page.content.find(".data-chart").dxChart({
				palette: "violet",
				dataSource: orderdata,
				rotated: false,
				commonSeriesSettings: {
					argumentField: "date",
					type: 'line',
					bar: {
						barWidth: 20,
						barPadding: 0.5,
						barGroupPadding: 0.5
					}
				},
				margin: {
					bottom: 20
				},
				valueAxis: [{
					// name:'basic',
					// position:'left',
					grid: {
						visible: true
					}
					// title:{
					//     text:basic
					//     }
				}, {
					name: "sale",
					position: "right",
					grid: {
						visible: true
					},
					title: {
						text: text
					}
				}],
				series: series,
				legend: {
					verticalAlignment: "top",
					horizontalAlignment: "right",
					itemTextPosition: "right",
					orientation: 'vertical'
				},
				tooltip: {
					enabled: true,
					customizeTooltip: function customizeTooltip(arg) {
						return {
							html: '<div><div class=\'tooltip-header\'>' + arg.argument + '</div>' + arg.seriesName + ':' + arg.valueText + '</div>'
						};
					}
				}
			});

			page.content.find('.toolbar2').dxToolbar({
				items: [{
					location: 'after',
					widget: 'dxButton',
					options: {
						text: '导出',
						onClick: function onClick(e) {

							window.location = API(origin + '?export=true&start=' + filterExpr.start + '&end=' + filterExpr.end + '&type=' + filterExpr.type);
						}
					}
				}]
			});

			page.content.find('.data-grid').dxDataGrid({
				dataSource: orderdata,
				columns: columns
			});
		}

		INSERT();
	}
});

// App.registerRoute({
// 	name:'manage/gift',
// 	onLoad:function(context,$container){
// 		let page=new Page($container);
// 		let columns =[],title,tooltip,dataSource;
// 		let origin=context.params.data;

//    		if(origin){
//    			columns = [{
// 			caption:'id',
// 			dataField:'id'
// 		},{
// 			caption:'类型',
// 			dataField:'category',
// 			cellTemplate:function($c,d){
// 				let text = '';
// 				switch(d.value){
// 					case 'general':text='普通';break;
// 					case 'product':text='指定商品';break;
// 					case 'importer':text='指定进口商';break;
// 					case 'dealer':text='指定经销商'
// 				};
// 				$c.text(text)

// 			}
// 		},{
// 			caption:'进口商/商品',
// 			dataField:'importerName',
// 			cellTemplate:function($c,d){
// 				let text = d.data.importerName?d.data.importerName:d.data.productName
// 				$c.text(text)
// 			}
// 		},{
// 			caption:'满减类型',
// 			dataField:'couponName'

// 		}]

// 		title = '优惠券管理';

// 		tooltip = '优惠券类型管理列表';

// 		dataSource = $.crudStore(API('restful?_model=sys-coupon-category'))

//    		}else{
//    			columns = [{
//    				caption:"id",
//    				dataField:'id'
//    			},{
//    				caption:'优惠券名称',
//    				dataField:'coupon_name'
//    			},{
//    				caption:'活动名称',
//    				dataField:'name'
//    			},{
//    				caption:'使用量',
//    				dataField:'used'
//    			},{
//    				caption:'领取量',
//    				dataField:'get'
//    			},{
//    				caption:'库存剩余量',
//    				dataField:'sku',
//    				cellTemplate:function(c,d){

//    					let $datagird = page.content.find('.data-grid').dxDataGrid('instance').option('dataSource');

//    					$('<a>').text(d.value).css('text-decoration','underline').click(function(){
//    						let num = prompt('请输入要修改的数值');
//    					if(num) { 
//    					d.data.sku=num
//    					$.crudStore(API('restful?_model=sys-coupon')).update(d.data.id,d.data).then(res=>{
//    						DevExpress.ui.notify({message:'修改成功'},'success',1500)
//    						$datagird.load()
//    					}).fail(function(){
//    						DevExpress.ui.notify({message:'错误'},'warning',1500)
//    					}) }
//    					}).appendTo(c)
//    				}
//    			}];

//    			title = '优惠券使用';

//    			tooltip = '优惠券活动使用列表';

//    			dataSource = $.crudStore(API('restful?_model=sys-coupon'))
//    		}


// 		page.crudLayout({title:title});
// 		page.content.html(`<div class="panel panel-default">
// 			    <div class="panel-heading font-bold">
// 			      ${tooltip}
// 			    </div>
// 			    <div class="panel-body">
// 			    	<div class="toolbar1"></div>
// 			    	<div class="data-grid"></div>
// 			    </div>
// 		  	</div>`);

// 		page.content.find('.toolbar1').dxToolbar({
// 			items:[{
// 				location:'before',
// 				widget:'dxButton',
// 				options:{
// 					text:'增加',
// 					onClick:function(e){
// 						newData()
// 					}
// 				}
// 			}]
// 		});


// 		 page.content.find('.data-grid').dxDataGrid({
// 			dataSource:new DevExpress.data.DataSource({
// 				store:dataSource
// 			}), 
// 			columns:columns
// 		})


// 		function newData(){
// 			let newtitle,new_data,new_items;
// 			var now = new Date();
// 			if(origin){
// 			 newtitle = '新建优惠券';
// 			 new_data = new DevExpress.data.CustomStore({
// 				store:$.crudStore(API('restful?_model=use-coupon'))
// 			});

// 			 new_items = [{
// 				label:{text:'优惠券类型'},
// 				editorType:'dxSelectBox',
// 				dataField:'category',
// 				editorOptions:{
// 					dataSource:[{name:'普通',type:'general'},{name:'指定商品',type:'product'},{name:'指定进口商',type:'importer'}],
// 					displayExpr:'name',
// 					valueExpr:'type',
// 					onValueChanged(e){
// 						let $container = $('.formData').find(".new_tool_data").dxForm('instance');
// 						$container.option('formData.category',e.value);
// 						if(e.value == 'general'){
// 						 	$container.getEditor('importer_id').element().closest('.dx-field-item').addClass('hidden')
// 							$container.getEditor('dealer_id').element().closest('.dx-field-item').addClass('hidden');
// 							$container.option({'formData.importer_id':null,'formData.dealer_id':null});

// 						}else if(e.value == 'product'){
// 							$container.getEditor('importer_id').element().closest('.dx-field-item').removeClass('hidden')
// 							$container.getEditor('dealer_id').element().closest('.dx-field-item').addClass('hidden');
// 							$container.option('formData.dealer_id',null)
// 						}else {
// 							$container.getEditor('dealer_id').element().closest('.dx-field-item').removeClass('hidden')
// 							$container.getEditor('importer_id').element().closest('.dx-field-item').addClass('hidden')
// 							$container.option('formData.importer_id',null)
// 						}

// 					}
// 				}
// 			},
// 			{
// 				dataField:'importer_id',
// 				cssClass:'hidden',
// 				label:{text:'指定商品'},
// 				editorType:'dxSelectBox',
// 				editorOptions:{
// 					dataSource:$.crudStore(API('restful?_model=wine-product')),
// 					valueExpr:'id',
// 					displayExpr:'chname',
// 					placeholder:'指定商品',
// 					onValueChanged:function(ev){
// 					let $container = $('.formData').find(".new_tool_data")
// 					$container.dxForm('instance').option('formData.product_id',ev.value);

// 							}
// 				}
// 			},{
// 				dataField:'dealer_id',
// 				cssClass:'hidden',
// 				label:{text:'指定进口商'},
// 				editorType:'dxSelectBox',
// 				editorOptions:{
// 					dataSource:$.crudStore(API('restful?_model=sys-importer')),
// 					placeholder:'指定进口商',
// 					valueExpr:'id',
// 					displayExpr:'name',
// 					onValueChanged:function(ev){
// 					let $container = $('.formData').find(".new_tool_data")
// 					$container.dxForm('instance').option('formData.importer_id',ev.value);	
// 							}
// 				}
// 			},
// 			{
// 				label:{text:'满减类型'},
// 				editorType:'dxSelectBox',
// 				editorOptions:{
// 					dataSource:[{name:'直减',type:'direct'},{name:'满减',type:'full'}],
// 					valueExpr:'type',
// 					displayExpr:'name',
// 					onValueChanged:function(e){
// 					let $container = $('.formData').find(".new_tool_data").dxForm('instance');	

// 					$container.option('formData.loseType',e.value)

// 					if(e.value == 'direct'){
// 							$container.getEditor('full_amount').element().closest('.dx-field-item').addClass('hidden')
// 						// $container.itemOption('on_select_amount','items',[{label:{text:'直减'},dataField:'amount'}]);
// 					}else{
// 						$container.getEditor('full_amount').element().closest('.dx-field-item').removeClass('hidden')
// 					    // $container.itemOption('on_select_amount',"items",[{label:{text:'满'},dataField:'full_amount'},{label:{text:'减'},dataField:'amount'}])
// 					}


// 					}
// 				}
// 			},{
// 				itemType:'group',
// 				colCount:2,
// 				caption:'on_select_amount',
// 				items:[{
// 					label:{text:'满'},
// 					dataField:'full_amount'
// 				},{
// 					label:{text:'减'},
// 					dataField:'amount'
// 				}]

// 			}]
// 			}else{
// 				newtitle = '新建优惠券活动';
// 				new_data = new DevExpress.data.CustomStore({
// 					store:$.crudStore(API('restful?_model=sys-coupon'))
// 				});

// 				new_items=[{
// 					label:{text:'活动名称'},
// 					dataField:'name'
// 				},{
// 					label:{text:'通用类型'},
// 					dataField:'coupon_category_id',
// 					editorType:'dxSelectBox',
// 					editorOptions:{
// 						dataSource:$.crudStore(API('restful?_model=sys-coupon-category')),
// 						displayExpr:'couponName',
// 						valueExpr:'id',
// 						onValueChanged:function(e){
// 							let $container = $('.formData').find('.new_tool_data').dxForm('instance');
// 							$container.option('formData.coupon_category_id',e.value)
// 						}
// 					}
// 				},{
// 					label:{text:'领取有效期'},
// 					itemType:'group',
// 					colCount:2,
// 					items:[{
// 						dataField:'can_get_start',
// 						label:{text:'自'},
// 						editorType:'dxDateBox',
// 						editorOptions:{
// 							type: "date",
// 							displayFormat: 'yyyy-MM-dd',
// 							dateSerializationFormat: "yyyy-MM-dd",
// 							editorOptions: {
// 							displayFormat: 'yyyy-MM-dd',
// 							dateSerializationFormat: 'yyyy-MM-dd'
// 					}
// 						}
// 					},{
// 						label:{text:'至'},
// 						dataField:'can_get_end',
// 						editorType:'dxDateBox',
// 						editorOptions:{
// 							type: "date",
// 							displayFormat: 'yyyy-MM-dd',
// 							dateSerializationFormat: "yyyy-MM-dd",
// 							editorOptions: {
// 							displayFormat: 'yyyy-MM-dd',
// 							dateSerializationFormat: 'yyyy-MM-dd'
// 					}
// 						}
// 					}]
// 				},{
// 					label:{text:'使用有效期'},
// 					itemType:'group',
// 					colCount:2,
// 					items:[
// 					{	editorType:'dxRadioGroup',
// 						editorOptions:{
// 							layout: "horizontal",
// 							items:[{text:'固定日期',value:'yes'},{text:'浮动日期',value:'no'}],
// 							value:'yes',
// 							valueExpr:'value',
// 							onValueChanged:function(e){
// 								let value = e.value=='yes'?'no':'yes'
// 							let $container = $('.formData').find('.new_tool_data').dxForm('instance');
// 							$container.option('formData.enable_use_date',e.value);
// 							$container.option('formData.enable_use_days',value);
// 							if(e.value ==  'yes'){
// 								$container.getEditor('can_use_start').element().closest(".dx-field-item").removeClass('hidden');
// 								$container.getEditor('can_use_end').element().closest(".dx-field-item").removeClass('hidden');
// 								$container.getEditor('can_use_days').element().closest(".dx-field-item").addClass('hidden');

// 							}else{
// 								$container.getEditor('can_use_start').element().closest(".dx-field-item").addClass('hidden');
// 								$container.getEditor('can_use_end').element().closest(".dx-field-item").addClass('hidden');
// 								$container.getEditor('can_use_days').element().closest(".dx-field-item").removeClass('hidden');

// 							}

// 							}
// 						}
// 					},
// 					{
// 							editorType:'dxTextBox',
// 							cssClass:'hidden'
// 					},
// 					{
// 						dataField:'can_use_start',
// 						label:{text:'自'},
// 						editorType:'dxDateBox',
// 						editorOptions:{
// 							type: "date",
// 							displayFormat: 'yyyy-MM-dd',
// 							dateSerializationFormat: "yyyy-MM-dd",
// 							editorOptions: {
// 							displayFormat: 'yyyy-MM-dd',
// 							dateSerializationFormat: 'yyyy-MM-dd'
// 					}
// 						}
// 					},{
// 						dataField:'can_use_end',
// 						label:{text:'至'},
// 						editorType:'dxDateBox',
// 						editorOptions:{
// 							type: "date",
// 							displayFormat: 'yyyy-MM-dd',
// 							dateSerializationFormat: "yyyy-MM-dd",
// 							editorOptions: {
// 							displayFormat: 'yyyy-MM-dd',
// 							dateSerializationFormat: 'yyyy-MM-dd'
// 					}
// 						}
// 					},{
// 						label:{text:'自领取日期起'},
// 						dataField:'can_use_days',
// 						cssClass:'hidden'
// 					}
// 					]
// 				},{
// 					label:{text:'使用条件'},
// 					editorType:'dxSelectBox',
// 					editorOptions:{
// 						dataSource:[{name:'新用户',value:'new'},{name:'老用户',value:'old'},{name:'新老用户',value:'all'}],
// 						displayExpr:"name",
// 						valueExpr:'value',
// 						onValueChanged:function(e){
// 							let $container = $('.formData').find('.new_tool_data').dxForm('instance');
// 							$container.option('formData.condition',e.value);

// 						}
// 					}
// 				},{
// 					label:{text:'库存量'},
// 					dataField:'sku'
// 				}]

// 			}


// 			$("#popups").html('<div>');

// 			$("<div/>").appendTo($("#popups")).dxPopup({
// 					width:800,
// 					height:600,
// 					visible:true,
// 					title:newtitle,
// 					contentTemplate: function(contentElement) {
// 					return '<div class="formData"><div class="new_tool_data"></div><div class="toolbar2" style="margin:30px auto;display:block"></div></div>'
// 					},
// 					onContentReady:function(e){
// 						let $content = e.component.content();

// 						$content.find('.new_tool_data').dxForm({
// 						dataSource:new_data,
//             			validationGroup: "crud",
//             			items:new_items
// 						});

// 						$content.find('.toolbar2').dxButton({
// 							type:'success',
// 							text:'创建',
// 							onClick:function(){
// 								let data = $('.formData').find('.new_tool_data').dxForm('instance').option('formData');
// 						console.log(data);
// 						let route = ''
// 						if(origin){
// 							route = 'restful?_model=sys-coupon-category'
// 						}else{
// 							route = 'restful?_model=sys-coupon';
// 							data.enable_use_date=data.enable_use_date?data.enable_use_date:'yes';
// 							data.enable_use_days=data.enable_use_days?data.enable_use_days:'no'
// 						};

// 						$.ajax({url:API(route),method:'post',data:data}).always(function(resp){
// 							console.log(resp)
// 							if(resp.status == 'success'){
// 								DevExpress.ui.notify({message:'创建成功'},'success',1500);
// 							$("#popups").find('div').remove();
// 								 page.content.find('.data-grid').dxDataGrid('instance').option('dataSource').reload()
// 							}else{
// 								DevExpress.ui.notify({message:function(){

// 									let text = "提示:"+resp.responseJSON.message;
// 									return text
// 								}},'warning',1500)
// 							}
// 								})

// 							}
// 						})
// 					}
// 				})
// 		}
// 	}
// })

_app.App.registerRoute({
	name: 'modify_pwd',
	onLoad: function onLoad(content, $container) {
		var page = new _page.Page($container);
		page.content.html('<div class="panel panel-default">\n\t\t    <div class="panel-heading font-bold">\n\t\t\t     \u4FEE\u6539\u5BC6\u7801\n\t\t\t    </div>\n\t\t\t    <div class="crud-container crud-reset row">\n\t\t\t      \t<div class="col-md-12">\n\t\t\t    \t\t<div class="data-grid" style=\'padding:50px;border:0\'></div>\n\t\t\t    \t</div>\n\t\t\t      \t<div class="col-md-12">\n\t\t\t     \t\t<div class="toolbar2" style=\'padding:0 130px\'></div>\n\t\t\t      \t</div>\n\t\t\t    </div>\n\n\t\t  \t</div>');

		page.content.find('.data-grid').dxForm({
			readOnly: false,
			showColonAfterLabel: true,
			showValidationSummary: false,
			colCount: 1,
			items: [{
				dataField: 'old_password',
				label: { text: '原密码' },
				editorOptions: {
					mode: 'password'
				},
				validationRules: [{
					type: "required"
				}]
			}, {
				dataField: 'password',
				label: { text: '新密码' },
				editorOptions: {
					mode: 'password'
				},
				validationRules: [{
					type: "required"
				}]
			}, {
				dataField: 'password_confirmation',
				label: { text: "确认密码" },
				editorOptions: {
					mode: 'password'
				},
				validationRules: [{
					type: "required"
				}]
			}]
		});

		page.content.find('.toolbar2').dxToolbar({
			items: [{
				location: 'before',
				widget: "dxButton",
				options: {
					text: '确认修改',
					type: 'success',
					onClick: function onClick() {
						var data = page.content.find('.data-grid').dxForm('instance').option('formData');
						$.ajax({
							url: API('auth/change-password'),
							headers: { Authorization: "bearer " + localStorage.accessToken },
							method: 'post',
							data: data,
							success: function success(res) {

								DevExpress.ui.notify({ message: '修改成功' }, 'success', 1500);
								setTimeout(function () {
									window.location.href = 'login.html';
								}, 1500);
							},
							error: function error(err) {
								DevExpress.ui.notify({ message: function message() {
										var text = '提示 ：' + err.responseJSON.message;
										return text;
									} }, 'warning', 2000);
							}
						});
					}
				}
			}]
		});
	}

});


},{"./app":1,"./dx-ext":3,"./page":4,"./util":5}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

jQuery.fn.extend({

	crudForm: function crudForm(options) {
		if ((typeof options === "undefined" ? "undefined" : _typeof(options)) === 'object') {}
	},

	dxInit: function dxInit() {
		$("[dx-toggle]", $(this)).each(function () {
			if ($(this).hasClass("dx-widget")) {
				return true;
			}

			var type = $(this).attr("dx-toggle"),
			    options = $(this).attr("dx-options") || "{}";

			try {
				options = JSON.parse(options);
			} catch (ex) {

				options = {};
			}

			switch (type) {
				case "button":
					$(this).dxButton(options);
					break;

				case "select":
					$(this).dxSelectBox(options);
					break;

				case "text":
					$(this).dxTextBox(options);
					break;

				case "textarea":
					$(this).dxTextArea(options);
					break;

				case "number":
					$(this).dxNumberBox(options);
					break;

				case "check":
					$(this).dxCheckBox($.extend({
						text: $(this).text()
					}, options));
					break;

				default:
					console.log("Error: unsupported type " + type);
					break;
			}
		});

		return $(this);
	},
	dxDelete: function dxDelete(url, option) {
		$("<a>").text("删除").click(function () {
			var id = option.key.id;
			DevExpress.ui.dialog.confirm("确定删除吗?", "请确认").done(function (sele) {

				if (sele) {

					$.ajax({
						type: "delete",
						url: $.config('apiUrl') + "restful/" + id + "?_model=" + url
					}).then(function (a) {

						if (a.status = "success") {

							DevExpress.ui.notify({
								message: function message() {

									var gridComponent = $("#content .crud-grid").dxDataGrid('instance');

									gridComponent.option('dataSource').load();

									return "成功删除";
								}
							}, "success", 1000);
						} else {

							DevExpress.ui.notify({

								message: function message() {

									return "操作失败";
								}
							}, "warning", 1000);
						}
					});
				}
			});
		}).appendTo(this);
		return this;
	},
	dxUpdown: function dxUpdown(url, option) {
		var text = option.value == 'yes' ? '下线' : '上线',
		    off = option.value == 'yes' ? 'no' : 'yes';

		$('<a>').text(text).css({
			'text-decoration': 'underline'
		}).click(function () {
			$.post($.config('apiUrl') + "state-change", {
				'changeType': url,
				'changeKey': 'online',
				'changeValue': off,
				'changeId': option.key.id
			}, function (d) {
				if (d.status) {
					DevExpress.ui.notify({
						message: function message() {
							var gridComponent = $("#content .crud-grid").dxDataGrid('instance');
							gridComponent.option('dataSource').load();
							return off == 'yes' ? '上线成功' : '下线成功';
						}
					}, "success", 1000);
				} else {
					DevExpress.ui.notify({
						message: function message() {
							return '操作失败';
						}
					}, "warning", 1000);
				}
			});
		}).appendTo(this);
		return this;
	},
	dxWeight: function dxWeight(url, option) {

		$('<a>').text(option.value == null ? '无' : option.value).css({
			'text-decoration': 'underline'
		}).click(function () {
			var number = prompt('请输入权重', option.value);
			if (number == null) {
				return;
			};
			if (number >= 0 && number <= 100) {
				$.post($.config('apiUrl') + "state-change", {
					'changeType': url,
					'changeKey': 'weight',
					'changeValue': number,
					'changeId': option.key.id
				}, function (d) {
					if (d.status) {
						DevExpress.ui.notify({
							message: function message() {
								var gridComponent = $("#content .crud-grid").dxDataGrid('instance');
								gridComponent.option('dataSource').load();
								return '修改成功';
							}
						}, "success", 1000);
					} else {
						DevExpress.ui.notify({
							message: function message() {
								return '操作失败';
							}
						}, "warning", 1000);
					}
				});
			} else {
				DevExpress.ui.notify({
					message: '请输入正确的值'
				}, 'warning', 1000);
			}
		}).appendTo(this);
		return this;
	},
	dxsetsku: function dxsetsku(url, option) {
		$("<a>").text(option.value ? option.value : '0').css({
			'text-decoration': 'underline'
		}).click(function () {
			var num = prompt('请输入库存', option.value ? option.value : '0');
			if (num >= 0) {
				$.post($.config('apiUrl') + "state-change", {
					'changeType': url,
					'changeKey': 'sku',
					'changeValue': num,
					'changeId': option.key.id
				}, function (res) {
					if (res.status) {
						DevExpress.ui.notify({
							message: function message() {
								var gridComponent = $("#content .crud-grid").dxDataGrid('instance');
								gridComponent.option('dataSource').load();
								return '修改成功';
							}
						}, "success", 1000);
					} else {
						DevExpress.ui.notify({
							message: function message() {
								return '操作失败';
							}
						}, "warning", 1000);
					}
				});
			} else {
				DevExpress.ui.notify({
					message: '请输入正确的值'
				}, 'warning', 1000);
			}
		}).appendTo(this);
		return this;
	}
});

var DxExtInstance = klass(function ($container, config, opt) {
	this.$container = $container;
	this.opt = $.extend({}, opt);
	this.config = $.extend({}, config);
	this.user = {};

	this.config.init.call(this, this.$container, opt);

	this.setValue(opt.value || undefined);
}).statics({
	validate: function validate(formInstance) {
		var editors = [];

		function _check(o) {
			if (o.dataField) {
				var editor = formInstance.getEditor(o.dataField);
				if (editor && typeof editor.isExtInstance === 'function') {
					editors.push(editor);
				}
			} else if (o.items) {
				$.each(o.items, function (i, item) {
					_check(item);
				});
			}
		}

		_check({
			items: formInstance.option('items')
		});

		var result = true;
		$.each(editors, function (i, e) {
			if (!e.validate()) result = false;
		});

		return result;
	},

	define: function define(config) {
		var obj = {};
		obj[config.name] = function () {
			var instance = $(this).data('dx-ext-instance');

			if (arguments.length > 0 && _typeof(arguments[0]) === 'object') {

				if (!instance) {
					instance = new DxExtInstance($(this), config, arguments[0]);
					$(this).data('dx-ext-instance', instance);
				}

				return $(this);
			} else if (arguments.length > 0 && typeof arguments[0] === 'string') {
				var args = [];
				for (var i = 1; i < arguments.length; i++) {
					args.push(arguments[i]);
				}

				console.log("call " + arguments[0]);
				console.log(args);

				return instance[arguments[0]].apply(instance, args);
			} else {
				return undefined;
			}
		};

		jQuery.fn.extend(obj);
	}
}).methods({
	isExtInstance: function isExtInstance() {
		return true;
	},

	validate: function validate() {
		if (this.config.validate) {
			return this.config.validate.call(this);
		} else {
			return true;
		}
	},

	getValue: function getValue() {
		if (this.config.onGetValue) return this.config.onGetValue.call(this);
		return this.value;
	},

	getOption: function getOption() {
		return this.opt;
	},

	notifyValueChanged: function notifyValueChanged() {
		this.$container.trigger("dxExtValueChanged");
	},

	setValue: function setValue(v) {
		this.value = v;
		this.notifyValueChanged();
		this.render();
	},

	render: function render() {
		this.config.render && this.config.render.call(this, this.$container);
	},

	setAria: function setAria() {
		return undefined;
	},

	option: function option(k, v) {

		if (k === 'value' && v !== undefined) {
			return this.setValue(v);
		} else if (k === 'value') {
			if (this.config.beforeGetValue) {
				this.config.beforeGetValue.call(this);
			}
			return this.getValue();
		}

		if (v === undefined) {
			console.log("query option " + k);

			return this.opt[k];
		} else {
			var orig = this.opt[k];
			this.opt[k] = v;

			if (this.config.onOptionChanged) {
				this.config.onOptionChanged.call(this, {
					option: k,
					value: v,
					oldValue: orig
				});
			}
		}
	},

	element: function element() {
		return this.$container;
	},

	instance: function instance() {
		return this;
	},

	on: function on(evt, fn) {
		var _this = this;

		console.log("on " + evt);

		if (evt === 'valueChanged') {
			this.$container.on("dxExtValueChanged", function () {
				fn.call(_this, {
					component: _this,
					value: _this.value
				});
			});
		}
	},

	reset: function reset() {
		console.log("call reset");
	}
});

DxExtInstance.define({
	name: 'propertyList',
	init: function init($container, option) {},

	render: function render($container) {
		var _this2 = this;

		var _this = this;
		var DATA = this.getValue() || [];

		var te1 = "<div class='row' style='margin-bottom:10px'><div class='col-sm-12' >" + "<div class='pull-left' style='margin-right:30px'><div class='property'></div></div>" + "</div></div>" + "<div class='row ' style='margin-bottom:10px'><div class='col-sm-12 propertydata' >" + "</div></div>";

		var te4 = "<div class='pull-left' ><div class='property_add'></div></div>";

		var post_list = function post_list(ID) {
			console.log(_this2.option);

			$.get($.config('apiUrl') + ("restful/" + ID + "?_model=product-attr-group")).then(function (res) {
				var labels = res.data.labels;
				var formItem = [];

				DATA.forEach(function (it) {
					var form = {
						colCount: 5,
						itemType: 'group',
						items: []
					};

					$.each(labels, function (i, items) {

						var item = {
							label: { text: items.name },
							dataField: "values",
							validationRules: [{
								type: "required",
								message: '请选择' + items.name
							}],
							editorType: 'dxSelectBox',
							editorOptions: {
								dataSource: items.values,
								valueExpr: 'id',
								displayExpr: 'value',
								onValueChanged: function onValueChanged(e) {
									console.log(e);
								}
							}
						};

						form['items'].push(item);
					});

					var origin_price = [{
						label: { text: '原价' },
						dataField: "origin_price",
						value: it.origin_price,
						validationRules: [{
							type: "required",
							message: '请输入原价'
						}]

					}, {
						label: { text: '促销价' },
						dataField: "price",
						value: it.price,
						validationRules: [{
							type: "required",
							message: '请输入促销价'
						}]
					}, {
						label: { text: '库存' },
						dataField: "sku",
						value: it.sku,
						validationRules: [{
							type: "required",
							message: '请输入库存'
						}]
					}, {
						editorType: 'dxButton',
						editorOptions: {
							text: '删除',
							onClick: function onClick() {}
						}
					}];

					form['items'] = form['items'].concat(origin_price);

					formItem.push(form);
				});

				console.log(formItem);
				$container.find('.propertydata').dxForm({
					showColonAfterLabel: true,
					showValidationSummary: false,
					validationGroup: "crud",
					alignItemLabels: true,
					alignItemLabelsInAllGroups: true,
					items: formItem
				});

				$container.find('.property_add').dxButton({
					text: "增加",
					onClick: function onClick() {
						var post_data = {
							origin_price: '',
							price: '',
							sku: '',
							labels: []
						};
						var label = {
							value: ''
						};
						labels.forEach(function (its) {
							post_data['labels'].push(label['attr_label_id'] = its.id);
						});
						_this.setValue(DATA.push(post_data));
						post_list(ID);
					}
				});
			});
		};

		$container.css({ padding: '10px 0' });

		var te3 = "<div class='row' style='margin-bottom:10px'><div class='property_list'></div></div>" + te1 + te4;

		$container.html(te3);

		$container.find('.property_list').dxSelectBox({
			dataSource: $.crudStore(API('restful?_model=product-attr-group')),
			displayExpr: 'name',
			valueExpr: 'id',
			onValueChanged: function onValueChanged(e) {
				var ID = e.value;

				post_list(ID);
			}

		});
	}
});

DxExtInstance.define({
	name: 'imagesdetail',
	init: function init($container, option) {
		var _this = this;

		option = $.extend({}, {
			single: false
		}, option);
		console.log(option);
		$container.css({ padding: '10px 0' });
		$container.html("<div class='images'></div><div class='file image-uploader'></div>");

		var labelText = '';
		if (option.labelText) {
			labelText = option.labelText;
		} else if (option.imageWidth && option.imageHeight) {
			labelText = '建议宽高比是' + option.imageWidth + 'px x ' + option.imageHeight + 'px。';
		}

		$container.find(".file").dxFileUploader({
			selectButtonText: "上传",
			labelText: labelText,
			name: 'file',
			accept: "image/*",
			uploadMode: "instantly",
			uploadUrl: $.config('apiUrl') + "file-upload",
			onUploaded: function onUploaded(e) {
				var pat = JSON.parse(e.request.response).data;

				var value = _this.getValue();

				if (!value) {
					value = option.single ? "" : [];
				};
				console.log(value);
				if (option.single) {
					value = pat.imgSrv;
				} else {
					value.push(pat.imgSrv);
				}
				console.log(value);

				_this.setValue(value);
			}
		});

		// $container.find(".file .dx-button-content").html('<i class="iconfont">&#xe623;</i>');
	},

	render: function render($container) {
		// function imageUrl(url) {
		// 	if(!url.match(/imageView2/)) {
		// 		url += "?imageView2/1/w/100/h/57";
		// 	}

		// 	return url;
		// }

		var _this = this,
		    value = this.getValue();

		var $images = $container.find(".images");
		$images.html("");

		if (value) {
			var option = this.getOption();

			if (option.single) value = [value];
			console.log(option);
			$.each(value, function (i, dat) {
				$("<div>").append($("<img>").attr("src", dat)).append($("<span>").text("删除").click(function () {

					if (!option.single) {
						value.splice(i, 1);
						_this.setValue(value);
					} else {
						value = "";
						_this.setValue(value);
					}

					$(this).parent().remove();
				})).appendTo($images);
			});
		}
	}
});

DxExtInstance.define({
	name: 'ImageUploader',
	init: function init($container, option) {
		var _this = this;

		option = $.extend({}, {
			single: false
		}, option);

		$container.css({ padding: '10px 0' });
		$container.html("<div class='images'></div><div class='file image-uploader'></div>");

		var labelText = '';
		if (option.labelText) {
			labelText = option.labelText;
		} else if (option.imageWidth && option.imageHeight) {
			labelText = '建议宽高比是' + option.imageWidth + 'px x ' + option.imageHeight + 'px。';
		}

		$container.find(".file").dxFileUploader({
			selectButtonText: "上传",
			labelText: labelText,
			name: 'file',
			accept: "image/*",
			uploadMode: "instantly",
			uploadUrl: $.config('apiUrl') + "file-upload",
			onUploaded: function onUploaded(e) {
				var pat = JSON.parse(e.request.response).data;

				var value = _this.getValue();

				if (value === undefined) {
					value = option.single ? "" : [];
				};

				if (option.single) {
					value = pat.imgSrv;
				} else {
					value.push(pat);
				}

				_this.setValue(value);
			}
		});

		// $container.find(".file .dx-button-content").html('<i class="iconfont">&#xe623;</i>');
	},

	render: function render($container) {
		function imageUrl(url) {
			if (!url.match(/imageView2/)) {
				url += "?imageView2/1/w/100/h/57";
			}

			return url;
		}

		var _this = this,
		    value = this.getValue();

		var $images = $container.find(".images");
		$images.html("");

		if (value) {
			var option = this.getOption();

			if (option.single) value = [value];

			$.each(value, function (i, dat) {
				$("<div>").append($("<img>").attr("src", imageUrl(option.single ? dat : dat.imgSrv))).append($("<span>").text("删除").click(function () {

					if (!option.single) {
						value.splice(i, 1);
						_this.setValue(value);
					} else {
						value = "";
						_this.setValue(value);
					}

					$(this).parent().remove();
				})).appendTo($images);
			});
		}
	}
});

DxExtInstance.define({
	name: 'VideoUploader',
	init: function init($container, option) {
		var _this = this;

		option = $.extend({}, {
			single: false
		}, option);

		$container.html("<div class='video-url'></div><div class='file'></div>");

		$container.find(".file").dxFileUploader({
			selectButtonText: "上传",
			labelText: '',
			name: 'file',
			accept: "video/mp4,video/x-m4v,video/*",
			uploadMode: "instantly",
			uploadUrl: $.config('apiUrl') + "file-upload",
			onUploaded: function onUploaded(e) {
				var pat = JSON.parse(e.request.response).data;

				_this.setValue(pat.imgSrv);
			}
		});

		$container.find(".video-url").dxTextBox({
			onValueChanged: function onValueChanged(e) {
				_this.setValue(e.value);
			},
			placeholder: '视频地址'
		});
	},
	render: function render($container) {
		var v = $container.find(".video-url").dxTextBox("option", "value");
		if (v != this.getValue()) {
			$container.find(".video-url").dxTextBox("option", "value", this.getValue());
		}
	}
});

DxExtInstance.define({
	name: 'GrapeList',
	init: function init($container, option) {},
	render: function render($container) {
		var v = this.getValue(),
		    _this = this;

		v = v || [];

		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">葡萄</div>' + '<div class="pull-left"><div class="grape-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin: 0 10px;">种植面积占比</div>' + '<div class="pull-left"><div class="grape-input-area"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="grape-remove"></div>' + '</div></div>';

		$container.html('');
		$.each(v, function (i, item) {
			var $c = $(tpl);

			$c.find(".grape-input-name").dxTextBox({
				width: 200,
				value: item.name,
				onValueChanged: function onValueChanged(e) {
					v[i].name = e.value;
				}
			});

			$c.find(".grape-input-area").dxNumberBox({
				width: 80,
				value: item.proportion,
				showSpinButtons: true,
				// format: "#0%",
				onValueChanged: function onValueChanged(e) {
					v[i].proportion = e.value;
				}
			});

			$c.find(".grape-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c.appendTo($container);
		});

		$("<div/>").dxButton({
			text: '增加',
			onClick: function onClick() {
				v.push({
					name: '',
					area: 0
				});
				_this.setValue(v);
			}
		}).appendTo($container);
	}
});

DxExtInstance.define({
	name: 'GrapeDeployList',

	validate: function validate() {
		try {
			var result = DevExpress.validationEngine.validateGroup(this.validationGroup);
			if (result.isValid) {
				var value = this.getValue(),
				    err = false;
				value = value || [];

				this.opt.isValid = false;
				this.opt.validationError = "";

				if (this.opt.mode === 'single') {} else if (this.opt.mode === 'blend') {
					var total = 0;
					$.each(value, function (i, v) {
						total += parseFloat(v.scale);
					});

					if (value.length < 2) {
						err = '至少配置两种葡萄';
					} else if (total > 100) {
						err = '比例设置超过100';
					}
				} else {}

				if (!err) {
					this.opt.isValid = true;
				} else {
					this.opt.validationError = err;
				}
			}

			this.setValue(value);
		} catch (e) {
			this.opt.isValid = false;
		}

		return this.opt.isValid;
	},

	init: function init($container, option) {
		this.opt.mode = this.opt.mode || 'single';
		this.validationGroup = 'GrapeDeployList-' + Math.floor(Math.random() * 1000);

		console.log("init GrapeDeployList");

		// this.setValue(null);
	},

	render: function render($container) {
		var v = this.getValue(),
		    _this = this;

		v = v || [];

		var validationGroup = this.validationGroup;
		var vtpl = '<div class="dx-ex-validation-error"></div>';
		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">葡萄</div>' + '<div class="pull-left"><div class="grape-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin: 0 10px;">采收时间</div>' + '<div class="pull-left"><div class="grape-input-time"></div></div>' + (this.opt.mode === 'blend' ? '<div class="pull-left" style="line-height: 36px; margin: 0 10px;">比例</div>' + '<div class="pull-left"><div class="grape-input-area"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="grape-remove"></div></div>' : '') + '</div></div>';

		$container.html('');

		if (!this.opt.isValid && this.opt.validationError) {
			$('<div class="dx-ex-validation-error">' + this.opt.validationError + '</div>').appendTo($container);
		}

		$.each(v, function (i, item) {
			var $c = $(tpl);

			$c.find(".grape-input-name").dxTextBox({
				width: 200,
				value: item.name,
				onValueChanged: function onValueChanged(e) {
					v[i].name = e.value;
				}
			}).dxValidator({
				validationGroup: validationGroup,
				validationRules: [{
					type: 'required'
				}]
			});

			if (_this.opt.mode === 'blend') {
				$c.find(".grape-input-area").dxNumberBox({
					width: 80,
					value: item.scale,
					showSpinButtons: true,
					// format: "#0%",
					onValueChanged: function onValueChanged(e) {
						v[i].scale = e.value;
					}
				}).dxValidator({
					validationGroup: validationGroup,
					validationRules: [{
						type: 'required'
					}]
				});
			}

			$c.find('.grape-input-time').dxDateBox({
				value: item.harvestDate,
				type: "date",
				displayFormat: 'yyyy-MM-dd',
				dateSerializationFormat: "yyyy-MM-dd",
				onValueChanged: function onValueChanged(e) {
					v[i].harvestDate = e.value;
				}
			});

			$c.find(".grape-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c.appendTo($container);
		});

		if (this.opt.mode === 'blend') {
			$("<div/>").dxButton({
				text: '增加',
				onClick: function onClick() {
					v.push({
						name: '',
						scale: 100,
						harvestDate: null
					});
					_this.setValue(v);
				}
			}).appendTo($container);
		}

		if (v.length == 0) {
			$container.html('未选择调配类型');
		}
	},

	onOptionChanged: function onOptionChanged(e) {
		if (e.option === 'mode') {
			var value = this.getValue();

			this.opt.isValid = true;
			this.opt.validationError = "";

			if (value) {
				if (e.value === 'single') {
					if (value.length >= 1) {
						value = value.slice(0, 1);
					} else {
						value.push({
							name: '',
							scale: 100
						});
					}
				} else if (e.value === 'blend') {
					value = value.slice(0, 20);

					while (value.length < 2) {
						value.push({
							name: '',
							scale: 0
						});
					}
				}

				this.setValue(value);
			} else {
				if (e.value === 'single') {
					value = [{
						name: '',
						scale: 100
					}];
				} else if (e.value === 'blend') {
					value = [];

					while (value.length < 2) {
						value.push({
							name: '',
							scale: 0
						});
					}
				}

				this.value = value;
				this.render();
			}
		} else if (e.option === 'isValid') {} else if (e.option === 'validationError') {}
	}
});

DxExtInstance.define({
	name: 'dxEditor',
	init: function init($container, option) {

		var _this = this;
		option = $.extend({}, {
			single: false
		}, option);

		$container.html('<div contenteditable="true" class="ckeditor"></div>');

		// editorH1 editorH3

		// CKEDITOR.basePath = '../libs/ckeditor/';
		this.editor = CKEDITOR.replace($container.find('.ckeditor')[0], {
			extraPlugins: 'uploadimage,simpleimage,wine',
			height: 300,
			allowedContent: true,

			// Upload images to a CKFinder connector (note that the response type is set to JSON).
			uploadUrl: $.config('apiUrl') + "file-upload",

			// Load the default contents.css file plus customizations for this sample.
			contentsCss: [CKEDITOR.basePath + 'contents.css']
		});

		this.editor.setData("", {
			internal: true,
			noSnapshot: true
		});

		this.editor.on("change", function (evt) {
			_this.setValue(_this.editor.getData());
		});

		this.editor.on("notificationHide", function (evt) {
			_this.setValue(_this.editor.getData());
		});

		this.editor.on("wine.insert", function (evt) {
			var editor = _this.editor;

			$("<div>").appendTo($("body")).dxPopup({
				title: '请选择葡萄酒',
				visible: true,
				width: 400,
				height: 200,
				onContentReady: function onContentReady(e) {
					var $content = e.component.content();

					var instance = $(".select-wine").dxSelectBox({
						dataSource: $.crudStore(API("restful?_model=wine-product")),
						searchEnabled: true,
						valueExpr: 'id',
						itemTemplate: function itemTemplate(data, index, $el) {
							if (data) {
								$("<span>" + data.chname + " (" + data.name + ")</span>").appendTo($el);
							}
						},
						displayExpr: function displayExpr(data) {
							return data ? data.chname + " (" + data.name + ")" : "";
						}
					}).dxSelectBox('instance');

					$(".toolbar", $content).dxToolbar({
						items: [{
							location: 'after',
							widget: 'dxButton',
							options: {
								type: 'success',
								text: '确定',
								onClick: function onClick() {
									var v = instance.option('value');
									if (v) {
										editor.insertHtml("<p>{{drink:" + v + "}}</p>");
									}
									e.component.hide();
								}
							}
						}, {
							location: 'after',
							widget: 'dxButton',
							options: {
								type: 'normal',
								text: '取消',
								onClick: function onClick() {
									e.component.hide();
								}
							}
						}]
					});
				},
				contentTemplate: function contentTemplate(contentElement) {
					$('<div><div class="form-container" style="padding-bottom: 20px;"><div class="select-wine"></div></div><div class="toolbar"></div></div>').appendTo(contentElement);
				}
			});
		});

		this.editor.on("simpleimage.insert", function (evt) {
			var editor = _this.editor;

			$("<input type='file' accept='image/*'/>").click().change(function () {
				var formData = new FormData();

				if (this.files.length > 0) {
					formData.append('file', this.files[0]);

					$.ajax({
						url: $.config('apiUrl') + "file-upload",
						type: "POST",
						data: formData,
						processData: false,
						contentType: false
					}).then(function (resp) {
						if (resp && resp.status === "success") {
							editor.insertHtml("<img src='" + resp.data.imgSrv + "?imageView2/0/h/120'/>");
						} else {
							DevExpress.ui.dialog.alert('文件上传失败', '操作失败');
						}
					});
				}
			});
		});
	},

	render: function render($container) {
		if (this.editor) {
			var html = this.editor.getData();
			if (!html && this.getValue()) {
				var _this = this;
				// 直接调用setData偶尔会失败，原因未知
				var editable = _this.editor.editable();

				if (editable) {
					editable.setData(_this.getValue());
				} else {
					_this.editor.setData(_this.getValue(), {
						internal: true,
						noSnapshot: true,
						callback: function callback() {}
					});
				}
			}
		}
	},

	beforeGetValue: function beforeGetValue() {
		if (this.editor) {
			this.value = this.editor.getData();
			this.notifyValueChanged();
		}
	}
});

DxExtInstance.define({
	name: 'Tfselect',
	init: function init($container, option) {
		var _this = this,
		    itemData = [];
		option = $.extend({}, {
			single: false
		}, option);

		if (option.value) {
			$.each(option.value, function (i, va) {
				$('<div>').append($('<div class="tfselect">').dxSelectBox({
					dataSource: va,
					disabled: true,
					displayExpr: "expert_judges",
					valueExpr: "id",
					deferRendering: false
				})).append($('<div class="tftextarea">').dxTextArea({
					value: va.content
				})).append($('<div class="tfdelete">').dxButton({
					text: '删除',
					type: 'normal'
				})).appendTo($container);
			});
		}

		$container.append($('<div class="tfbutton">').dxButton({
			text: '增加专家鉴',
			type: 'normal',
			onClick: function onClick() {
				$container.find('.tfbutton').before($('<div>').append($('<div>').dxSelectBox({
					dataSource: new DevExpress.data.DataSource({
						store: $.crudStore(API("restful?_model=expert-user"))
					}),
					displayExpr: 'name',
					valueExpr: 'id'

				})).append($('<div>').dxTextArea({})).append($('<div>').dxButton({
					text: '删除',
					type: 'normal'

				})));
			}
		}));
	},
	render: function render($container) {}
});

DxExtInstance.define({
	name: 'Tfexpret',
	init: function init($container, option) {
		var _this = this;
		option = $.extend({}, {
			single: false
		}, option);

		$container.append($("<div>").dxSelectBox({
			dataSource: option.value,
			value: option.value[0].id,
			displayExpr: 'expert_user_name',
			valueExpr: 'id',
			showClearButton: true,
			placeholder: "分类",
			searchEnabled: true,
			noDataText: '没有请求到分类数据',
			deferRendering: false,
			onValueChanged: function onValueChanged(e) {
				_this.setValue(e.value);
			}
		}));
	},
	render: function render($container) {}
});

DxExtInstance.define({
	name: 'Tfscore',
	init: function init($container, option) {
		var _this = this;
		option = $.extend({}, {
			single: false
		}, option);
		var docu = $("<div> <span>专家评分</span> <div class='tfselect' ></div> <span>评分</span> <div class='tftext'></div>   </div>");

		docu.find("span").css({
			"margin": "10px",
			"width": "10%"
		});

		var boxtext = docu.find(".tftext").dxTextBox({}).data("dxTextBox");

		docu.find(".tfselect").dxSelectBox({
			dataSource: option.value,
			displayExpr: "expert_user_name",
			valueExpr: 'score',
			showClearButton: true,
			noDataText: '没有专家评分',
			placeholder: '专家分类',
			onValueChanged: function onValueChanged(e) {
				boxtext.option("value", e.value);
			}
		});

		docu.appendTo($container);
	},
	render: function render($container) {}
});

DxExtInstance.define({
	name: 'Tfsame_score',
	init: function init($container, option) {
		var _this = this;
		option = $.extend({}, {
			single: false
		}, option);
		var docu = $("<div> <span>生产年份</span> <div class='tfselect' ></div> <span>平均分</span> <div class='tftext'></div>   </div>");

		docu.find("span").css({
			"margin": "10px",
			"width": "10%"
		});

		var boxtext = docu.find(".tftext");

		var boxselect = $("<div>").dxSelectBox({
			dataSource: [],
			displayExpr: 'expert_user_name',
			valueExpr: 'expert_user_id',
			showClearButton: true,
			noDataText: '没有数据',
			onSelectionChanged: function onSelectionChanged(e) {}
		});

		docu.find(".tfselect").dxSelectBox({
			dataSource: option.value,
			displayExpr: "vintage",
			valueExpr: 'averageScore',
			showClearButton: true,
			noDataText: '没有同系列酒',
			placeholder: '同系列酒分类',
			value: 'expert_scores',
			onSelectionChanged: function onSelectionChanged(e) {
				console.log(e);
				boxtext.text(e.selectedItem.averageScore);
				boxselect.option('dataSource', e.selectedItem.expert_scores);
			}
		});

		docu.appendTo($container);
		boxselect.appendTo($container);
	},
	render: function render($container) {}
});

DxExtInstance.define({
	name: 'ExpertList',
	init: function init($container, option) {},
	render: function render($container) {
		var v = this.getValue(),
		    _this = this;

		v = v || [];

		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="width: 120px; line-height: 36px;">专家/达人</div>' + '<div class="pull-left" style="width: 50%;"><div class="expert-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="expert-remove"></div></div>' + '</div></div>' + '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="width: 120px; line-height: 36px;">点评内容</div>' + '<div class="pull-left" style="width: 50%;"><div class="expert-input-area"></div></div>' + '</div></div>' + '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="width: 120px; line-height: 36px;">一句话点评</div>' + '<div class="pull-left" style="width: 50%;"><div class="expert-input-word"></div></div>' + '</div></div>;';

		$container.html('');
		$.each(v, function (i, item) {
			var $c = $(tpl);

			$c.find(".expert-input-name").dxSelectBox({
				dataSource: $.crudStore(API('restful?_model=expert-user')),
				showClearButton: true,
				placeholder: "专家",
				searchEnabled: true,
				valueExpr: 'id',
				noDataText: '没有请求到专家数据',
				itemTemplate: function itemTemplate(data, index, $el) {
					if (data) {
						$("<span>" + data.name + " (" + data.account + ")</span>").appendTo($el);
					}
				},
				displayExpr: function displayExpr(data) {
					return data ? data.name + " (" + data.account + ")" : "";
				},
				width: '100%',
				value: item.expert_user_id,
				onValueChanged: function onValueChanged(e) {
					v[i].expert_user_id = e.value;
				}
			});

			$c.find(".expert-input-area").dxTextArea({
				width: '100%',
				value: item.content,
				onValueChanged: function onValueChanged(e) {
					v[i].content = e.value;
				}
			});

			$c.find(".expert-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c.find('.expert-input-word').dxTextBox({
				width: '100%',
				value: item.short_review,
				onValueChanged: function onValueChanged(e) {
					v[i].short_review = e.value;
				}
			});

			$c.appendTo($container);
		});

		$("<div/>").dxButton({
			text: '增加',
			onClick: function onClick() {
				v.push({
					expert_user_id: 0,
					content: '',
					short_review: ''
				});
				_this.setValue(v);
			}
		}).appendTo($container);
	}
});

DxExtInstance.define({
	name: 'ExpertScoreList',
	init: function init($container, option) {
		$container.html('<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<h3 class="pull-left score-year-title">本款酒</h3>' + '<div class="score-title pull-left" style="line-height: 36px; margin: 0 10px;">平均分 <span class="avg-score">0</span></div>' + '</div></div>' + '<div class="score-container"></div>');
	},
	render: function render($container) {
		var v = this.getValue(),
		    _this = this,
		    $content = $container.find(".score-container");

		v = v || [];

		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">专家</div>' + '<div class="pull-left"><div class="expert-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin: 0 10px;">评分</div>' + '<div class="pull-left"><div class="expert-input-area"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="expert-remove"></div>' + '</div></div>';

		var updateScore = function updateScore() {
			var v = _this.getValue() || [],
			    s = 0;

			$.each(v, function (i, vitem) {
				var n = parseInt(vitem.score) || 0;
				s += n;
			});

			$(".avg-score", $container).text(v.length > 0 ? Math.ceil(s / v.length) : 0);
		};

		$content.html('');
		$.each(v, function (i, item) {
			var $c = $(tpl);

			$c.find(".expert-input-name").dxSelectBox({
				dataSource: $.crudStore(API('restful?_model=expert-user')),
				showClearButton: true,
				placeholder: "专家",
				searchEnabled: true,
				valueExpr: 'id',
				noDataText: '没有请求到专家数据',
				itemTemplate: function itemTemplate(data, index, $el) {
					if (data) {
						$("<span>" + data.name + " (" + data.account + ")</span>").appendTo($el);
					}
				},
				displayExpr: function displayExpr(data) {
					return data ? data.name + " (" + data.account + ")" : "";
				},
				value: item.expert_user_id,
				onValueChanged: function onValueChanged(e) {
					v[i].expert_user_id = e.value;
				}
			});

			$c.find(".expert-input-area").dxNumberBox({
				width: 80,
				value: item.score,
				showSpinButtons: true,
				// format: "#0%",
				onValueChanged: function onValueChanged(e) {
					v[i].score = e.value;
					updateScore();
				}
			});

			$c.find(".expert-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c.appendTo($content);
		});

		$("<div/>").dxButton({
			text: '添加专家评分',
			onClick: function onClick() {
				v.push({
					expert_user_id: 0,
					score: 0
				});
				_this.setValue(v);
			}
		}).appendTo($content);

		updateScore();
	}
});

DxExtInstance.define({
	name: 'ExpertYearScoreList',
	init: function init($container, option) {},
	render: function render($container) {
		var v = this.getValue(),
		    _this = this;
		console.log(v);
		v = v || [];

		var tplH = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<h3 class="pull-left score-year-title">属性</h3>' + '<div class="pull-right"><div class="score-year-remove"></div></div>' + '</div></div>';

		var tpl0 = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">属性名称</div>' + '<div class="pull-left"><div class="score-year"></div></div>' + '</div></div>' + '<div class="score-container"></div>' + '<div style="margin-top: 20px;"><div class="expert-add"></div></div>';

		var tpl = '<div class="row" style="margin-bottom: 10px;"><div class="col-sm-12">' + '<div class="pull-left" style="line-height: 36px; margin-right: 10px;">属性</div>' + '<div class="pull-left"><div class="expert-input-name"></div></div>' + '<div class="pull-left" style="line-height: 36px; margin-left: 20px;"><div class="expert-remove"></div>' + '</div></div>';

		var updateScore = function updateScore() {
			var v = _this.getValue() || [],
			    s = 0;

			$.each(v, function (i, vitem) {
				var n = parseInt(vitem.score) || 0;
				s += n;
			});

			$(".avg-score", $container).text(v.length > 0 ? Math.ceil(s / v.length) : 0);
		};

		$container.html('');
		$.each(v, function (i, item) {
			var $c0 = $("<div style='margin-bottom: 20px;'>" + tplH + tpl0 + "</div>");

			$c0.find(".score-year-title").text('属性 #' + i);

			$c0.find(".score-year-remove").dxButton({
				text: '删除',
				onClick: function onClick() {
					v.splice(i, 1);
					_this.setValue(v);
				}
			});

			$c0.find(".score-year").dxTextBox({
				value: v[i].name,
				onValueChanged: function onValueChanged(e) {
					v[i].name = e.value;
				}
			});

			var $cc = $c0.find(".score-container");

			if (v[i].values) {

				v[i].values = v[i].values;
				// v[i].values = undefined;
			} else {
				v[i].values = v[i].values || [];
			}

			v[i].type = 'radio';

			$c0.find(".expert-add").dxButton({
				text: '添加属性',
				onClick: function onClick() {
					v[i].values.push({
						value: ''

					});
					_this.setValue(v);
				}
			});

			$.each(item.values, function (k, item) {
				var _$c$find$dxTextBox;

				var $c = $(tpl);

				$c.find(".expert-input-name").dxTextBox((_$c$find$dxTextBox = {
					value: '',
					itemTemplate: function itemTemplate(data, index, $el) {
						if (data) {
							$("<span>" + data.name + " (" + data.account + ")</span>").appendTo($el);
						}
					},
					displayExpr: function displayExpr(data) {
						return data ? data.name + " (" + data.account + ")" : "";
					}
				}, _defineProperty(_$c$find$dxTextBox, "value", item.value), _defineProperty(_$c$find$dxTextBox, "onValueChanged", function onValueChanged(e) {
					v[i].values[k].value = e.value;
				}), _$c$find$dxTextBox));

				// $c.find(".expert-input-area").dxNumberBox({
				// 	width: 80,
				// 	value: item.score,
				// 	showSpinButtons: true,
				// 	// format: "#0%",
				// 	onValueChanged: function(e) {
				// 		v[i].items[k].score = e.value;
				// 		updateScore();
				// 	}
				// });

				$c.find(".expert-remove").dxButton({
					text: '删除',
					onClick: function onClick() {
						v[i].values.splice(k, 1);
						_this.setValue(v);
					}
				});

				$c.appendTo($cc);
			});

			$c0.appendTo($container);
		});

		$("<div/>").dxButton({
			text: '添加类',
			onClick: function onClick() {
				v.push({
					name: "",
					values: []
				});
				_this.setValue(v);
			}
		}).appendTo($container);

		updateScore();
	}
});

DxExtInstance.define({
	name: 'YearSelect',
	init: function init($container, option) {
		var years = [],
		    _this = this;
		for (var y = 1950; y <= 2050; y++) {
			years.push(y);
		}$("<div class='year-select'/>").appendTo($container).dxSelectBox({
			dataSource: years,
			acceptCustomValue: true,
			onValueChanged: function onValueChanged(e) {
				_this.setValue(e.value);
			},
			displayExpr: function displayExpr(v) {
				return v ? v + '年' : '';
			}
		});
	},
	render: function render($container) {
		var v = this.getValue(),
		    v0 = $(".year-select", $container).dxSelectBox("option", "value");

		if (v != v0) {
			$(".year-select", $container).dxSelectBox("option", "value", v);
		}
	}
});

var DxExtClass = function () {
	function DxExtClass() {
		_classCallCheck(this, DxExtClass);
	}

	_createClass(DxExtClass, [{
		key: "define",
		value: function define(opt) {
			DxExtInstance.define(opt);
		}
	}, {
		key: "validate",
		value: function validate(formInstance) {
			DxExtInstance.validate(formInstance);
		}
	}]);

	return DxExtClass;
}();

var DxExt = exports.DxExt = new DxExtClass();


},{}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Page = exports.Page = function () {
	function Page($container) {
		_classCallCheck(this, Page);

		this.$container = $container;
		this.$content = $container;
	}

	_createClass(Page, [{
		key: "crudLayout",
		value: function crudLayout(opt) {
			var data = $.extend({ title: '未设置' }, opt),
			    html = "<div class=\"app-content-body \">\n\t\t  <div class=\"bg-light lter b-b wrapper-md hidden-print\">\n\t\t    <h1 class=\"m-n font-thin h3\">{{title}}</h1>\n\t\t  </div>\n\t\t  <div>\n\t\t    <div class=\"crud-container crud-reset\">\n\t\t    </div>\n\t\t  </div>\n\t\t</div>";

			this.$container.html(Mustache.render(html, data));
			this.$content = this.$container.find(".crud-container");

			return this;
		}
	}, {
		key: "content",
		get: function get() {
			return this.$content;
		}
	}]);

	return Page;
}();


},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Util = function () {
	function Util() {
		_classCallCheck(this, Util);
	}

	_createClass(Util, [{
		key: 'formatXbm',
		value: function formatXbm(xbm) {
			if (xbm == '2') return '女';else if (xbm == '1') return '男';else return xbm;
		}
	}, {
		key: 'crudStore',
		value: function crudStore(url, options) {

			var SERVICE_URL = url;
			options = $.extend({
				onDataArrived: function onDataArrived(data) {
					return data;
				}
			}, options);

			return new DevExpress.data.CustomStore($.extend({}, options, {

				load: function load(loadOptions) {

					var u = new Url(SERVICE_URL);

					u.query._param = JSON.stringify(loadOptions);

					return $.getJSON(u).then(function (resp) {
						console.log("data done");
						return options.onDataArrived(resp);
					});
				},

				byKey: function byKey(key) {
					var u = new Url(SERVICE_URL);
					u.path += "/" + encodeURIComponent(key);

					return $.getJSON(u).then(function (resp) {
						return resp;
					});
				},

				insert: function insert(values) {
					console.log(values);
					return $.post(SERVICE_URL, values).always(function (resp) {
						$.crudStoreResp = resp.responseJSON;
						return resp;
					});
				},

				update: function update(key, values) {
					var u = new Url(SERVICE_URL);
					u.path += "/" + encodeURIComponent(key);

					return $.ajax({
						url: u,
						method: "PUT",
						data: values
					}).always(function (resp) {
						$.crudStoreResp = resp.responseJSON;
						return resp;
					});
				},

				remove: function remove(key) {
					var u = new Url(SERVICE_URL);
					u.path += "/" + encodeURIComponent(key);

					return $.ajax({
						url: u,
						method: "DELETE"
					});
				}

			}));
		}
	}]);

	return Util;
}();

exports.default = new Util();


},{}]},{},[2]);
