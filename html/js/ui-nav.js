+function ($) {

  $(function(){
			// $("[ui-nav]>ul>li:not([nots])").addClass('active')
      // nav
      $(document).on('click', '[ui-nav] a', function (e) {
        if($('body').find('div').first().hasClass('app-aside-folded') ){
          return 
        }
        var $this = $(e.target), $active;
        $this.is('a') || ($this = $this.closest('a'));
        
        
        
//      $active && $active.toggleClass('active').find('> ul:visible').slideUp(200);
        ($this.parent().hasClass('active') && $this.next().slideUp(200)) || $this.next().slideDown(200);
        $this.parent().toggleClass('active');
        
        $this.next().is('ul') && e.preventDefault();
      });

      $('.nav.navbar-nav').click(function(){
        $('.navi .nav').find("*").removeClass('active');
        $('.nav.nav-sub.dk').css('display','block')
      })

  });
}(jQuery);