
jQuery.extend({
	config: function(key) {
		var items = {
			
			qrCodeUrl: 'https://mprog.letsdrink.com.cn/wine/',
			apiUrl: 'https://admin.letsdrink.com.cn/api/admin/',
			// apiUrl: 'http://letsdrink-admin.aosaiban.com/api/admin/'
			// qrCodeUrl: 'http://letsdrink-mprog.aosaiban.com/wine/',
			//apiUrl:'http://192.168.0.106/project/letsdrink-api/public/api/admin/'
			apiUrl: 'http://localhost/project/letsdrink-api/public/api/admin/',
		};

		if (key === undefined) {
			return items;
		} else {
			return items[key];
		}
	}
});
