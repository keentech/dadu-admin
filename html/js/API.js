   var API = function(name, param) {
  if(typeof(param) === 'object') {
    var u = new Url($.config('apiUrl') + name);

    for(var k in param) {
      u.query[k] = param[k];
    }

    return u.toString();
  } else {
    return $.config('apiUrl') + name + (param ? "/" + param : "");
  }
};