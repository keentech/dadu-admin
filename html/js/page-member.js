
$(function(){
	$("#form").dxForm({
        formData: {},
        readOnly: false,
        showColonAfterLabel: true,
        showValidationSummary: false,
        colCount: 2,
        validationGroup: "customerData",
        items: [{
            itemType: "group",
            items: [{
                dataField: "nickname",
                label: {text: '会员昵称'},
                validationRules: [{
                    type: "required",
                    message: "请输入会员昵称"
                }]
            }]
        }, {
            itemType: "group",
            items: [{
                dataField: "会员性别"
            }]
        }, {
            itemType: "group",
            colCount: 3,
            items: [{
                editorType: "dxSelectBox",
                editorOptions: {
//                  dataSource: countries
                },
                validationRules: [{
                    type: "required",
                    message: "请选择省份"
                }]
            },{
                editorType: "dxSelectBox",
                editorOptions: {
//                  dataSource: countries
                },
                validationRules: [{
                    type: "required",
                    message: "请选择市"
                }]
            },{
                editorType: "dxSelectBox",
                editorOptions: {
//                  dataSource: countries
                },
                validationRules: [{
                    type: "required",
                    message: "请选择区"
                }]
            }]
        },{
            itemType: "group",
            items: [{
                dataField: "详细地址",
                validationRules: [{
                    type: "required",
                    message: "请输入详细地址"
                }]
            }]
        }, {
        	itemType: "group",
            items: [{
            	dataField: "电话",
	            editorOptions: {
	                mask: "",
	                maskRules: {
	                    "X": /[02-9]/
	                },
	                maskInvalidMessage: "The phone must have a correct USA phone format",
	                useMaskedValue: true
	            },
	             validationRules: [{
	             	type:"required",
	             	message:"请输入电话"
	             },{
	                type: "pattern",
	                pattern: /^\+\s*1\s*\(\s*[02-9]\d{2}\)\s*\d{3}\s*-\s*\d{4}$/,
	                message: "电话号码格式错误"
	            }]
            }]
         },{
        	itemType: "group",
            items: [{
            	dataField: "remarks",
	            validationRules: [{
	                type: "required",
	                message: "请输入备注"
	            }]
            }]
        }]
    }).dxForm("instance");

    $("#form-container").on("submit", function(e) {
        DevExpress.ui.notify({
            message: "You have submitted the form",
            position: {
                my: "center top",
                at: "center top"
            }
        }, "success", 1000);
        
        e.preventDefault();
    });

    $("#save").dxButton({
        text: "保存",
        type: "success",
        useSubmitBehavior: true,
        validationGroup: "customerData"
    });
    $("#save_build").dxButton({
        text: "保存并新建联系人",
        type: "success",
        useSubmitBehavior: true,
        validationGroup: "customerData"
    });
    $("#cancel").dxButton({
        text: "取消",
        type: "normal"
    });
    $("#sumbit").dxButton({
        text: "提交 / 更改",
        type: "success",
        useSubmitBehavior: true,
        validationGroup: "customerData"
    });
    
    $("#zz_form").dxForm({
        formData: {},
        readOnly: false,
        showColonAfterLabel: true,
        showValidationSummary: false,
        validationGroup: "customerData",
        items: [{
            itemType: "group",
            items: [{
                dataField: "会员姓名",
                validationRules: [{
                    type: "required",
                    message: "请输入会员姓名"
                }]
            },{
            	dataField: "身份证号",
                validationRules: [{
                    type: "required",
                    message: "请输入身份证号"
                },{
	                type: "pattern",
	                pattern: /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/,
	                message: "身份证号格式错误"
            	}]	
            },{
            	dataField: "会员卡号",
                validationRules: [{
                    type: "required",
                    message: "请输入会员卡号"
                }]	
            },{
            	dataField: "当前数量",
                validationRules: [{
                    type: "required",
                    message: "请输入当前数量"
                },{
	            	type:"pattern",
	            	pattern:/^[1-9]\d*$/,
	            	message:"请输入正确的数量"
	            }]	
            },{
            	dataField: "新增/终止数量",
                validationRules: [{
                    type: "required",
                    message: "请输入新增/终止数量"
                },{
	            	type:"pattern",
	            	pattern:/^[1-9]\d*$/,
	            	message:"请输入正确的数量"
	            }]	
            },{
                dataField: "备注信息",
                editorType: "dxTextArea",
                editorOptions: {
                    height: 140
                }
            },{
                dataField: "备注文件",
                editorType: "dxButton",
                editorOptions:{
                	width:100,
                	height:30
                }
            }]
        }]
    }).dxForm("instance");
    
    $("#qy_form").dxForm({
        formData: {},
        readOnly: false,
        showColonAfterLabel: true,
        showValidationSummary: false,
        validationGroup: "customerData",
        items: [{
            itemType: "group",
            items: [{
                dataField: "会员姓名",
                validationRules: [{
                    type: "required",
                    message: "请输入会员姓名"
                }]
            },{
            	dataField: "身份证号",
                validationRules: [{
                    type: "required",
                    message: "请输入身份证号"
                },{
	                type: "pattern",
	                pattern: /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/,
	                message: "身份证号格式错误"
            	}]	
            },{
            	dataField: "会员卡号",
                validationRules: [{
                    type: "required",
                    message: "请输入会员卡号"
                }]	
            },{
            	dataField: "代理类型",
            	editorType: "dxSelectBox",
                validationRules: [{
                    type: "required",
                    message: "请选择代理类型"
                }]	
             }]
	        },{
            	itemType: "group",
            	colCount:"3",
            	items:[{
            		dataField: "代理区域",
            		editorType: "dxSelectBox",
	                validationRules: [{
	                    type: "required",
	                    message: "请选择省"
	                }]	
            	},{
            		editorType: "dxSelectBox",
	                validationRules: [{
	                    type: "required",
	                    message: "请选择市"
	                }]	
            	},{
            		editorType: "dxSelectBox",
	                validationRules: [{
	                    type: "required",
	                    message: "请选择区"
	                }]	
            	}]
            },{
            	itemType: "group",
            	items:[{
                dataField: "备注信息",
                editorType: "dxTextArea",
                editorOptions: {
                    height: 140
                }
            },{
                dataField: "备注文件",
                editorType: "dxButton",
                editorOptions:{
                	width:100,
                	height:30
                }
            }]
        }]
    }).dxForm("instance");
    
    $("#money_form").dxForm({
        formData: {},
        readOnly: false,
        showColonAfterLabel: true,
        showValidationSummary: false,
        validationGroup: "customerData",
        items: [{
            itemType: "group",
            items: [{
                dataField: "会员姓名",
                validationRules: [{
                    type: "required",
                    message: "请输入会员姓名"
                }]
            },{
            	dataField: "打款银行",
                validationRules: [{
                    type: "required",
                    message: "请输入打款银行"
                },{
	                type: "pattern",
	                pattern: /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/,
	                message: "身份证号格式错误"
            	}]	
            },{
            	dataField: "打款卡号",
                validationRules: [{
                    type: "required",
                    message: "请输入打款卡号"
                }]	
            },{
            	dataField: "打款金额",
                validationRules: [{
                    type: "required",
                    message: "请输入打款金额"
                },{
	            	type:"pattern",
	            	pattern:/^[1-9]\d*$/,
	            	message:"请输入正确的打款金额"
	            }]	
            },{
            	dataField: "手续费",
            	disabled:true
            },{
            	dataField: "实际打款金额",
                validationRules: [{
                    type: "required",
                    message: "请输入实际打款金额"
                },{
	            	type:"pattern",
	            	pattern:/^[1-9]\d*$/,
	            	message:"请输入正确的实际打款金额"
	            }]	
            },{
            	dataField: "银行流水号",
                validationRules: [{
                    type: "required",
                    message: "请输入银行流水号"
                },{
	            	type:"pattern",
	            	pattern:/^[1-9]\d*$/,
	            	message:"请输入正确的银行流水号"
	            }]	
            },{
                dataField: "备注",
                editorType: "dxTextArea",
                editorOptions: {
                    height: 140
                }
            }]
        }]
    }).dxForm("instance");
});