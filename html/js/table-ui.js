
$(function(){
   $("#vipcert").dxForm({
		readOnly: false,
		validationGroup: "customerData",
		items:[{
			itemType: "group",
            items: [{
                dataField: "会员姓名",
                validationRules: [{
                    type: "required",
                    message: "请输入会员姓名"
                }]
            },{
		        itemType: "group",
		        items: [{
		            dataField: "身份证号",
		            validationRules: [{
		                type: "required",
		                message: "请输入身份证号"
		            },{
		                type: "pattern",
		                pattern: /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/,
		                message: "身份证号格式错误"
	            	}]
		        }]
			}]
		}]
	}).dxForm("instance");

    $("#form-container").on("submit", function(e) {
        DevExpress.ui.notify({
            message: "You have submitted the form",
            position: {
                my: "center top",
                at: "center top"
            }
        }, "success", 3000);
        
        e.preventDefault();
    });

    $("#save").dxButton({
        text: "保存",
        type: "success",
        useSubmitBehavior: true,
        validationGroup: "customerData"
    });
    
    
});