

module.exports = function(grunt) {

grunt.initConfig({
    babel: {
        options: {
            "sourceMap": true
        },
        dist: {
            files: [{
                "expand": true,
                "cwd": "src/js",
                "src": ["**/*.jsx"],
                "dest": "src/js-compiled/",
                "ext": ".js"
            }]
        }
    },
    browserify: {
        compile:{
            expand: true,
            cwd: "src/js-compiled",
            src: ["*.js"],
            dest: "src/js-browserify/",
            ext: "-browserify.js"
        }
    },
    uglify: {
        all_src : {
            options : {
              sourceMap : true,
              sourceMapName : 'src/build/sourceMap.map'
            },
            src : 'src/js-browserify/admin-ui-browserify.js',
            dest : 'html/js/all.min.js'
        }
    }
});

    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask("default", ["babel", "browserify", "uglify"]);
};