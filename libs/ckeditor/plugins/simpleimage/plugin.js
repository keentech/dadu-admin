CKEDITOR.plugins.add( 'simpleimage', {
    icons: 'simpleimage',
    init: function( editor ) {
        editor.addCommand( 'insertSimpleImage', {
            exec: function( editor ) {
                editor.fire("simpleimage.insert");
            }
        });
        editor.ui.addButton( 'simpleimage', {
            label: '插入图片',
            command: 'insertSimpleImage',
            toolbar: 'insert'
        });
    }
});