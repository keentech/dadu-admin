CKEDITOR.plugins.add( 'wine', {
    icons: 'wine',
    init: function( editor ) {
        editor.addCommand( 'insertWine', {
            exec: function( editor ) {
                editor.fire("wine.insert");
            }
        });
        editor.ui.addButton( 'Wine', {
            label: '插入葡萄酒',
            command: 'insertWine',
            toolbar: 'insert'
        });
    }
});